
# CspLeafNodes

This project is developing the CSP Master and Subarray Leaf Node components of Telescope Monitoring and Control (TMC) system, for the Square Kilometre Array. CSP Leaf Nodes monitor and control the CSP Master and Subarray devices.

Documentation
-------------
[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-tmc-cspleafnodes/badge/?version=latest)](https://developer.skao.int/projects/ska-tmc-cspleafnodes/en/latest/)

The documentation for this project, including how to get started with it, can be found in the `docs` folder, and can be better browsed in the SKA development portal:

* [CspLeafNode documentation](https://developer.skao.int/projects/ska-tmc-cspleafnodes/en/latest/?badge=latest  "SKA Developer Portal: CspLeafNode documentation")