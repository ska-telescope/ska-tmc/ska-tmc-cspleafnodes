Modules
-------


ska\_tmc\_cspsubarrayleafnode.csp\_subarray\_leaf\_node\ module
---------------------------------------------------------------

.. automodule:: ska_tmc_cspsubarrayleafnode.csp_subarray_leaf_node
   :members:
   :undoc-members:
   :show-inheritance:


ska\_tmc\_cspsubarrayleafnode.csp\_subarray\_leaf\_node\_mid module
-------------------------------------------------------------------

.. automodule:: ska_tmc_cspsubarrayleafnode.csp_subarray_leaf_node_mid
   :members:
   :undoc-members:
   :show-inheritance:


ska\_tmc\_cspsubarrayleafnode.csp\_subarray\_leaf\_node\_low module
-------------------------------------------------------------------

.. automodule:: ska_tmc_cspsubarrayleafnode.csp_subarray_leaf_node_low
   :members:
   :undoc-members:
   :show-inheritance:



###################################
Properties in CSP Subarry Leaf Node
###################################

+-------------------------------+---------------+--------------------------------------------------------------------------------+
| Property Name                 | Data Type     | Description                                                                    |
+===============================+===============+================================================================================+
| CspSubarrayFQDN               | DevString     | FQDN of the CSP Subarray Tango Device Server.                                  |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| LivelinessCheckPeriod         | DevFloat      | Period for the liveliness probe to monitor each device in a loop.              |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| EventSubscriptionCheckPeriod  | DevFloat      | Period for the event subscriber to check the device subscriptions in a loop.   |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| AdapterTimeOut                | DevFloat      | Timeout for the adapter creation. This property is for internal use.           |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| CommandTimeOut                | DevFloat      | Timeout for the command execution                                              |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| TelmodelSource                | DevString     | Property to provide telmodel source for array layout                           |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| TelmodelPath                  | DevString     | Property to provide telmodel path for array layout                             |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| DelayModelTimeInAdvance       | DevShort      | Time in advance for calculating delays                                         |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| DelayCadence                  | DevFloat     | Cadence for delay calculation                                                   |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| DelayValidityPeriod           | DevFloat     | Validity period for calculated delays                                           |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+


##################################################
Additional Properties in CSP Subarry Leaf Node Low
##################################################

+-------------------------------+------------+-----------------------------------------------------------------------------+
| PSTDelayModelTimeInAdvance    | DevShort   | Time in advance for calculating PST beam delays                             |
+-------------------------------+------------+-----------------------------------------------------------------------------+
| PSTDelayCadence               | DevFloat   | Cadence for PST beam delay calculation                                      |
+-------------------------------+------------+-----------------------------------------------------------------------------+
| PSTDelayValidityPeriod        | DevFloat   | Validity period for calculated PST beam delays                              |
+-------------------------------+------------+-----------------------------------------------------------------------------+