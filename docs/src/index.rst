This project is developing the TMC CSP Leaf Nodes component of the Telescope Monitoring and Control (TMC) prototype, for the `Square Kilometre Array`_.

.. _Square Kilometre Array: https://skatelescope.org/

.. toctree::
   :maxdepth: 1
   :caption: Getting started

   getting_started/getting_started
   
.. toctree::
   :maxdepth: 2
   :caption: Developer guide

   developer_guide/code_quality
   developer_guide/fqdn

.. toctree::
   :maxdepth: 1
   :caption: CSP Controller leaf node

   cspmasterleafnode/ska_tmc_cspmasterleafnode

.. toctree::
   :maxdepth: 1
   :caption: CSP Subarray Leaf Node

   cspsubarrayleafnode/ska_tmc_cspsubarrayleafnode


Indices and tables
------------------
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

* :ref:`search`