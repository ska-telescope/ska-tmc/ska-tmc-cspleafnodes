#############################
FQDN for CSP LeafNode devices
#############################


FQDN for CSP LeafNode devices
=============================
The FQDNs of all the TMC low devices are updated to comply with ADR-9

+-----------------------------------------+---------------------------------------+
| Previous FQDN                           | Updated FQDN                          |
+-----------------------------------------+---------------------------------------+
| `ska_low/tm_leaf_node/csp_master`       | `low-tmc/leaf-node-csp/0`             |
+-----------------------------------------+---------------------------------------+
| `ska_low/tm_leaf_node/csp_subarray01`   | `low-tmc/subarray-leaf-node-csp/01`   |
+-----------------------------------------+---------------------------------------+
| `ska_mid/tm_leaf_node/csp_master`       | `mid-tmc/leaf-node-csp/0`             |
+-----------------------------------------+---------------------------------------+
| `ska_mid/tm_leaf_node/csp_subarray01`   | `mid-tmc/subarray-leaf-node-csp/01`   |
+-----------------------------------------+---------------------------------------+