ska\_tmc\_cspmasterleafnode package
===================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   ska_tmc_cspmasterleafnode.commands
   ska_tmc_cspmasterleafnode.manager

Submodules
----------

ska\_tmc\_cspmasterleafnode._csp\_master\_leaf\_node module
-----------------------------------------------------------

.. automodule:: ska_tmc_cspmasterleafnode.csp_master_leaf_node
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: ska_tmc_cspmasterleafnode
   :members:
   :undoc-members:
   :show-inheritance:



##################################
Properties in CSP Master Leaf Node
##################################

+-------------------------------+---------------+--------------------------------------------------------------------------------+
| Property Name                 | Data Type     | Description                                                                    |
+===============================+===============+================================================================================+
| CspMasterFQDN                 | DevString     | FQDN of the CSP Master Tango Device Server.                                    |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| LivelinessCheckPeriod         | DevFloat      | Period for the liveliness probe to monitor each device in a loop.              |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| EventSubscriptionCheckPeriod  | DevFloat      | Period for the event subscriber to check the device subscriptions in a loop.   |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| AdapterTimeOut                | DevFloat      | Timeout for the adapter creation. This property is for internal use.           |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+


#################################################
Additional Properties in CSP Master Leaf Node Mid
#################################################

+-------------------------------+---------------+----------------------+---------------------------------------------------------+
| CommandTimeOut                | DevFloat      | Timeout for the command execution                                              |
+-------------------------------+---------------+----------------------+---------------------------------------------------------+