"""
CSP Controller leaf node acts as a CSP contact point
for Master Node and also to monitor
and issue commands to the CSP Master.
"""
import threading
from typing import Any, List, Tuple

from ska_tango_base.base.base_device import SKABaseDevice
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from ska_tmc_common.enum import LivelinessProbeType
from tango import AttrWriteType
from tango.server import attribute, command, run

from ska_tmc_cspmasterleafnode.csp_master_leaf_node import (
    TmcLeafNodeCspControl,
)
from ska_tmc_cspmasterleafnode.manager import CspMLNComponentManagerMid

__all__ = ["MidTmcLeafNodeCspControl", "main"]

# pylint: disable = attribute-defined-outside-init


class MidTmcLeafNodeCspControl(TmcLeafNodeCspControl):
    """
    Tango device class for TMC CSP Controller leaf node MID.
    """

    sourceDishVccConfig = attribute(
        dtype="DevString", access=AttrWriteType.READ
    )
    dishVccConfig = attribute(dtype="DevString", access=AttrWriteType.READ)

    DishVccMapValidationResult = attribute(
        dtype="str", access=AttrWriteType.READ
    )

    def init_device(self):
        super().init_device()
        self._source_dish_vcc_config: str = ""
        self._dish_vcc_config: str = ""
        self._dish_vcc_map_validation_result: ResultCode = ResultCode.STARTED

    class InitCommand(TmcLeafNodeCspControl.InitCommand):
        """
        A class for the CSP Controller leaf node MID init_device() method.
        """

        def do(
            self: SKABaseDevice.InitCommand,
            *args: Any,
            **kwargs: Any,
        ) -> Tuple[ResultCode, str]:
            """
            Initializes the attributes and properties of the CSP Master Leaf
            Node.

            :return: A tuple containing a return code and a string message
            indicating status. The message is for informational purposes only.
            """
            super().do()
            device = self._device
            device._source_dish_vcc_config = ""
            device._dish_vcc_config = ""
            device._memorized_dish_vcc_map = ""
            device._dish_vcc_map_validation_result = ResultCode.STARTED
            for attribute_name in [
                "DishVccMapValidationResult",
                "sourceDishVccConfig",
            ]:
                device.set_change_event(attribute_name, True, False)
                device.set_archive_event(attribute_name, True)
            device.submit_task_for_dish_vcc_validation()
            return (ResultCode.OK, "")

    def update_dish_vcc_config_callback(self, value: str):
        """Update dishVccConfig attribute callback"""
        self.logger.info(
            "Updating dishVccConfig attribute value to: %s", value
        )
        self._dish_vcc_config = value

    def handle_updated_dish_vcc_config(self, value: str):
        """Update sourceDishVccConfig attribute callback"""
        self._source_dish_vcc_config = value

        try:
            self.push_change_archive_events(
                "sourceDishVccConfig", self._source_dish_vcc_config
            )
            self.logger.info(
                "Successfully updated and pushed sourceDishVccConfig "
                "attribute value to: %s",
                value,
            )
        except Exception as exception:
            self.logger.exception(
                "Exception while pushing event for sourceDishVccConfig: %s",
                exception,
            )

    def update_dish_vcc_map_validation_result(self, value: ResultCode):
        """Update and push event for DishVccMapValidationResult"""
        self.logger.info(
            "Updating DishVccMapValidationResult attribute to: %s", value
        )
        self._dish_vcc_map_validation_result = value
        try:
            self.push_change_archive_events(
                "DishVccMapValidationResult",
                str(int(self._dish_vcc_map_validation_result)),
            )
        except Exception as exception:
            self.logger.exception(
                "Exception while pushing event for DishVccMapValidationResult:"
                "%s",
                exception,
            )

    def read_sourceDishVccConfig(self) -> str:
        """
        Reads the sourceDishVccConfig value.
        :return: The sourceDishVccConfig value.
        """
        return self._source_dish_vcc_config

    def read_dishVccConfig(self) -> str:
        """
        Reads the dishVccConfig value.
        :return: The dishVccConfig value.
        """
        return self._dish_vcc_config

    def read_DishVccMapValidationResult(self) -> str:
        """
        Reads the DishVccMapValidationResult value.
        :return: The DishVccMapValidationResult value as a string.
        """
        return str(int(self._dish_vcc_map_validation_result))

    @attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
        memorized=True,
        hw_memorized=True,
    )
    def memorizedDishVccMap(self) -> str:
        """
        This attribute stores the latest dish VCC map version data
        in the Tango DB. The attribute is memorized so that when the device
        restarts, the last set dish VCC map version will be used for loading
        dish VCC config on the CSP master.
        """
        return self.component_manager.memorized_dish_vcc_map

    @memorizedDishVccMap.write
    def memorizedDishVccMap(self, value: str):
        """
        Sets the memorized dish VCC map.
        :param value: Dish VCC config JSON string.
        """
        self.component_manager.memorized_dish_vcc_map = value

    def create_component_manager(self) -> CspMLNComponentManagerMid:
        """
        Creates the component manager for the CSP Controller leaf node MID.
        :return: The component manager instance.
        """
        cm = CspMLNComponentManagerMid(
            self.CspMasterFQDN,
            logger=self.logger,
            _liveliness_probe=LivelinessProbeType.SINGLE_DEVICE,
            _event_receiver=True,
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            adapter_timeout=self.AdapterTimeOut,
            command_timeout=self.CommandTimeOut,
            _update_availablity_callback=(self.update_availablity_callback),
            _update_dish_vcc_config_callback=(
                self.update_dish_vcc_config_callback
            ),
            handle_updated_dish_vcc_config=(
                self.handle_updated_dish_vcc_config
            ),
            _update_dish_vcc_map_validation_result=(
                self.update_dish_vcc_map_validation_result
            ),
            _update_admin_mode_callback=self.update_admin_mode_callback,
        )
        cm.csp_master_device_name = self.CspMasterFQDN or ""
        return cm

    def is_LoadDishCfg_allowed(self) -> bool:
        """
        Checks whether the LoadDishCfg command is allowed to be run in
        the current device state.
        :return: True if the command is allowed to be run in the current
          device state, False otherwise.
        """
        return self.component_manager.is_command_allowed("LoadDishCfg")

    @command(
        dtype_in="DevString",
        doc_in=(
            "The string in JSON format containing data source, "
            "path, and interface."
        ),
        dtype_out="DevVarLongStringArray",
        doc_out="(ReturnType, 'Informational message')",
    )
    def LoadDishCfg(self, argin: str) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes the LoadDishCfg command on the CSP Master.
        :param argin: JSON formatted string with data source, path,
          and interface.
        :return: A tuple containing a list of ResultCodes and a list of
          informational messages.
        """
        handler = self.get_command_object("LoadDishCfg")
        result_code, unique_id = handler(argin)

        return [result_code], [unique_id]

    def init_command_objects(self):
        """
        Initializes the command handlers for commands supported by this device.
        """
        super().init_command_objects()
        self.register_command_object(
            "LoadDishCfg",
            SubmittedSlowCommand(
                "LoadDishCfg",
                self._command_tracker,
                self.component_manager,
                "load_dish_cfg_command",
                logger=self.logger,
            ),
        )

    def submit_task_for_dish_vcc_validation(self):
        """
        Validates the dish VCC map version against the CSP master.
        """
        self.logger.info("Starting dish VCC validation task.")
        start_thread = threading.Timer(
            5, self.component_manager.start_dish_vcc_validation
        )
        start_thread.start()


def main(args=None, **kwargs):
    """
    Runs the MidTmcLeafNodeCspControl.
    :param args: Arguments internal to TANGO
    :param kwargs: Arguments internal to TANGO
    :return: MidTmcLeafNodeCspControl TANGO object.
    """
    return run((MidTmcLeafNodeCspControl,), args=args, **kwargs)


if __name__ == "__main__":
    main()
