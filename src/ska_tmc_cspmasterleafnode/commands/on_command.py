"""
On command class for CSPMasterLeafNode.
"""
import threading
from typing import Any, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus

from ska_tmc_cspmasterleafnode.commands.csp_master_leaf_node_command import (
    CspMLNCommand,
)


# pylint: disable = abstract-method
class On(CspMLNCommand):
    """
    A class for CspMasterLeafNode's On() command.

    On command on CspmasterLeafNode enables the telescope to perform further
    operations and observations. It Invokes On command on CSP Master device.
    """

    # pylint: disable=unused-argument
    def on(
        self,
        logger,
        task_callback: TaskCallbackType,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """A method to invoke the On command.
        It sets the task_callback status according to command progress.

        :param logger: logger
        :type logger: logging.Logger
        :param task_callback: Update task state, defaults to None
        :type task_callback: TaskCallbackType
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """
        task_callback(status=TaskStatus.IN_PROGRESS)
        result_code, message = self.do()
        if result_code == ResultCode.FAILED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(result_code, message),
                exception=message,
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Command Completed"),
            )

    # pylint: enable=unused-argument

    def do(self, argin: Optional[Any] = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke On command on CSP Master.

        """
        return_code, message = self.init_adapter()
        if return_code == ResultCode.FAILED:
            return return_code, message

        result_code, message = self.call_adapter_method(
            "CSP Master", self.csp_master_adapter, "On", []
        )

        return result_code[0], message[0]
