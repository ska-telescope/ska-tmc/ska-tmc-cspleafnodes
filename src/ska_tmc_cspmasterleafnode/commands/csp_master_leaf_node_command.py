"""Command class for CSP Controller leaf node

This module defines a command class `CspMLNCommand` for the CSPMasterLeafNode
The `CspMLNCommand` class provides implementation for commands that interact
with the CSP Controller leaf node.
It includes methods for initializing adapters, checking device responsiveness,
and handling commands for both low-level and mid-level devices.

Attributes:
    LOGGER (logging.Logger): The logger for the module.
"""

import logging
import time
from typing import Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tmc_common import DeviceInfo
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.tmc_command import TmcLeafNodeCommand
from tango import ConnectionFailed, DevFailed

configure_logging()
LOGGER = logging.getLogger(__name__)


class CspMLNCommand(TmcLeafNodeCommand):
    """Abstract command class for all CspMasterLeafNode"""

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
    ):
        super().__init__(component_manager, logger)
        self.csp_master_adapter = None

    def check_unresponsive(self) -> None:
        """Checks if the device is unresponsive

        :return:
            None
        """
        device_info: DeviceInfo = self.component_manager.get_device()
        if device_info is None or device_info.unresponsive:
            raise DeviceUnresponsive(
                f"The invocation of the {__class__} command on this device "
                + "is not allowed."
                + "Reason: CSP master device is not available."
                + "The command has NOT been executed."
                + "This device will continue with normal operation.",
            )

    def init_adapter_low(self):
        """
        Initialize the adapter for the low-level device.
        """
        self.init_adapter()

    def init_adapter_mid(self):
        """
        Initialize the adapter for the mid-level device.
        """
        self.init_adapter()

    def do_mid(self, argin=None):
        """
        Handle a command for the mid-level device.

        :param argin: Optional command argument.
        """

    def do_low(self, argin=None):
        """
        Handle a command for the low-level device.

        :param argin: Optional command argument.
        """

    def do(self, argin=None):
        """
        Handle a command.

        :param argin: Optional command argument.
        """

    def update_task_status(self, **kwargs):
        """
        Update the status of a task.

        :param kwargs: Keyword arguments for task status update.
        """

    def init_adapter(self) -> Tuple[ResultCode, str]:
        """Creates adapter for underlying csp master device"""
        dev_name: str = self.component_manager.csp_master_device_name
        adapter_timeout = self.component_manager.adapter_timeout
        elapsed_time = 0
        start_time = time.time()
        while (
            self.csp_master_adapter is None and elapsed_time <= adapter_timeout
        ):
            try:
                self.csp_master_adapter = (
                    self.adapter_factory.get_or_create_adapter(
                        dev_name,
                        AdapterType.CSPMASTER,
                    )
                )
            except ConnectionFailed as connection_failed:
                elapsed_time = time.time() - start_time
                if elapsed_time > adapter_timeout:
                    message = (
                        f"Error in creating adapter for "
                        f"Error in creating adapter for "
                        f"{dev_name}: {connection_failed}"
                    )
                    return ResultCode.FAILED, message
            except DevFailed as dev_failed:
                elapsed_time = time.time() - start_time
                if elapsed_time > adapter_timeout:
                    message = (
                        f"Error in creating adapter for "
                        f"Error in creating adapter for "
                        f"{dev_name}: {dev_failed}"
                    )
                    return ResultCode.FAILED, message

            except (AttributeError, ValueError, TypeError) as exception:
                message = (
                    f"Error in creating adapter for "
                    f"{dev_name}: {exception}"
                )
                return ResultCode.FAILED, message

        return (ResultCode.OK, "")
