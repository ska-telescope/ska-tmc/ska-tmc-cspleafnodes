"""
Load Dish Cfg command class for CSP Controller leaf node
"""
import threading
import time
from typing import Any, Callable, Dict, Optional, Tuple, Union

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_telmodel.data import TMData
from ska_tmc_common import TimeoutCallback
from tango.server import DevFailed

from ska_tmc_cspmasterleafnode.commands.csp_master_leaf_node_command import (
    CspMLNCommand,
)


class LoadDishCfg(CspMLNCommand):
    """
    A class for CSP MasterLeafNode's LoadDishCfg command.

    LoadDishCfg command on CSP MasterLeafNode invokes LoadDishCfg command on
    CSP Master device.
    """

    def __init__(self, component_manager, logger=None) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, self.logger)
        self.task_callback: Optional[Callable] = None

    def loadDishCfg(
        self,
        argin,
        logger,
        task_callback: TaskCallbackType,
        task_abort_event: threading.Event,
    ) -> None:
        """A method to invoke the Standby command.
        It sets the task_callback status according to command progress.

        :param logger: logger
        :type logger: logging.Logger
        :param task_callback: Update task state, defaults to None
        :type task_callback: TaskCallbackType
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """

        self.task_callback = task_callback
        task_callback(status=TaskStatus.IN_PROGRESS)

        self.component_manager.start_timer(
            self.timeout_id,
            self.component_manager.command_timeout,
            self.timeout_callback,
        )

        return_code, message = self.do(argin)

        logger.info(message)
        if return_code == ResultCode.FAILED:
            self.update_task_status(
                result=(return_code, message), exception=message
            )
            self.component_manager.stop_timer()
        else:
            self.start_tracker_thread(
                "get_load_dish_cfg_result",
                [ResultCode.OK],
                task_abort_event,
                self.timeout_id,
                self.timeout_callback,
                command_id=self.component_manager.load_dish_cfg_id,
                lrcr_callback=(
                    self.component_manager.long_running_result_callback
                ),
            )

    def update_task_status(
        self,
        **kwargs: Dict[str, Union[Tuple[ResultCode, str], TaskStatus, str]],
    ) -> None:
        """
        Update the status of a task.

        Args:
            **kwargs: Keyword arguments for task status update.
        """
        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("exception")
        if status == TaskStatus.ABORTED:
            self.task_callback(status=status)

        if result[0] == ResultCode.FAILED:
            self.task_callback(status=status, result=result, exception=message)
        else:
            self.task_callback(status=status, result=result)
        self.component_manager.clear_load_dish_cfg_attributes()

    def get_dishid_vcc_map_json(
        self, initial_params: dict
    ) -> Tuple[dict, str]:
        """Get DishId-VCC map json from initial params
        :param initial_param: this param containg tm data source uri
        and file path which is used for extracting vcc_map json file
        :param type: str
        :rtype: Tuple (dict, str)
        """
        data_sources = initial_params.get("tm_data_sources", None)
        tm_data_filepath = initial_params.get("tm_data_filepath", None)
        if data_sources and tm_data_filepath:
            try:
                data = TMData(data_sources)
                return data[tm_data_filepath].get_dict(), ""

            except (
                AttributeError,
                ValueError,
                TypeError,
                KeyError,
                DevFailed,
            ) as exception:
                self.logger.exception(
                    "Error in Loading Dish VCC map json file %s", exception
                )
                return (
                    {},
                    f"Error in Loading Dish VCC map json file {exception}",
                )
        return {}, "tm_data_sources and tm_data_filepath not provided in json"

    def do(self, argin: Optional[Any] = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke LoadDishCfg command on CSP Master.
        """
        return_code, message = self.init_adapter()

        if return_code == ResultCode.FAILED:
            return return_code, message

        result_code, _ = self.call_adapter_method(
            "CSP Master", self.csp_master_adapter, "LoadDishCfg", argin
        )
        return result_code[0], "Command Completed"
