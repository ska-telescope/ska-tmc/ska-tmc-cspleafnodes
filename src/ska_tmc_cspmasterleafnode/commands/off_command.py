"""
Off command class for CSP Controller leaf node
"""
import threading
from typing import Any, Optional, Tuple

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus

from ska_tmc_cspmasterleafnode.commands.csp_master_leaf_node_command import (
    CspMLNCommand,
)


class Off(CspMLNCommand):
    """
    A class for CspMasterLeafNode's Off() command.

    Off command on CspMasterLeafNode invokes Off command on CSP Master device.
    """

    # pylint: disable=unused-argument
    def off(
        self,
        logger,
        task_callback: TaskCallbackType,
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """A method to invoke the Off command.
        It sets the task_callback status according to command progress.

        :param logger: logger
        :type logger: logging.Logger
        :param task_callback: Update task state, defaults to None
        :type task_callback: TaskCallbackType
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """

        task_callback(status=TaskStatus.IN_PROGRESS)

        result_code, message = self.do()
        if result_code == ResultCode.FAILED:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(result_code, message),
                exception=message,
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, "Command Completed"),
            )

    # pylint: enable=unused-argument

    def do(self, argin: Optional[Any] = None) -> Tuple[ResultCode, str]:
        """
        Method to invoke Off command on CSP Master.
        """
        return_code, message = self.init_adapter()
        if return_code == ResultCode.FAILED:
            return return_code, message

        result_code, message = self.call_adapter_method(
            "CSP Master", self.csp_master_adapter, "Off", []
        )

        return result_code[0], message[0]
