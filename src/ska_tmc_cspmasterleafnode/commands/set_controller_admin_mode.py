"""
SetAdminMode command class for CSPMasterLeafNode.
"""
from typing import Tuple

from ska_control_model import AdminMode
from ska_tango_base.commands import ArgumentValidator, FastCommand, ResultCode

from ska_tmc_cspmasterleafnode.commands.csp_master_leaf_node_command import (
    CspMLNCommand,
)


# pylint: disable = abstract-method
class SetAdminMode(CspMLNCommand, FastCommand):
    """
    A class for CspMasterLeafNode's SetAdminMode() command.

    SetAdminMode command on CspMasterLeafNode enables to set the adminMode of
    the csp controller device
    """

    def __init__(self, logger, component_manager):
        """Initialization.

        Args:
            logger (logging.Logger): Used for logging.
            component_manager (CSPMLNcomponentManager): Instance of
            CSPMLNComponentManager.
        """
        super().__init__(logger)
        self.component_manager = component_manager
        self._validator = ArgumentValidator()
        self._name = "SetAdminMode"

    # pylint: disable=signature-differs
    def do(self, argin: AdminMode) -> Tuple[ResultCode, str]:
        """
        A method to set the adminMode of the CSP controller device
        :param argin: A adminMode enum value to be set for controller device
        """
        if self.component_manager.is_admin_mode_enabled:
            return_code, message = self.init_adapter()
            if return_code == ResultCode.FAILED:
                return return_code, message
            try:
                self.csp_master_adapter.adminMode = argin
                self.logger.info(
                    "Invoking SetAdminMode command on %s",
                    self.csp_master_adapter.dev_name,
                )
            except Exception as e:
                self.logger.info(
                    "Failed to set the adminMode of the CSP Controller."
                    + " Error while setting the adminMode : %s",
                    e,
                )
                return ResultCode.FAILED, "Command Failed"
            return ResultCode.OK, "Command Completed"
        self.logger.info(
            "AdminMode functionality is disabled, "
            + "Device will function normally"
        )

        return ResultCode.NOT_ALLOWED, (
            "AdminMode functionality is disabled, "
            + "Device will function normally"
        )
