"""Class for managing dish vcc validation during
initialization of device
"""
# pylint: disable=no-value-for-parameter
import json
import time

from ska_control_model.admin_mode import AdminMode
from ska_tango_base.commands import ResultCode
from tango import DevState

from ska_tmc_cspmasterleafnode.commands.load_dish_cfg_command import (
    LoadDishCfg,
)


class DishVccMapValidationManager:
    """Class for managing dish VCC validation during device initialization."""

    def __init__(self, component_manager, logger) -> None:
        self.component_manager = component_manager
        self.logger = logger
        self.csp_master_source_dish_vcc = ""
        self.csp_master_dish_vcc_config = ""
        self.csp_master_adapter = None

    def get_csp_master_adapter(self) -> None:
        """Creates an adapter for the CSP Master."""
        self.logger.info("Initializing CSP Master adapter.")
        load_dish_cfg = LoadDishCfg(self.component_manager, self.logger)
        result, _ = load_dish_cfg.init_adapter()

        if result == ResultCode.OK:
            self.csp_master_adapter = load_dish_cfg.csp_master_adapter
            self.logger.info(
                "CSP Master adapter has been successfully initialized."
            )
        else:
            self.logger.exception(
                "Failed to initialize CSP Master adapter. Result code: %s",
                result,
            )

    def check_csp_master_dish_vcc_config_ready(self) -> bool:
        """Reads DishVccConfig info from the CSP Master Device."""
        if (
            self.csp_master_adapter
            and self.csp_master_adapter.adminMode == AdminMode.ONLINE
            and self.csp_master_adapter.state
            not in (DevState.DISABLE, DevState.FAULT)
        ):
            self.logger.info(
                "CSP Master device state is %s",
                self.csp_master_adapter.state,
            )
            self.logger.info(
                "Source dish vcc is %s",
                self.csp_master_adapter.sourceDishVccConfig,
            )
            self.csp_master_source_dish_vcc = (
                self.csp_master_adapter.sourceDishVccConfig
            )
            self.csp_master_dish_vcc_config = (
                self.csp_master_adapter.dishVccConfig
            )
            return True

        return False

    def wait_for_csp_dish_vcc_config_ready(self) -> bool:
        """Waits and checks if the dishVccConfig info on the CSP
        Master is ready."""
        self.logger.info("Waiting for CSP LMC to be ready")
        is_csp_master_responsive = False

        while True:
            try:
                self.component_manager.check_if_csp_master_is_responsive()
                is_csp_master_responsive = True
                if not self.csp_master_adapter:
                    self.get_csp_master_adapter()
                if self.check_csp_master_dish_vcc_config_ready():
                    self.logger.info(
                        "CSP LMC is ready with correct DishVCC config"
                    )
                    return True
            except Exception as exception:
                self.logger.exception(
                    "Error while checking CSP Master readiness: %s", exception
                )
            if not is_csp_master_responsive:
                self.component_manager.update_dish_vcc_map_validation_result(
                    ResultCode.NOT_ALLOWED
                )
            time.sleep(1)

    def get_csp_master_dish_vcc(self) -> str:
        """Get sourceDishVccVersion attribute value of csp master"""
        return self.csp_master_source_dish_vcc

    def get_csp_ln_memorized_dish_vcc(self) -> str:
        """Return memorized dish vcc map value of csp Controller leaf node"""
        return self.component_manager.memorized_dish_vcc_map

    def validate_dish_vcc_version(self) -> None:
        """
        Validates the dish VCC of the CSP Controller leaf node and CSP Master.

        Validation Criteria:
        - Both not set: UNKNOWN
        - One set, the other not: FAILED
        - Both set, values differ: FAILED
        - Both set, values match: OK
        """
        csp_master_dish_vcc = self.get_csp_master_dish_vcc()
        csp_ln_dish_vcc = self.get_csp_ln_memorized_dish_vcc()
        self.logger.info("CSP LN dish VCC: %s, CSP Master dish VCC: %s")
        try:
            if not csp_master_dish_vcc and not csp_ln_dish_vcc:
                self.logger.info(
                    "Dish VCC version not set an CSP MLN and CSP Master."
                )
                self.component_manager.update_dish_vcc_map_validation_result(
                    ResultCode.UNKNOWN
                )
            elif (not csp_master_dish_vcc and csp_ln_dish_vcc) or (
                csp_master_dish_vcc and not csp_ln_dish_vcc
            ):
                self.logger.info("Dish VCC map version missing in CSP Master.")
                self.component_manager.update_dish_vcc_map_validation_result(
                    ResultCode.FAILED
                )
            elif json.loads(csp_master_dish_vcc) == json.loads(
                csp_ln_dish_vcc
            ):
                self.logger.info("Dish VCC values match.")
                self.component_manager.update_dish_vcc_map_validation_result(
                    ResultCode.OK
                )
                if self.component_manager.handle_updated_dish_vcc_config:
                    self.component_manager.handle_updated_dish_vcc_config(
                        csp_ln_dish_vcc
                    )
                if self.component_manager.update_dish_vcc_config_callback:
                    self.component_manager.update_dish_vcc_config_callback(
                        self.csp_master_dish_vcc_config
                    )
            else:
                self.logger.info("Dish VCC values do not match.")
                self.component_manager.update_dish_vcc_map_validation_result(
                    ResultCode.FAILED
                )
        except Exception as exception:
            self.logger.exception(
                "Exception occurred during dish VCC validation: %s",
                str(exception),
            )
