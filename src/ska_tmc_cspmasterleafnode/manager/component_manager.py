"""
This module implements ComponentManager class for the CSP Controller leaf node.
"""
import logging
import threading
import time
from logging import Logger
from typing import Any, Callable, Optional, Tuple

import tango
from ska_control_model import AdminMode
from ska_ser_logging.configuration import configure_logging
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import (
    CommandNotAllowed,
    DeviceInfo,
    DeviceUnresponsive,
    LivelinessProbeType,
)
from ska_tmc_common.v1.event_receiver import EventReceiver
from ska_tmc_common.v1.tmc_component_manager import TmcLeafNodeComponentManager
from tango import DevState

from ska_tmc_cspmasterleafnode.commands import Off, On, Standby

configure_logging()
LOGGER = logging.getLogger(__name__)


class CspMLNComponentManager(TmcLeafNodeComponentManager):
    """
    A component manager for The CSP Controller leaf node component.

    It supports in controlling the behaviour of CSP Master.
    """

    def __init__(
        self,
        csp_master_device_name: str,
        logger: Logger = LOGGER,
        _liveliness_probe: LivelinessProbeType = (
            LivelinessProbeType.SINGLE_DEVICE
        ),
        _event_receiver: bool = False,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        adapter_timeout: int = 30,
        _update_availablity_callback: Optional[Callable[[bool], None]] = None,
        _update_admin_mode_callback: Optional[Callable[[Any], None]] = None,
    ):
        """
        Initialise a new ComponentManager instance.

        :param csp_master_device_name: Device name for the CSP Master device.
        :param logger: Optional. A logger for this component manager
        :param _liveliness_probe: Optional. LivelinessProbe for the
            component manager
        :param _event_receiver: Optional. Object of EventReceiver class
            monitoring purpose. Default 1
        :param proxy_timeout: Optional. Time period to wait for
            event and responses. Default 500 milliseconds
        :param sleep_time: Optional. Sleep time between reties. Default 1 Sec
        :param adapter_timeout : Optional. Time period to wait for
            intialization of adapter.

        """
        self._csp_master_device_name = csp_master_device_name
        super().__init__(
            logger,
            _liveliness_probe=_liveliness_probe,
            _event_receiver=_event_receiver,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
        )
        self._device: DeviceInfo = DeviceInfo(csp_master_device_name)
        self._is_admin_mode_enabled: bool = True
        self.adapter_timeout = adapter_timeout
        self.on_command_object = On(self, logger)
        self.off_command_object = Off(self, logger)
        self.standby_command_object = Standby(self, logger)
        self.update_availablity_callback = _update_availablity_callback
        self.update_admin_mode_callback = _update_admin_mode_callback
        if _event_receiver:
            evet_subscribe_check_period = event_subscription_check_period
            self._event_receiver = EventReceiver(
                self,
                logger,
                proxy_timeout=proxy_timeout,
                event_subscription_check_period=evet_subscribe_check_period,
                attribute_dict=self.get_attribute_dict(),
            )
            self._event_receiver.start()
        self.rlock = threading.RLock()
        if _liveliness_probe:
            self.start_liveliness_probe(_liveliness_probe)

    def get_device(self) -> DeviceInfo:
        return self._device

    @property
    def csp_master_device_name(self) -> str:
        """Returns device name for the CSP Master Device."""
        return self._csp_master_device_name

    @csp_master_device_name.setter
    def csp_master_device_name(self, device_name: str) -> None:
        """Sets the device name for CSP Master Device."""
        self._csp_master_device_name = device_name

    def get_attribute_dict(self) -> dict:
        """
        Returns the common attribute dictionary for all component types.

        :return: Dictionary of common attributes to be handled by
            the EventReceiver.
        """
        return {"adminMode": self.handle_admin_mode_event}

    # pylint:disable = signature-differs
    # It needs to be fix in ska-tmc-common
    def on(self, task_callback):
        """Empty method for resolving lint issue"""

    def off(self, task_callback):
        """Empty method for resolving lint issue"""

    def standby(self, task_callback):
        """Empty method for resolving lint issue"""

    def start_communicating(self):
        """Empty method for resolving lint issue"""

    def stop_communicating(self):
        """Empty method for resolving lint issue"""

    def on_command(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """Submits the On command for execution.

        :rtype: tuple
        """
        task_status, response = self.submit_task(
            self.on_command_object.on,
            args=[self.logger],
            task_callback=task_callback,
        )
        self.logger.info("On command queued for execution")
        return task_status, response

    def off_command(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """Submits the Off command for execution.

        :rtype: tuple
        """
        task_status, response = self.submit_task(
            self.off_command_object.off,
            args=[self.logger],
            task_callback=task_callback,
        )
        self.logger.info("Off command queued for execution")
        return task_status, response

    def standby_command(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """Submits the Standby command for execution.

        :rtype: tuple
        """
        task_status, response = self.submit_task(
            self.standby_command_object.standby,
            args=[self.logger],
            task_callback=task_callback,
        )
        self.logger.info("Standby command queued for execution")
        return task_status, response

    def check_if_csp_master_is_responsive(self) -> None:
        """Checks if Csp Master device is responsive."""

        if self._device is None or self._device.unresponsive:
            raise DeviceUnresponsive(
                f"{self.csp_master_device_name} not available"
            )

    def is_command_allowed(self, command_name: str) -> bool:
        """
        Checks whether this command is allowed.
        It checks that the device is in the right state to execute this command
        and that all the components needed for the operation are not
        unresponsive.

        :return: True if this command is allowed

        :rtype: boolean
        """

        if command_name in ["On", "Off", "Standby", "LoadDishCfg"]:
            if self.op_state_model.op_state in [
                DevState.FAULT,
                DevState.UNKNOWN,
            ]:
                raise CommandNotAllowed(
                    f"The invocation of the {__class__} command on this "
                    + "device is not allowed.\n"
                    + "Reason: The current operational state "
                    + f"is {self.op_state_model.op_state}."
                    + "The command has NOT been executed."
                    + "This device will continue with normal operation.",
                    self.op_state_model.op_state,
                )
            self.check_if_csp_master_is_responsive()
            return True
        return False

    def update_exception_for_unresponsiveness(
        self, device_info: DeviceInfo, exception: str
    ) -> None:
        """Set a device to failed and call the relative callback if available
        :param device_info: a device info
        :type device_info: DeviceInfo
        :param exception: an exception
        :type: Exception
        """
        with self.rlock:
            device_info.update_unresponsive(True, exception)
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(False)

    # pylint: disable=unused-argument
    def update_responsiveness_info(self, device_name: str) -> None:
        """
        Update a device with the correct availability information.

        :param dev_name: name of the device
        :type dev_name: str
        """
        with self.rlock:
            self.get_device().update_unresponsive(False, "")
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(True)

    def update_device_admin_mode(
        self, device_name: str, admin_mode: AdminMode
    ) -> None:
        """
        Update a monitored device admin mode,
        and call the relative callbacks if available
        :param device_name: Name of the device on which admin mode is updated
        :type device_name: str
        :param admin_state: admin mode of the device
        :type admin_mode: AdminMode
        """
        super().update_device_admin_mode(device_name, admin_mode)
        self.logger.info(
            "Admin Mode value updated to :%s", AdminMode(admin_mode).name
        )
        if self.update_admin_mode_callback:
            self.update_admin_mode_callback(admin_mode)

    def is_event_failed(self, event) -> bool:
        """Check if event failed"""
        if event.err:
            error = event.errors[0]
            error_msg = f"{error.reason},{error.desc}"
            dev_info = self.get_device()
            dev_info.last_event_arrived = time.time()
            self.logger.error(error_msg)
            self.update_event_failure(event.device.dev_name())
            return True
        return False

    def handle_admin_mode_event(
        self, event: tango.EventType.CHANGE_EVENT
    ) -> None:
        """Handle CSP controller change event"""
        if self.is_admin_mode_enabled:
            if event.err:
                error = event.errors[0]
                error_msg = f"{error.reason},{error.desc}"
                self.logger.error(error_msg)
                self.update_event_failure(event.device.dev_name())
                return
            new_value = event.attr_value.value
            self.logger.info(
                "Received an adminMode event with : %s for device: %s",
                new_value,
                event.device.dev_name(),
            )
            self.update_device_admin_mode(event.device.dev_name(), new_value)
            self.logger.info(
                "Admin Mode updated to :%s", AdminMode(new_value).name
            )
