"""
Component Manager class for Mid Telescope.
"""
import json
import logging
import time
from json import JSONDecodeError
from typing import Any, Callable, Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus
from ska_tmc_common import LRCRCallback
from ska_tmc_common.enum import LivelinessProbeType

from ska_tmc_cspmasterleafnode.commands import LoadDishCfg
from ska_tmc_cspmasterleafnode.manager.component_manager import (
    CspMLNComponentManager,
)
from ska_tmc_cspmasterleafnode.manager.dish_vcc_validation_manager import (
    DishVccMapValidationManager,
)

configure_logging()
LOGGER: logging.Logger = logging.getLogger(__name__)


class CspMLNComponentManagerMid(CspMLNComponentManager):
    """
    A component manager for The CSP Controller leaf node Mid component.
    """

    def __init__(
        self,
        csp_master_device_name: str,
        logger: logging.Logger = LOGGER,
        _liveliness_probe: LivelinessProbeType = (
            LivelinessProbeType.SINGLE_DEVICE
        ),
        _event_receiver: bool = False,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        adapter_timeout: int = 30,
        _update_availablity_callback: Callable[[Any], None] = None,
        _update_dish_vcc_config_callback: Callable[[Any], None] = None,
        handle_updated_dish_vcc_config: Callable[[Any], None] = None,
        _update_admin_mode_callback: Callable[[Any], None] = None,
        _update_dish_vcc_map_validation_result: Callable[
            [ResultCode], None
        ] = None,
        command_timeout: int = 15,
    ):
        """
        Initialise a new ComponentManager instance.

        :param csp_master_device_name: Device name for the CSP Master device.
        :param logger: Optional. A logger for this component manager
        :param _liveliness_probe: Optional. LivelinessProbe for the
          component manager
        :param _event_receiver: Optional. Object of EventReceiver class
        :param proxy_timeout: Optional. Time period to wait for event and
          responses. Default 500 milliseconds
        :param sleep_time: Optional. Sleep time between retries. Default
          1 second
        :param adapter_timeout: Optional. Time period to wait for the
          initialization of adapter.
        :param _update_availablity_callback: Optional. Callback for
           updating availability
        :param _update_dish_vcc_config_callback: Optional. Callback for
          updating dish VCC config
        :param handle_updated_dish_vcc_config: Optional. Callback for handling
          updated dish VCC config
        :param _update_dish_vcc_map_validation_result: Optional. Callback for
          updating dish VCC map validation result
        :param command_timeout: Optional. Timeout for commands.
          Default 15 seconds

        """
        super().__init__(
            csp_master_device_name,
            logger=logger,
            _liveliness_probe=_liveliness_probe,
            _event_receiver=_event_receiver,
            proxy_timeout=500,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
            adapter_timeout=adapter_timeout,
            _update_availablity_callback=_update_availablity_callback,
        )
        self.long_running_result_callback = LRCRCallback(self.logger)
        self.load_dish_cfg_command_obj = None
        self.command_timeout = command_timeout
        self.update_dish_vcc_config_callback = _update_dish_vcc_config_callback
        self.handle_updated_dish_vcc_config = handle_updated_dish_vcc_config
        self.update_dish_vcc_map_validation_result_callback = (
            _update_dish_vcc_map_validation_result
        )
        self.update_admin_mode_callback = _update_admin_mode_callback
        self.dish_vcc_validation_result = ""

        self.load_dish_cfg_command_result: ResultCode = ResultCode.UNKNOWN
        self.dish_vcc_config_sent: dict = {}
        self.source_dish_vcc_config_sent: dict = {}
        self.received_dish_vcc_config_event_value: str = ""
        self.received_source_dish_vcc_config_event_value: str = ""
        self.load_dish_cfg_id: str = ""
        self.memorized_dish_vcc_map = ""

    # pylint: disable=line-too-long
    def get_attribute_dict(self) -> dict:
        """
        Extends the common attribute dictionary with mid-specific attributes.

        :return: Dictionary of attributes to be handled by the EventReceiver.
        """
        common_attributes = super().get_attribute_dict()
        mid_specific_attributes = {
            "dishVccConfig": self.handle_dish_vcc_config_callback,
            "sourceDishVccConfig": self.handle_source_dish_vcc_config_callback,
            "longRunningCommandResult": self.handle_lrcr_callback,
        }
        return {**common_attributes, **mid_specific_attributes}

    def load_dish_cfg_command(
        self,
        argin: str,
        task_callback: Callable[[TaskStatus, str], None] = None,
    ) -> Tuple[TaskStatus, str]:
        """
        Submits the LoadDishCfg command for execution.

        :param argin: JSON string containing dish VCC configuration parameters
        :param task_callback: Optional. Callback function for task completion
        :return: A tuple of TaskStatus and a response message
        """
        self.load_dish_cfg_command_obj = LoadDishCfg(self, self.logger)
        try:
            dish_id_vcc_params = json.loads(argin)
        except JSONDecodeError as e:
            self.logger.exception(
                "Error in loading dish config JSON: %s Exception: %s", argin, e
            )
            message = f"The JSON string is malformed. Error: {str(e)}"
            return TaskStatus.REJECTED, message
        (
            dish_id_vcc_map_json,
            error,
        ) = self.load_dish_cfg_command_obj.get_dishid_vcc_map_json(
            dish_id_vcc_params
        )
        if error:
            return TaskStatus.REJECTED, error
        self.load_dish_cfg_id = f"{time.time()}-{LoadDishCfg.__name__}"
        self.dish_vcc_config_sent = dish_id_vcc_map_json
        self.source_dish_vcc_config_sent = dish_id_vcc_params

        task_status, response = self.submit_task(
            self.load_dish_cfg_command_obj.loadDishCfg,
            args=[argin, self.logger],
            task_callback=task_callback,
        )
        self.logger.info("LoadDishCfg command queued for execution")
        return task_status, response

    def clear_load_dish_cfg_attributes(self):
        """Clear all load dish cfg attribute after command completes"""
        self.dish_vcc_config_sent = {}
        self.source_dish_vcc_config_sent = {}
        self.load_dish_cfg_id = ""
        self.received_source_dish_vcc_config_event_value = ""
        self.received_dish_vcc_config_event_value = ""
        self.load_dish_cfg_command_result = ""

    def get_load_dish_cfg_result(self) -> None:
        """Return Load Dish Cfg command result"""
        return self.load_dish_cfg_command_result

    def is_dish_vcc_config_valid(self) -> Tuple[bool, str]:
        """
        Validate the sent dish VCC config and source dish VCC config match
        with received dish VCC config and source dish VCC config.

        :return: A tuple of boolean indicating validation success and an
            error message if any
        """
        self.logger.info(
            "Checking received dishVccConfig and sourceDishVccConfig"
        )
        error_message = ""
        if (
            not self.received_dish_vcc_config_event_value
            or not self.source_dish_vcc_config_sent
        ):
            error_message = "Dish VCC config not received from CSP"
            return False, error_message

        if self.dish_vcc_config_sent != json.loads(
            self.received_dish_vcc_config_event_value
        ):
            error_message = (
                "Sent dish VCC config and received dish VCC "
                "config from CSP controller do not match."
            )
        if self.source_dish_vcc_config_sent != json.loads(
            self.received_source_dish_vcc_config_event_value
        ):
            error_message += (
                " Sent source dish VCC config and received source"
                "dish VCC config do not match."
            )

            if error_message:
                return False, error_message
        return True, error_message

    def handle_dish_vcc_config_callback(self, event) -> None:
        """Handle dish vcc config attribute change event"""
        if not self.is_event_failed(event):
            self.logger.info(
                "DishVccConfig Event called with value %s",
                event.attr_value.value,
            )
            dev_info = self.get_device()
            dev_info.last_event_arrived = time.time()
            new_value = event.attr_value.value
            with self.lock:
                self.received_dish_vcc_config_event_value = new_value

    def handle_source_dish_vcc_config_callback(self, event) -> None:
        """Handle source dish vcc config attribute change event"""
        if not self.is_event_failed(event):
            self.logger.info(
                "SourceDishVccConfig Event called with value %s",
                event.attr_value.value,
            )
            with self.lock:
                self.received_source_dish_vcc_config_event_value = (
                    event.attr_value.value
                )

    def handle_lrcr_callback(self, event) -> None:
        """Event for LongRunning Command Result"""
        if not self.is_event_failed(event):
            self.update_command_result(
                event.device.dev_name(), event.attr_value.value
            )

    def update_command_result(
        self, dev_name: str, value: Tuple[str, str]
    ) -> None:
        """Updates the long running command result callback"""
        self.logger.info(
            "Received longRunningCommandResult event for device: %s, "
            "with value: %s",
            dev_name,
            value,
        )
        unique_id, result_and_message = value
        if not result_and_message:
            return
        command_result = json.loads(result_and_message)
        self.logger.info("command_result: %s", command_result)
        if "LoadDishCfg" in unique_id:
            result_code = command_result[0]
            if result_code == int(ResultCode.OK):
                (
                    is_dish_vcc_config_valid,
                    error_message,
                ) = self.is_dish_vcc_config_valid()
                self.logger.info(
                    "dishVccConfig and sourceDishVccConfig match is %s",
                    is_dish_vcc_config_valid,
                )
                if is_dish_vcc_config_valid:
                    self.update_dish_vcc_config_callback(
                        self.received_dish_vcc_config_event_value
                    )
                    self.handle_updated_dish_vcc_config(
                        self.received_source_dish_vcc_config_event_value
                    )
                    self.load_dish_cfg_command_result = ResultCode.OK
                    self.update_dish_vcc_map_validation_result(ResultCode.OK)

                else:
                    self.long_running_result_callback(
                        self.load_dish_cfg_id,
                        ResultCode.FAILED,
                        exception_msg=error_message,
                    )
                    self.update_dish_vcc_map_validation_result(
                        ResultCode.FAILED
                    )
                    self.observable.notify_observers(command_exception=True)
            elif result_code == int(ResultCode.FAILED):
                self.long_running_result_callback(
                    self.load_dish_cfg_id,
                    ResultCode.FAILED,
                    exception_message=command_result[1],
                )
                self.update_dish_vcc_map_validation_result(ResultCode.FAILED)
                self.observable.notify_observers(command_exception=True)

    def start_dish_vcc_validation(self) -> None:
        """
        Create Dish Vcc validation manager object and call validate method.
        """
        self.logger.info("Starting dish VCC validation")
        dish_vcc_validation_manager = DishVccMapValidationManager(
            self, self.logger
        )
        if dish_vcc_validation_manager.wait_for_csp_dish_vcc_config_ready():
            dish_vcc_validation_manager.validate_dish_vcc_version()
        else:
            self.logger.exception(
                "Timeout while waiting for CSP master to be ready"
            )
            self.update_dish_vcc_map_validation_result(ResultCode.NOT_ALLOWED)

    def update_dish_vcc_map_validation_result(self, value: ResultCode) -> None:
        """Update dish vcc map validation result
        :param value: ResultCode value to be updated
        :param type: ResultCode Enum
        """
        self.dish_vcc_validation_result = value
        if self.update_dish_vcc_map_validation_result_callback:
            self.update_dish_vcc_map_validation_result_callback(value)
