"""
This module uses the SKA Control Model for health state definitions an
SKA Tango Base for the base device class.
"""
from typing import Any, Union

from ska_control_model import AdminMode, HealthState
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from ska_tmc_common.enum import LivelinessProbeType
from ska_tmc_common.exceptions import CommandNotAllowed, DeviceUnresponsive
from ska_tmc_common.v1.tmc_base_leaf_device import TMCBaseLeafDevice
from tango import AttrWriteType, DebugIt
from tango.server import attribute, command, device_property, run

from ska_tmc_cspmasterleafnode import release
from ska_tmc_cspmasterleafnode.commands.set_controller_admin_mode import (
    SetAdminMode,
)
from ska_tmc_cspmasterleafnode.manager import CspMLNComponentManager

__all__ = ["TmcLeafNodeCspControl", "main"]


# pylint: disable = attribute-defined-outside-init
class TmcLeafNodeCspControl(TMCBaseLeafDevice):
    """
    CSP Controller leaf node acts as a CSP contact point
    for Master Node and also to monitor
    and issue commands to the CSP Master.
    """

    # all instances

    # -----------------
    # Device Properties
    # -----------------

    CspMasterFQDN = device_property(
        dtype="str",
        doc="FQDN of the CSP Master Tango Device Server.",
    )

    # -----------------
    # Attributes
    # -----------------

    isSubsystemAvailable = attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ,
    )

    cspControllerAdminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ,
    )
    isAdminModeEnabled = attribute(dtype=bool, access=AttrWriteType.READ_WRITE)

    CommandTimeOut = device_property(dtype="DevFloat", default_value=15)

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        super().init_device()
        self._is_subsystem_available = False

    class InitCommand(TMCBaseLeafDevice.InitCommand):
        """
        A class for the TMC CspMasterLeafNode's init_device() method.
        """

        def do(
            self: TMCBaseLeafDevice.InitCommand,
            *args: Any,
            **kwargs: Any,
        ) -> tuple[ResultCode, str]:
            """
            Initializes the attributes and properties of the
            CspMasterLeafNode.

            return:
                A tuple containing a return code and a string message
                indicating status.
                The message is for information purpose only.

            rtype:
                (ResultCode, str)
            """
            super().do()
            device = self._device

            device._build_state = (
                f"{release.name},{release.version},{release.description}"
            )
            device._health_state = HealthState.OK
            device._admin_mode = AdminMode.ONLINE
            device._csp_master_leaf_node_admin_mode = AdminMode.ONLINE
            device._is_admin_mode_enabled = True
            device._version_id = release.version
            device._is_subsystem_available = False
            for attribute_name in [
                "healthState",
                "isSubsystemAvailable",
                "cspControllerAdminMode",
                "isAdminModeEnabled",
            ]:
                device.set_change_event(attribute_name, True, False)
                device.set_archive_event(attribute_name, True)
            device.op_state_model.perform_action("component_on")
            return (ResultCode.OK, "")

    def always_executed_hook(self):
        pass

    # ------------------
    # Attributes methods
    # ------------------
    def update_availablity_callback(
        self, csp_master_availability: bool
    ) -> None:
        """Change event callback for isSubsystemAvailable"""
        if csp_master_availability != self._is_subsystem_available:
            self._is_subsystem_available = csp_master_availability
            self.push_change_archive_events(
                "isSubsystemAvailable", csp_master_availability
            )

    def update_admin_mode_callback(self, admin_mode: int):
        """Update CSPMLNAdminMode attribute callback"""
        self._csp_master_leaf_node_admin_mode = admin_mode

        try:
            self.push_change_archive_events(
                "cspControllerAdminMode",
                self._csp_master_leaf_node_admin_mode,
            )
            self.logger.info(
                "Successfully updated and pushed cspControllerAdminMode "
                "attribute value to: %s",
                admin_mode,
            )
        except Exception as exception:
            self.logger.exception(
                "Exception while pushing event for "
                + "cspControllerAdminMode: %s",
                exception,
            )

    def read_isSubsystemAvailable(self) -> bool:
        """Read method for isSubsystemAvailable"""
        return self._is_subsystem_available

    def read_cspControllerAdminMode(self) -> int:
        """Read method for cspControllerAdminMode Attribute"""
        return self._csp_master_leaf_node_admin_mode

    def read_isAdminModeEnabled(self) -> bool:
        """Return the isAdminModeEnabled attribute value"""
        return self.component_manager.is_admin_mode_enabled

    def write_isAdminModeEnabled(self, value: bool):
        """Set the value of isAdminModeEnabled attribute"""
        self.component_manager.is_admin_mode_enabled = value

    @attribute(label="cspMasterDevName", dtype="DevString")
    def cspMasterDevName(self) -> str:
        """Returns the cspmasterdevname attribute."""
        return self.component_manager.csp_master_device_name

    @cspMasterDevName.write
    def cspMasterDevName(self, value) -> str:
        """Sets the cspmasterdevname attribute."""
        self.component_manager.csp_master_device_name = value

    # --------
    # Commands
    # --------

    def is_On_allowed(
        self,
    ) -> Union[bool, CommandNotAllowed, DeviceUnresponsive]:
        """Checks whether this command is allowed to be run in
        current device state.

        :return: True if this command is allowed to be run in
             current device state.

        :rtype: boolean
        """
        return self.component_manager.is_command_allowed("On")

    @command(
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def On(self):
        """
        This command invokes On() command on CSP Master.

        Note: CSP Master supports input argument to turn on specific devices,
        but TMC does not yet support it. So the On command will always instruct
        CSP Master to turn on all the devices.
        """
        handler = self.get_command_object("On")
        result_code, unique_id = handler()

        return [[result_code], [unique_id]]

    def is_Off_allowed(
        self,
    ) -> Union[bool, CommandNotAllowed, DeviceUnresponsive]:
        """
        Checks whether this command is allowed to be run in
        current device state.

        :return: True if this command is allowed to be run in
            current device state.

        :rtype: boolean
        """
        return self.component_manager.is_command_allowed("Off")

    @command(
        dtype_out="DevVarLongStringArray",
    )
    def Off(self):
        """
        This command invokes Off() command on CSP Master.

        Note: CSP Master supports input argument to turn off specific devices,
        but TMC does not yet support it. So the Off command will always
        instruct CSP Master to turn on all the devices.
        """
        handler = self.get_command_object("Off")
        result_code, unique_id = handler()

        return [[result_code], [unique_id]]

    def is_Standby_allowed(
        self,
    ) -> Union[bool, CommandNotAllowed, DeviceUnresponsive]:
        """
        Checks whether this command is allowed to be run
        in current device state.

        :return: True if this command is allowed to be run
            in current device state.

        :rtype: boolean
        """
        return self.component_manager.is_command_allowed("Standby")

    @command(
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def Standby(self):
        """
        This command invokes Standby() command on CSP Master.

        Note: CSP Master supports input argument to turn specific devices to
        standby, but TMC does not yet support it. So the Standby command will
        always instruct CSP Master to turn on all the devices.
        """
        handler = self.get_command_object("Standby")
        result_code, unique_id = handler()

        return [[result_code], [unique_id]]

    @command(
        dtype_in=AdminMode,
        doc_in="The adminMode in enum format",
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def SetAdminMode(self, argin: AdminMode):
        """
        This command sets the adminMode command on Csp Controller.
        """
        handler = self.get_command_object("SetAdminMode")
        result_code, message = handler(argin)

        return [result_code], [message]

    def create_component_manager(self):
        cm = CspMLNComponentManager(
            self.CspMasterFQDN,
            logger=self.logger,
            _liveliness_probe=LivelinessProbeType.SINGLE_DEVICE,
            _event_receiver=True,
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            adapter_timeout=self.AdapterTimeOut,
            _update_availablity_callback=self.update_availablity_callback,
            _update_admin_mode_callback=self.update_admin_mode_callback,
        )
        cm.csp_master_device_name = self.CspMasterFQDN or ""
        return cm

    def init_command_objects(self):
        """
        Initialises the command handlers for commands supported by this
        device.
        """
        super().init_command_objects()
        for command_name, method_name in [
            ("On", "on_command"),
            ("Off", "off_command"),
            ("Standby", "standby_command"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    logger=self.logger,
                ),
            )
        self.register_command_object(
            "SetAdminMode", SetAdminMode(self.logger, self.component_manager)
        )


# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """
    Runs the MidTmcLeafNodeCspControl.

    :param args: Arguments internal to TANGO
    :param kwargs: Arguments internal to TANGO
    :return: MidTmcLeafNodeCspControl TANGO object.
    """
    return run((TmcLeafNodeCspControl,), args=args, **kwargs)


if __name__ == "__main__":
    main()
