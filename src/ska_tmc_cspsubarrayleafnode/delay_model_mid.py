"""
DelayManagerMid class
"""
from datetime import datetime, timedelta

import katpoint
import numpy as np
from astropy.time import Time
from ska_tango_base.commands import ResultCode

from ska_tmc_cspsubarrayleafnode.const import (
    MID_DELAYMODEL_VERSION,
    YPOL_OFFSET_NS,
)
from ska_tmc_cspsubarrayleafnode.delay_model import (
    DelayManager,
    get_antenna_params,
)

from .utils import dd_to_dms


class DelayManagerMid(DelayManager):
    """A class that calculates geometric delays for SKA Mid"""

    def __init__(
        self,
        csp_sln_cm,
        telmodel_source: str,
        telmodel_path: str,
        delay_model_time_in_advance: int,
        delay_cadence: float,
        delay_validity_period: float,
        logger,
    ):
        super().__init__(
            csp_sln_cm,
            telmodel_source,
            telmodel_path,
            delay_model_time_in_advance,
            delay_cadence,
            logger,
        )
        self.antenna_names = None
        self._delay_cadence = delay_cadence
        self._delay_validity_period = delay_validity_period

    def create_antenna_objects(self, antenna_params):
        """
        Creates KatPoint Antenna objects from antenna parameters.

        Args:
            antenna_params (dict): Dictionary containing antenna parameters.

        Returns:
            list: List of KatPoint Antenna objects.
        """
        antennas = []
        for antenna in antenna_params["receptors"]:
            antenna_param = get_antenna_params(antenna)
            input_to_antenna = f"{antenna_param.station_label},\
                        {dd_to_dms(antenna_param.location.lat)},\
                            {dd_to_dms(antenna_param.location.lon)},\
                                {antenna_param.location.height},\
                                    {antenna_param.diameter}"
            antennas.append(katpoint.Antenna(input_to_antenna))
        return antennas

    def update_config_params(self):
        """
        Update configuration parameters related to assigned resources
        during assign, release, or configure commands execution.

        :return: (ResultCode, Message)
        """
        assigned_receptors_dict = {}
        assigned_receptors = []

        # Log configuration

        # Retrieve telescope model data
        antenna_params, message = self.fetch_antenna_params()
        if message:
            return (ResultCode.FAILED, message + "Number of retries exhausted")

        if "receptors" not in antenna_params:
            return (
                ResultCode.FAILED,
                "Missing receptors key in antenna parameters dictionary.",
            )
        antennas = self.create_antenna_objects(antenna_params)
        # Create a dictionary including antenna objects
        antennas_dict = {ant.name: ant for ant in antennas}
        antenna_keys_list = antennas_dict.keys()
        for receptor in self.csp_sln_cm.get_receptor_ids():
            if receptor in antenna_keys_list:
                self.logger.info(
                    "Antenna object for receptor : %s = %s ",
                    receptor,
                    antennas_dict[receptor],
                )
                assigned_receptors.append(antennas_dict[receptor])
                # Create a dictionary including antennas (objects)
                # assigned to
                # the Subarray
                assigned_receptors_dict[receptor] = antennas_dict[receptor]
        # Antenna having key 'AC' from antennas_dict, is referred as a
        # reference antenna.
        ref_ant = antennas_dict["AC"]
        # Create DelayCorrection Object
        self.delay_correction_object = katpoint.DelayCorrection(
            assigned_receptors, ref_ant
        )
        self.antenna_names = list(
            self.delay_correction_object.ant_models.keys()
        )
        self.logger.info("Completed updating config parameters.")
        return (ResultCode.OK, "")

    def set_dummy_targets(self):
        """
        Set dummy targets for mid.

        :return: Tuple[ResultCode, str]: result code and message
        """
        time_t0 = 0
        timestamp_array = [
            time_t0 - 40,
            time_t0 - 20,
            time_t0,
            time_t0 + 20,
            time_t0 + 40,
        ]
        dummy_target = katpoint.Target("radec ,21:08:47.92 , -88:57:22.9")
        try:
            self.delay_correction_object.delays(dummy_target, timestamp_array)
        except Exception as exception:
            self.logger.exception(exception)
        return (ResultCode.OK, "Completed setting dummy targets")

    def calculate_geometric_delays(self, time_t0, antenna_names):
        """
        Calculate geometric delay values using KATPoint library.

        :param time_t0: reference time for delay calculation
        :param antenna_names: list of antenna names
        :return: Dictionary containing fifth order polynomial coefficients
                 per antenna per fsp
        """
        delay_corrections_h_array_dict = {}
        # 6 Delays are calculated for the timestamps at an
        # interval of 20 seconds.
        timesteps = self._delay_validity_period
        delta_times = [timestep * timesteps for timestep in range(-2, 4)]
        timestamp_array = [
            (time_t0 + timedelta(seconds=delta_times[i]))
            for i in range(len(delta_times))
        ]
        self.logger.debug("The timestamp_array is %s : ", timestamp_array)
        # Calculate geometric delay values.
        delays = self.delay_correction_object.delays(
            self.csp_sln_cm.target, timestamp_array
        )
        # Convert delays in seconds to 5th order polynomial coefficients
        # delta_times is like [-40, -20, 0, 20, 40, 60] as the delays
        # are calculated for the timestamps at an interval of 20 seconds.
        # pylint: disable = duplicate-code
        for antenna, antenna_name in enumerate(antenna_names):
            # Array including horizontal delay values per antenna for the
            # given timestamps
            horizontal_delays = np.array(delays[antenna * 2])
            # Calculating 5th order polynomial
            polynomial = np.polynomial.Polynomial.fit(
                delta_times, horizontal_delays, 5
            )
            polynomial_coefficients = polynomial.convert().coef
            delay_corrections_h_array_dict[
                antenna_name
            ] = polynomial_coefficients
        return delay_corrections_h_array_dict

    def calculate_delay_model(self):
        """
        Calculate delays using KatPoint library.

        :return: None
        """
        try:
            self.logger.info("Calculating mid delays.")

            SKA_EPOCH = "1999-12-31T23:59:28Z"
            ska_epoch_utc = Time(SKA_EPOCH, scale="utc")
            ska_epoch_tai = ska_epoch_utc.unix_tai

            if self.start_time_for_delay_calculation is None:
                self.start_time_for_delay_calculation = (
                    datetime.today()
                    + timedelta(seconds=self._delay_model_time_in_advance)
                )
            else:
                self.start_time_for_delay_calculation += timedelta(
                    seconds=self._delay_cadence
                )

            time_t0_utc = Time(
                self.start_time_for_delay_calculation, scale="utc"
            )
            offset_since_epoch = time_t0_utc.unix_tai - ska_epoch_tai

            delay_corrections_h_array_dict = self.calculate_geometric_delays(
                self.start_time_for_delay_calculation, self.antenna_names
            )

            self.delay_model_json["interface"] = MID_DELAYMODEL_VERSION
            self.delay_model_json["start_validity_sec"] = offset_since_epoch
            self.delay_model_json["cadence_sec"] = self._delay_cadence
            self.delay_model_json[
                "validity_period_sec"
            ] = self._delay_validity_period
            self.delay_model_json["config_id"] = self.csp_sln_cm.config_id
            self.delay_model_json["subarray"] = self.csp_sln_cm.subarray_id

            delay_details = []
            for receptor in self.csp_sln_cm.get_receptor_ids():
                receptor_delay_coeffs = delay_corrections_h_array_dict[
                    receptor
                ]
                delay_details_dict = {}
                delay_coeff_array = list(receptor_delay_coeffs)
                delay_coeff_array_ns = [
                    coeff * 1e9 for coeff in delay_coeff_array
                ]
                delay_details_dict["receptor"] = receptor
                delay_details_dict["xypol_coeffs_ns"] = delay_coeff_array_ns
                delay_details_dict["ypol_offset_ns"] = YPOL_OFFSET_NS
                delay_details.append(delay_details_dict)

            self.delay_model_json["receptor_delays"] = delay_details
            log_msg = f"Mid Delay Model JSON: {self.delay_model_json}"
            self.logger.info(log_msg)
        except Exception as exception:
            self.logger.exception(exception)
