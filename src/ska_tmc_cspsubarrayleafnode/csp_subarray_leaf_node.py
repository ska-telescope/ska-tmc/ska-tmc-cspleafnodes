"""
Csp Subarray Leaf node monitors the CSP Subarray and issues control
actions during an observation.
It also acts as a CSP contact point for Subarray Node for observation
execution for TMC.
"""
from typing import Any, List, Tuple

from ska_control_model import AdminMode, HealthState
from ska_tango_base.commands import ResultCode, SubmittedSlowCommand
from ska_tango_base.control_model import ObsState
from ska_tmc_common.device_info import SdpSubarrayDeviceInfo
from ska_tmc_common.v1.tmc_base_leaf_device import TMCBaseLeafDevice
from tango import AttrWriteType, DebugIt
from tango.server import attribute, command, device_property, run

from ska_tmc_cspsubarrayleafnode import release
from ska_tmc_cspsubarrayleafnode.commands.set_csp_subarray_admin_mode import (
    SetAdminMode,
)

# pylint: disable = attribute-defined-outside-init


class TmcLeafNodeCspSubarray(TMCBaseLeafDevice):
    """
    CSP Subarray Leaf node monitors the CSP Subarray and issues control
    actions during an observation.

    :Device Properties:

        TelmodelSource:
            Property to provide telmodel source for array layout

        TelmodelPath:
            Property to provide telmodel path for array layout

        CspSubarrayFQDN:
            Property to provide FQDN of CSP Subarray Device

    :Device Attributes:

        commandExecuted:
            Stores command executed on the device.

        lastDeviceInfoChanged:
            Json String representing the last device changed in the
            internal model.

        cspSubarrayDevName:
            Stores CSP Subarray Device name.


    """

    # all instances

    def init_device(self):
        super().init_device()
        self._is_subsystem_available = False
        self._csp_subarray_obs_state = ObsState.EMPTY
        self._last_device_info_changed = None  # Instance attribute

    # -----------------
    # Device Properties
    # -----------------
    TelmodelSource = device_property(
        dtype="str",
        doc="Telmodel source for array layout.",
    )
    TelmodelPath = device_property(
        dtype="str",
        doc="Telmodel path for array layout.",
    )
    CspSubarrayFQDN = device_property(
        dtype="str", doc="FQDN of the CSP Subarray Tango Device ."
    )

    DelayModelTimeInAdvance = device_property(
        dtype="DevShort", default_value=30
    )
    DelayCadence = device_property(dtype="DevFloat")
    DelayValidityPeriod = device_property(dtype="DevFloat")
    CommandTimeOut = device_property(dtype="DevFloat", default_value=30)

    # -----------------
    # Attributes
    # -----------------

    lastDeviceInfoChanged = attribute(
        dtype="DevString",
        access=AttrWriteType.READ,
        doc="""Json String representing the last device changed in the
        internal model.""",
    )

    cspSubarrayDevName = attribute(
        dtype="DevString",
        access=AttrWriteType.READ_WRITE,
    )

    delayModel = attribute(
        dtype="DevString",
        access=AttrWriteType.READ,
    )

    isSubsystemAvailable = attribute(
        dtype="DevBoolean",
        access=AttrWriteType.READ,
    )

    isAdminModeEnabled = attribute(dtype=bool, access=AttrWriteType.READ_WRITE)
    cspSubarrayObsState = attribute(
        dtype=ObsState, access=AttrWriteType.READ_WRITE
    )
    cspSubarrayAdminMode = attribute(
        dtype=AdminMode,
        access=AttrWriteType.READ,
    )

    # ---------------
    # General methods
    # ---------------

    def update_device_callback(self, dev_info: SdpSubarrayDeviceInfo) -> None:
        """Updates the device callback"""
        self._last_device_info_changed = dev_info.to_json()
        self.push_change_archive_events(
            "lastDeviceInfoChanged", dev_info.to_json()
        )

    def update_delay_model_callback(self, delay_model):
        """Change event callback for delay model"""
        if delay_model != "":
            self.push_change_archive_events("delayModel", delay_model)

    def update_availablity_callback(self, csp_subarray_availability):
        """Change event callback for isSubsystemAvailable"""
        if csp_subarray_availability != self._is_subsystem_available:
            self._is_subsystem_available = csp_subarray_availability
            self.push_change_archive_events(
                "isSubsystemAvailable", csp_subarray_availability
            )

    def update_csp_subarray_obs_state_callback(
        self, obs_state: ObsState
    ) -> None:
        """Change event callback for cspSubarrayObsState"""
        self._csp_subarray_obs_state = obs_state
        self.push_change_archive_events(
            "cspSubarrayObsState", self._csp_subarray_obs_state
        )

    class InitCommand(TMCBaseLeafDevice.InitCommand):
        """
        A class for the TMC CspSubarrayLeafNode's init_device() method.
        """

        def do(self, *args, **kwargs) -> Tuple[ResultCode, str]:
            """
            Initializes the attributes and properties of the \
            CspSubarrayLeafNode \

            :return: A tuple containing a return code and a string message \
            indicating status. The message is \
            for information purpose only. \

            :rtype: (ReturnCode, str)

            """
            super().do()
            device = self._device

            device._build_state = (
                f"{release.name},{release.version},{release.description}"
            )
            device._health_state = HealthState.OK
            device._version_id = release.version
            device._admin_mode = AdminMode.ONLINE
            device._csp_subarray_leaf_node_admin_mode = AdminMode.ONLINE
            device._is_admin_mode_enabled = True
            device._last_device_info_changed = ""
            device._is_subsystem_available = False
            device.op_state_model.perform_action("component_on")
            for attribute_name in [
                "healthState",
                "isSubsystemAvailable",
                "cspSubarrayObsState",
                "delayModel",
                "cspSubarrayAdminMode",
                "isAdminModeEnabled",
            ]:
                device.set_change_event(attribute_name, True, False)
                device.set_archive_event(attribute_name, True)
            return (ResultCode.OK, "")

    def delete_device(self):
        # if the init is called more than once
        # I need to stop all threads
        if hasattr(self, "component_manager"):
            # pylint: disable=unnecessary-dunder-call
            self.component_manager.__del__()
            # pylint: enable=unnecessary-dunder-call

    def push_change_archive_events(
        self, attribute_name: str, value: Any
    ) -> None:
        """Method to push change event and archive event
        of the given attribute.

        Args:
            attribute_name (str): Attribute name
            value (Any): Attribute value need to be pushed
        """
        self.push_change_event(attribute_name, value)
        self.push_archive_event(attribute_name, value)

    def read_cspSubarrayDevName(self) -> str:
        """Returns the CspSubarrayDevName attribute value."""
        return self.component_manager.input_parameter.csp_subarray_dev_name

    def write_cspSubarrayDevName(self, value: str) -> None:
        """Set the cspsubarraydevname attribute."""
        self.component_manager.input_parameter.csp_subarray_dev_name = value

    def read_isAdminModeEnabled(self) -> bool:
        """Return the isAdminModeEnabled attribute value"""
        return self.component_manager.is_admin_mode_enabled

    def write_isAdminModeEnabled(self, value: bool):
        """Set the value of isAdminModeEnabled attribute"""
        self.component_manager.is_admin_mode_enabled = value

    def read_delayModel(self):
        """Returns the delay model."""
        return self.component_manager.delay_model

    def read_cspSubarrayAdminMode(self) -> int:
        """Read method for cspSubarrayAdminMode Attribute"""
        return self._csp_subarray_leaf_node_admin_mode

    # ------------------
    # Attributes methods
    # ------------------

    def read_lastDeviceInfoChanged(self) -> str:
        """Read method for Last Device info Changed"""
        return self._last_device_info_changed

    def read_isSubsystemAvailable(self) -> bool:
        """Read method for isSubsystemAvailable"""
        return self._is_subsystem_available

    def read_cspSubarrayObsState(self) -> ObsState:
        """Read method for cspSubarrayObsState"""
        return self._csp_subarray_obs_state

    def write_cspSubarrayObsState(self, value: ObsState) -> None:
        """Write method for cspSubarrayObsState"""
        self._csp_subarray_obs_state = value

    def update_admin_mode_callback(self, admin_mode: int):
        """Update cspSubarrayAdminMode attribute callback"""
        self._csp_subarray_leaf_node_admin_mode = admin_mode

        try:
            self.push_change_archive_events(
                "cspSubarrayAdminMode",
                self._csp_subarray_leaf_node_admin_mode,
            )
            self.logger.info(
                "Successfully updated and pushed cspSubarrayAdminMode "
                "attribute value to: %s",
                admin_mode,
            )
        except Exception as exception:
            self.logger.exception(
                "Exception while pushing event for "
                + "cspSubarrayAdminMode: %s",
                exception,
            )

    # --------
    # Commands
    # --------

    def is_AssignResources_allowed(self) -> bool:
        """
        Checks whether AssignResources command is allowed to be run in \
        current device state. \

        :return: True if AssignResources command is allowed to be run in \
        current device state \

        :rtype: boolean
        """
        return self.component_manager.is_command_allowed("AssignResources")

    @command(
        dtype_in="str",
        doc_in="The string in JSON format",
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def AssignResources(
        self, argin: str
    ) -> Tuple[List[ResultCode], List[str]]:
        """
        This command invokes the AssignResources() command on Csp Subarray.
        """
        handler = self.get_command_object("AssignResources")
        args = argin
        result_code, unique_id = handler(args)
        return [result_code], [unique_id]

    def is_ReleaseAllResources_allowed(self) -> bool:
        """
        Checks whether ReleaseResources command is allowed to be run in \
        current device state. \

        :return: True if ReleaseResources command is allowed to be run in \
        current device state. \

        :rtype: boolean
        """
        return self.component_manager.is_command_allowed("ReleaseAllResources")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def ReleaseAllResources(self) -> Tuple[List[ResultCode], List[str]]:
        """
        This command invokes ReleaseResources() command on Csp Subarray."""
        handler = self.get_command_object("ReleaseAllResources")
        result_code, unique_id = handler()
        return [result_code], [unique_id]

    def is_Configure_allowed(self) -> bool:
        """
        Checks whether Configure command is allowed to be run in
        current device state.
        :return: True if Configure command is allowed to be run in
        current device state

        :rtype: boolean
        """

        return self.component_manager.is_command_allowed("Configure")

    @command(
        dtype_in="str",
        doc_in="The string in JSON format",
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def Configure(self, argin: str) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes Configure command on Csp Subarray.
        """
        handler = self.get_command_object("Configure")
        args = argin
        result_code, unique_id = handler(args)
        return [result_code], [unique_id]

    def is_End_allowed(self) -> bool:
        """
        Checks whether End command is allowed to be run in current device
        state.

        return:
            True if End command is allowed to be run in current device
            state.

        rtype:
            boolean

        """
        return self.component_manager.is_command_allowed("End")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def End(self) -> Tuple[List[ResultCode], List[str]]:
        """This command invokes End command on Csp Subarray to end the current
        Scheduling block."""
        handler = self.get_command_object("End")
        result_code, unique_id = handler()
        return [result_code], [unique_id]

    def is_Scan_allowed(self):
        """
        Checks whether Scan command is allowed to be run in current device
        state.

        return:
          True if Scan command is allowed to be run in current device state.

        rtype:
            boolean

        """
        return self.component_manager.is_command_allowed("Scan")

    @command(
        dtype_in="str",
        doc_in="The JSON input string consists of SB ID.",
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def Scan(self, argin: str) -> Tuple[List[ResultCode], List[str]]:
        """
        This command invokes the Scan() command on Csp Subarray..
        """

        handler = self.get_command_object("Scan")
        args = argin
        result_code, unique_id = handler(args)
        return [result_code], [unique_id]

    def is_EndScan_allowed(self):
        """
        Checks whether EndScan command is allowed to be run in current device
        state.

        return:
            True if EndScan command is allowed to be run in current device
            state.

        rtype:
            boolean

        """
        return self.component_manager.is_command_allowed("EndScan")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def EndScan(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invokes EndScan command on Csp Subarray.

        """
        handler = self.get_command_object("EndScan")
        result_code, unique_id = handler()
        return [result_code], [unique_id]

    def is_Restart_allowed(self):
        """
        Checks whether Restart command is allowed to be run in current device
         state

         return:
             True if Restart command is allowed to be run in current device
             state

         rtype:
             boolean

         raises:
             DevFailed if this command is not allowed to be run in current
             device state

        """
        return self.component_manager.is_command_allowed("Restart")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def Restart(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invoke Restart command on Csp Subarray.
        """
        handler = self.get_command_object("Restart")
        result_code, unique_id = handler()
        return [result_code], [unique_id]

    def is_Abort_allowed(self):
        """
        Checks whether Abort command is allowed to be run in current device
        state

        return:
         True if Abort command is allowed to be run in current device state

        rtype:
            boolean

        raises:
            DevFailed if this command is not allowed to be run in current
            device state

        """
        return self.component_manager.is_command_allowed("Abort")

    @command(
        dtype_out="DevVarLongStringArray",
        doc_out="information-only string",
    )
    @DebugIt()
    def Abort(self) -> Tuple[List[ResultCode], List[str]]:
        """
        Invoke Abort command on Csp Subarray.
        """
        handler = self.get_command_object("Abort")
        result_code, unique_id = handler()
        return [result_code], [unique_id]

    @command(
        dtype_in=AdminMode,
        doc_in="The adminMode in enum format",
        dtype_out="DevVarLongStringArray",
    )
    @DebugIt()
    def SetAdminMode(self, argin: AdminMode):
        """
        This command sets the adminMode command on Csp Subarray.
        """
        handler = self.get_command_object("SetAdminMode")
        result_code, message = handler(argin)

        return [result_code], [message]

    def init_command_objects(self) -> None:
        """
        Initialises the command handlers for commands supported by this device.
        """
        super().init_command_objects()

        for command_name, method_name in [
            ("AssignResources", "assign_resources"),
            ("ReleaseAllResources", "release_all_resources"),
            ("Configure", "configure"),
            ("End", "end"),
            ("EndScan", "end_scan"),
            ("Scan", "scan"),
            ("Restart", "restart"),
        ]:
            self.register_command_object(
                command_name,
                SubmittedSlowCommand(
                    command_name,
                    self._command_tracker,
                    self.component_manager,
                    method_name,
                    logger=self.logger,
                ),
            )

        # as per base classes implementation, for SKABaseDevice
        # Abort is registered as AbortCommandsCommand
        self.register_command_object(
            "Abort",
            self.AbortCommandsCommand(self.component_manager, self.logger),
        )
        self.register_command_object(
            "SetAdminMode", SetAdminMode(self.logger, self.component_manager)
        )

    def create_component_manager(self):
        """Empty method for resolving lint issue #abstract-method"""


# ----------
# Run server
# ----------
def main(args=None, **kwargs):
    """
    Runs the TmcLeafNodeCspSubarray Tango device.
    :param args: Arguments internal to TANGO

    :param kwargs: Arguments internal to TANGO
    """
    return run((TmcLeafNodeCspSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
