"""
This file is part of the CspSubarrayLeafNode project and defines constants used
"""

# Constants of schema version used across CspSubarrayLeafNode

MID_DELAYMODEL_VERSION = "https://schema.skao.int/ska-mid-csp-delaymodel/3.0"
LOW_DELAYMODEL_VERSION = "https://schema.skao.int/ska-low-csp-delaymodel/1.0"
YPOL_OFFSET_NS = 0.0
