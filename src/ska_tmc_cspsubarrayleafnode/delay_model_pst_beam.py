"""DelayManager class for PST beam calculation."""
import datetime
import json
import time
from datetime import timedelta

import katpoint
import numpy as np
from astropy.time import Time

from ska_tmc_cspsubarrayleafnode.const import (
    LOW_DELAYMODEL_VERSION,
    YPOL_OFFSET_NS,
)
from ska_tmc_cspsubarrayleafnode.delay_model_low import DelayManagerLow


class DelayManagerPSTBeam(DelayManagerLow):
    """A class that calculates geometric delays for PST Beams for SKA Low"""

    def __init__(
        self,
        csp_sln_cm,
        telmodel_source: str,
        telmodel_path: str,
        pst_delay_model_time_in_advance: int,
        pst_delay_cadence: int,
        pst_delay_validity_period: int,
        pst_beam_id: int,
        ra: float,
        dec: float,
        logger,
    ):
        """
        Initialize the DelayManagerPSTBeam class.

        :param csp_sln_cm: CSP Subarray Leaf Node Component Manager
        :param pst_delay_model_time_in_advance: Time in advance to calculate
            delays
        :param pst_delay_cadence: Cadence for delay calculation
        :param pst_delay_validity_period: Validity period for calculated delays
        :param pst_beam_id: The PST beam ID for which delay is calculated
        :param ra: The right ascension value of the target to calculate delay
        :param dec: The declination value of the target to calculate delay
        :param logger: Logger for logging information and errors
        """
        super().__init__(
            csp_sln_cm,
            telmodel_source,
            telmodel_path,
            pst_delay_model_time_in_advance,
            pst_delay_cadence,
            pst_delay_validity_period,
            logger,
        )
        self.station_names = None
        self._pst_delay_cadence: int = pst_delay_cadence
        self._pst_delay_validity_period: int = pst_delay_validity_period
        self._pst_delay_model_time_in_advance = pst_delay_model_time_in_advance
        self._pst_beam_id = pst_beam_id
        self._ra = ra
        self._dec = dec
        self.delay_model_pst_beam_json: dict = {}
        self._DELAY_UPDATE_INTERVAL = pst_delay_cadence

    def calculate_geometric_delays(self, time_t0, antenna_names):
        """
        Calculate geometric delay values using the KATPoint library.

        :param time_t0: Initial time for delay calculation
        :param antenna_names: List of antenna names
        :return: Dictionary containing fifth order polynomial coefficients
          per antenna
        """
        delay_corrections_h_array_dict = {}
        # Create arrays of 20 points array having t0, and 10 points between
        # t0 and validity period 4 points are before t0 and
        # 5 points are comsidered after t0+validity period
        timesteps = self._pst_delay_validity_period / 10
        delta_times = [timestep * timesteps for timestep in range(-4, 16)]
        timestamp_array = [
            (time_t0 + timedelta(seconds=delta_times[i]))
            for i in range(len(delta_times))
        ]

        # Calculate geometric delay values
        # Two values per antenna per timestamp received: horizontal and
        # vertical delay values.So if there are n number of antennas and t
        # timestamps, the matrix received id of dimension (2n x t)
        delays = self.delay_correction_object.delays(
            katpoint.Target(f"radec , {self._ra} , {self._dec}"),
            timestamp_array,
        )

        # Convert delays in seconds to 5th order polynomial coefficients
        for antenna, antenna_name in enumerate(antenna_names):
            # Array including horizontal delay values per antenna for the
            # given timestamps
            horizontal_delays = np.array(delays[antenna * 2])
            # Fit polynomial to the values over 50-second range
            # Calculating fifth order polynomial coefficient
            polynomial = np.polynomial.Polynomial.fit(
                delta_times, horizontal_delays, 5
            )
            polynomial_coefficients = polynomial.convert().coef
            delay_corrections_h_array_dict[
                antenna_name
            ] = polynomial_coefficients

        return delay_corrections_h_array_dict

    def calculate_delay_model(self):
        """
        Calculate delays using the KatPoint library and update the delay model
        PST Beam JSON.
        """
        try:
            # As per ADR-78 Updated time format to TAI format.
            SKA_EPOCH = "1999-12-31T23:59:28Z"
            ska_epoch_utc = Time(SKA_EPOCH, scale="utc")
            ska_epoch_tai = ska_epoch_utc.unix_tai

            if self.start_time_for_delay_calculation is None:
                self.start_time_for_delay_calculation = (
                    self.csp_sln_cm.delay_start_time
                    + timedelta(seconds=self._pst_delay_model_time_in_advance)
                )
            else:
                self.start_time_for_delay_calculation += timedelta(
                    seconds=self._pst_delay_cadence
                )
            self.logger.debug(
                "The start time  is %s", self.csp_sln_cm.delay_start_time
            )
            time_t0_utc = Time(
                self.start_time_for_delay_calculation, scale="utc"
            )
            offset_since_epoch = time_t0_utc.unix_tai - ska_epoch_tai

            delay_corrections_h_array_dict = self.calculate_geometric_delays(
                self.start_time_for_delay_calculation, self.station_names
            )

            self.delay_model_pst_beam_json[
                "interface"
            ] = LOW_DELAYMODEL_VERSION
            self.delay_model_pst_beam_json[
                "start_validity_sec"
            ] = offset_since_epoch
            self.delay_model_pst_beam_json[
                "cadence_sec"
            ] = self._pst_delay_cadence
            self.delay_model_pst_beam_json[
                "validity_period_sec"
            ] = self._pst_delay_validity_period
            self.delay_model_pst_beam_json[
                "config_id"
            ] = self.csp_sln_cm.config_id
            self.delay_model_pst_beam_json[
                "station_beam"
            ] = self.csp_sln_cm.subarray_beam_id
            self.delay_model_pst_beam_json[
                "subarray"
            ] = self.csp_sln_cm.subarray_id

            delay_details = []

            # station is a list of station IDs and substation IDs
            # e.g [[1,1],[2,1]]
            for station in self.csp_sln_cm.get_station_substation_ids():
                station_delay_coeffs = delay_corrections_h_array_dict[
                    str(station[0])
                ]
                delay_details_dict = {}
                delay_coeff_array = list(station_delay_coeffs)
                self.logger.debug(
                    "Delay Coefficient is: %s", delay_coeff_array
                )
                delay_coeff_array_ns = [
                    coeff * 1e9 for coeff in delay_coeff_array
                ]
                delay_details_dict["station_id"] = int(station[0])
                delay_details_dict["substation_id"] = int(station[-1])
                delay_details_dict["xypol_coeffs_ns"] = delay_coeff_array_ns
                delay_details_dict["ypol_offset_ns"] = YPOL_OFFSET_NS
                delay_details.append(delay_details_dict)

            self.delay_model_pst_beam_json[
                "station_beam_delays"
            ] = delay_details
            self.logger.info(
                "PST Beam Delay Model JSON : %s",
                self.delay_model_pst_beam_json,
            )
        except (AttributeError, ValueError, TypeError) as exception:
            self.logger.exception(exception)

    def delay_model_handler(self, delay_cadence):
        """
        Handles the periodic calculation of the delay model for PST Beams.

        Args:
            argin (int): Delay model update interval in seconds.
        """
        cadence_seconds = float(delay_cadence)
        while not self._stop_delay_model_event.is_set():
            if self.next_polynomial_time is None:
                self.next_polynomial_time = time.time()
            self.calculate_delay_model()
            # update the attribute
            with self.delay_model_lock:
                self.csp_sln_cm.delay_model_pst_beam = json.dumps(
                    self.delay_model_pst_beam_json
                )
            self.next_polynomial_time += cadence_seconds
            next_timestamp = datetime.datetime.utcfromtimestamp(
                self.next_polynomial_time
            ).strftime("%Y-%m-%d %H:%M:%S")
            self.logger.debug("The next polynomial time is %s", next_timestamp)
            # How long to wait before next polynomial is due
            wait_interval = self.next_polynomial_time - time.time()
            # graceful fail if polynomial can't be calculated in time
            if wait_interval < 0.0:
                miss_count = 0
                self.logger.debug(
                    "Wait interval and next polynomial time is %s and %s",
                    wait_interval,
                    self.next_polynomial_time,
                )
                while wait_interval < 0.0:
                    self.next_polynomial_time += cadence_seconds
                    wait_interval += cadence_seconds
                    miss_count += 1
                self.logger.warning(
                    "Missed %d polynomials for PST beams.", miss_count
                )
            # wait for timer event
            self._stop_delay_model_event.wait(wait_interval)
        self.logger.info("Stop event received. Thread exit.")
