"""
Csp Subarray Leaf node mid monitors the CSP Subarray and issues control
actions during an observation.
It also acts as a CSP contact point for Subarray Node for observation
execution for TMC.
"""
from tango.server import run

from ska_tmc_cspsubarrayleafnode.csp_subarray_leaf_node import (
    TmcLeafNodeCspSubarray,
)
from ska_tmc_cspsubarrayleafnode.manager import CspSLNComponentManagerMid
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterMid

__all__ = ["MidTmcLeafNodeCspSubarray", "main"]


class MidTmcLeafNodeCspSubarray(TmcLeafNodeCspSubarray):
    """
    Tango device class for tmc csp subarray leaf node low
    """

    def create_component_manager(self) -> CspSLNComponentManagerMid:
        cm = CspSLNComponentManagerMid(
            self.TelmodelSource,
            self.TelmodelPath,
            InputParameterMid(None, device_name=self.CspSubarrayFQDN),
            delay_cadence=self.DelayCadence,
            delay_validity_period=self.DelayValidityPeriod,
            logger=self.logger,
            communication_state_callback=None,
            component_state_callback=None,
            _update_delay_model_callback=self.update_delay_model_callback,
            _update_csp_subarray_obs_state_callback=(
                self.update_csp_subarray_obs_state_callback
            ),
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            adapter_timeout=self.AdapterTimeOut,
            command_timeout=self.CommandTimeOut,
            _update_availablity_callback=self.update_availablity_callback,
            _update_admin_mode_callback=self.update_admin_mode_callback,
            delay_model_time_in_advance=self.DelayModelTimeInAdvance,
        )
        return cm


def main(args=None, **kwargs):
    """
    Runs the MidTmcLeafNodeCspSubarray.
    :param args: Arguments internal to TANGO

    :param kwargs: Arguments internal to TANGO

    :return: MidTmcLeafNodeCspSubarray TANGO object.
    """
    return run((MidTmcLeafNodeCspSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
