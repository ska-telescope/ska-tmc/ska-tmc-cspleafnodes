"""
Component Manager class for CSP Subarray Leaf Node Mid
"""
# pylint: disable=W0223(abstract-method)
import logging

from ska_ser_logging.configuration import configure_logging
from ska_tmc_common.enum import LivelinessProbeType

from ska_tmc_cspsubarrayleafnode.manager.component_manager import (
    CspSLNComponentManager,
)

configure_logging()
LOGGER: logging.Logger = logging.getLogger(__name__)


class CspSLNComponentManagerMid(CspSLNComponentManager):
    """
    A component manager for The CSP Subarray Leaf Node Mid component.
    """

    def __init__(
        self,
        telmodel_source,
        telmodel_path,
        _input_parameter,
        delay_cadence: int,
        delay_validity_period: int,
        logger: logging.Logger = LOGGER,
        _liveliness_probe=LivelinessProbeType.SINGLE_DEVICE,
        _event_receiver: bool = True,
        communication_state_callback=None,
        component_state_callback=None,
        _update_delay_model_callback=None,
        _update_csp_subarray_obs_state_callback=None,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        adapter_timeout: int = 30,
        _update_availablity_callback=None,
        _update_admin_mode_callback=None,
        delay_model_time_in_advance: int = 30,
        command_timeout: int = 30,
    ) -> None:
        """
        Initialise a new ComponentManager instance.

        :param logger: a logger for this component manager
        :param _component: allows setting of the component to be
            managed; for testing purposes only
        """
        super().__init__(
            telmodel_source,
            telmodel_path,
            _input_parameter,
            delay_cadence=delay_cadence,
            delay_validity_period=delay_validity_period,
            logger=logger,
            _liveliness_probe=_liveliness_probe,
            _event_receiver=_event_receiver,
            communication_state_callback=communication_state_callback,
            component_state_callback=component_state_callback,
            _update_delay_model_callback=_update_delay_model_callback,
            _update_csp_subarray_obs_state_callback=(
                _update_csp_subarray_obs_state_callback
            ),
            _update_admin_mode_callback=_update_admin_mode_callback,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
            adapter_timeout=adapter_timeout,
            _update_availablity_callback=_update_availablity_callback,
            delay_model_time_in_advance=delay_model_time_in_advance,
            command_timeout=command_timeout,
        )
        self.receptor_ids_str = []

    def get_receptor_ids(self):
        """
        Returns the receptor IDs.

        :return: receptor IDs
        :rtype: list
        """
        return self.receptor_ids_str
