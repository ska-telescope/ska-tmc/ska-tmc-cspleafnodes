"""
Component Manager class for CSP Subarray Leaf Node Low
"""
# pylint: disable=W0223(abstract-method)
import logging
from datetime import datetime
from typing import Any

from ska_ser_logging.configuration import configure_logging
from ska_tmc_common.enum import LivelinessProbeType

from ska_tmc_cspsubarrayleafnode.delay_model_pst_beam import (
    DelayManagerPSTBeam,
)
from ska_tmc_cspsubarrayleafnode.manager.component_manager import (
    CspSLNComponentManager,
)

configure_logging()
LOGGER: logging.Logger = logging.getLogger(__name__)


class CspSLNComponentManagerLow(CspSLNComponentManager):
    """
    A component manager for The CSP Subarray Leaf Node Low component.
    """

    def __init__(
        self,
        telmodel_source,
        telmodel_path,
        _input_parameter,
        delay_cadence: int,
        pst_delay_cadence: int,
        delay_validity_period: int,
        pst_delay_validity_period: int,
        logger: logging.Logger = LOGGER,
        _liveliness_probe=LivelinessProbeType.SINGLE_DEVICE,
        _event_receiver: bool = True,
        communication_state_callback=None,
        component_state_callback=None,
        _update_delay_model_callback=None,
        _update_delay_model_pst_beam_callback=None,
        _update_csp_subarray_obs_state_callback=None,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        adapter_timeout: int = 30,
        _update_availablity_callback=None,
        _update_admin_mode_callback=None,
        delay_model_time_in_advance: int = 600,
        pst_delay_model_time_in_advance: int = 600,
        command_timeout: int = 30,
    ) -> None:
        """
        Initialise a new ComponentManager instance.

        :param logger: a logger for this component manager
        :param _component: allows setting of the component to be
            managed; for testing purposes only
        """
        super().__init__(
            telmodel_source,
            telmodel_path,
            _input_parameter,
            delay_cadence=delay_cadence,
            delay_validity_period=delay_validity_period,
            logger=logger,
            _liveliness_probe=_liveliness_probe,
            _event_receiver=_event_receiver,
            _update_admin_mode_callback=_update_admin_mode_callback,
            communication_state_callback=communication_state_callback,
            component_state_callback=component_state_callback,
            _update_delay_model_callback=_update_delay_model_callback,
            _update_csp_subarray_obs_state_callback=(
                _update_csp_subarray_obs_state_callback
            ),
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
            adapter_timeout=adapter_timeout,
            _update_availablity_callback=_update_availablity_callback,
            delay_model_time_in_advance=delay_model_time_in_advance,
            command_timeout=command_timeout,
        )
        self.station_ids: list = []
        self.station_substation_ids: list[list] = [[]]
        self.subarray_beam_id: int = 0
        self._delay_model_pst_beam: str = ""
        self._update_delay_model_pst_beam_callback = (
            _update_delay_model_pst_beam_callback
        )
        self.pst_delay_cadence = pst_delay_cadence
        self.pst_delay_validity_period = pst_delay_validity_period
        self.pst_delay_model_time_in_advance = pst_delay_model_time_in_advance
        self.number_of_pst_beams: int = 0
        self.pst_beam_id: int = 0
        self.pst_beam_right_ascension: float = 0.0
        self.pst_beam_declination: float = 0.0
        self.delay_manager_pst_beam = DelayManagerPSTBeam(
            self,
            telmodel_source,
            telmodel_path,
            pst_delay_model_time_in_advance,
            pst_delay_cadence,
            pst_delay_validity_period,
            self.pst_beam_id,
            self.pst_beam_right_ascension,
            self.pst_beam_declination,
            logger,
        )
        self.delay_start_time: datetime = datetime.today()

    def _invoke_delay_model_pst_beam_callback(self):
        """This method calls delay model pst beam callback"""
        if self._update_delay_model_pst_beam_callback is not None:
            self._update_delay_model_pst_beam_callback(
                self._delay_model_pst_beam
            )

    @property
    def delay_model_pst_beam(self) -> str:
        """
        Returns the delayModelPSTBeam attribute.

        :return: the delayModelPSTBeam
        :rtype: str
        """
        return self._delay_model_pst_beam

    @delay_model_pst_beam.setter
    def delay_model_pst_beam(self, value: str):
        """
        Sets the delayModelPSTBeam value

        :param value: the new subarray observation state
        :type value: str
        """
        if self._delay_model_pst_beam != value:
            self._delay_model_pst_beam = value
            self._invoke_delay_model_pst_beam_callback()

    def get_station_substation_ids(self) -> list[list[Any]]:
        """
        Returns the stations and sub-stations IDs
        received under lowcbf configurations.

        :return: station and sub-station IDs
        :rtype: list[list]
        """
        return self.station_substation_ids

    def get_station_ids(self) -> list:
        """
        Returns the station IDs received under
        lowcbf configurations.

        :return: station IDs
        :rtype: list
        """
        return self.station_ids
