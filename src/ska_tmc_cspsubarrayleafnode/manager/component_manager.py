"""
Component Manager class for CSP Subarray Leaf Node
"""
import json
import logging
import threading
import time
from typing import Any, Callable, Optional, Tuple, Union

from ska_control_model import AdminMode
from ska_ser_logging.configuration import configure_logging
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.device_info import SubArrayDeviceInfo
from ska_tmc_common.enum import LivelinessProbeType
from ska_tmc_common.exceptions import (
    CommandNotAllowed,
    DeviceUnresponsive,
    InvalidObsStateError,
)
from ska_tmc_common.lrcr_callback import LRCRCallback
from ska_tmc_common.v1.tmc_component_manager import TmcLeafNodeComponentManager
from tango import DevState

from ska_tmc_cspsubarrayleafnode.commands.abort_command import Abort
from ska_tmc_cspsubarrayleafnode.commands.assign_resources_command import (
    AssignResources,
)
from ska_tmc_cspsubarrayleafnode.commands.configure_command import Configure
from ska_tmc_cspsubarrayleafnode.commands.end_command import End
from ska_tmc_cspsubarrayleafnode.commands.end_scan_command import EndScan

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.release_all_resources_command import (  # noqa: E501
    ReleaseAllResources,
)

# pylint: enable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.restart_command import Restart
from ska_tmc_cspsubarrayleafnode.commands.scan_command import Scan
from ska_tmc_cspsubarrayleafnode.delay_model_low import DelayManagerLow
from ska_tmc_cspsubarrayleafnode.delay_model_mid import DelayManagerMid
from ska_tmc_cspsubarrayleafnode.manager.event_receiver import (
    CspSLNEventReceiver,
)
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterMid

configure_logging()
LOGGER: logging.Logger = logging.getLogger(__name__)


# pylint: disable=too-many-instance-attributes
class CspSLNComponentManager(TmcLeafNodeComponentManager):
    """
    A component manager for The CSP Subarray Leaf Node component.

    It supports:

    * Monitoring its component, e.g. detect that it has been turned off
      or on
    """

    def __init__(
        self,
        telmodel_source,
        telmodel_path,
        _input_parameter,
        delay_cadence: int,
        delay_validity_period: int,
        logger: logging.Logger = LOGGER,
        _liveliness_probe=LivelinessProbeType.SINGLE_DEVICE,
        _event_receiver: bool = True,
        communication_state_callback: Optional[Callable[..., None]] = None,
        component_state_callback: Optional[Callable[..., None]] = None,
        _update_delay_model_callback=None,
        _update_csp_subarray_obs_state_callback: Optional[
            Callable[..., None]
        ] = None,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
        liveliness_check_period: int = 1,
        adapter_timeout: int = 30,
        _update_availablity_callback: Optional[Callable[..., None]] = None,
        _update_admin_mode_callback: Optional[Callable[[Any], None]] = None,
        delay_model_time_in_advance: int = 30,
        command_timeout: int = 15,
    ) -> None:
        """
        Initialise a new ComponentManager instance.

        :param op_state_model: the op state model used by this component
            manager
        :param logger: a logger for this component manager
        :param _component: allows setting of the component to be
            managed; for testing purposes only
        """
        self._input_parameter = _input_parameter
        self.update_availablity_callback = _update_availablity_callback
        self.logger = logger

        super().__init__(
            logger,
            _liveliness_probe=_liveliness_probe,
            _event_receiver=False,
            communication_state_callback=communication_state_callback,
            component_state_callback=component_state_callback,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
            liveliness_check_period=liveliness_check_period,
        )

        self._device: SubArrayDeviceInfo = SubArrayDeviceInfo(
            self.input_parameter.csp_subarray_dev_name, False
        )

        if _liveliness_probe:
            self.start_liveliness_probe(_liveliness_probe)

        if _event_receiver:
            evet_subscribe_check_period = event_subscription_check_period
            self._event_receiver = CspSLNEventReceiver(
                self,
                logger,
                proxy_timeout=proxy_timeout,
                event_subscription_check_period=evet_subscribe_check_period,
            )
            self._event_receiver.start()
        self.rlock = threading.RLock()
        self.adapter_timeout = adapter_timeout
        self.command_timeout = command_timeout
        self.target = None
        self.command_id: str = ""
        self._delay_model = ""
        self._is_admin_mode_enabled: bool = True
        self.config_id: str = ""
        self.subarray_id: int = None
        self._update_delay_model_callback = _update_delay_model_callback
        self._update_csp_subarray_obs_state_callback = (
            _update_csp_subarray_obs_state_callback
        )
        self._update_admin_mode_callback = _update_admin_mode_callback
        self.abort_event = threading.Event()
        self.long_running_result_callback = LRCRCallback(self.logger)
        self.supported_commands = (
            "AssignResources",
            "ReleaseAllResources",
            "Configure",
            "GoToIdle",
            "Scan",
            "EndScan",
        )
        self.tracker_thread = None
        if isinstance(self.input_parameter, InputParameterMid):
            self.delay_manager = DelayManagerMid(
                self,
                telmodel_source,
                telmodel_path,
                delay_model_time_in_advance,
                delay_cadence,
                delay_validity_period,
                logger,
            )
        else:
            self.delay_manager = DelayManagerLow(
                self,
                telmodel_source,
                telmodel_path,
                delay_model_time_in_advance,
                delay_cadence,
                delay_validity_period,
                logger,
            )
        self.scan_result: ResultCode = None

    @property
    def input_parameter(self):
        """
        Return the input parameter
        :return: input parameter
        :rtype: InputParameter
        """
        return self._input_parameter

    # pylint:disable = signature-differs
    def on(self, task_callback):
        "Empty method for resolving lint issue"

    def off(self, task_callback):
        "Empty method for resolving lint issue"

    def standby(self, task_callback):
        "Empty method for resolving lint issue"

    def start_communicating(self):
        "Empty method for resolving lint issue"

    def stop_communicating(self):
        "Empty method for resolving lint issue"

    def stop(self) -> None:
        """Stops the event receiver"""
        self._event_receiver.stop()

    def get_device(self) -> SubArrayDeviceInfo:
        """
        Return the device info

        :param None:
        :return: a device info
        :rtype: SubArrayDeviceInfo
        """
        return self._device

    def update_event_failure(self, device_name: str) -> None:
        with self.lock:
            dev_info = self.get_device()
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)

    def update_device_obs_state(
        self, device_name: str, obs_state: ObsState
    ) -> None:
        """
        Update a monitored device obs state,
        and call the relative callbacks if available
        :param obs_state: obs state of the device
        :type obs_state: ObsState
        """
        with self.rlock:
            dev_info = self.get_device()
            dev_info.obs_state = obs_state
            dev_info.last_event_arrived = time.time()
            dev_info.update_unresponsive(False)
            self.logger.info(
                "Obs State value updated to :%s", ObsState(obs_state).name
            )
            if self._update_csp_subarray_obs_state_callback:
                self._update_csp_subarray_obs_state_callback(obs_state)
            self.observable.notify_observers(attribute_value_change=True)

    def get_obs_state(self):
        """
        Get Current device obsState
        """
        return self.get_device().obs_state

    def update_command_result(self, device_name: str, value: str) -> None:
        """Updates the long running command result callback"""
        self.logger.info(
            "Received longRunningCommandResult event for device: %s, "
            + "with value: %s",
            device_name,
            value,
        )
        if value == ("", "") or not value:
            return
        try:
            unique_id, result_code_message = value
            result_code, message = json.loads(result_code_message)

            if result_code in [
                ResultCode.FAILED,
                ResultCode.NOT_ALLOWED,
                ResultCode.REJECTED,
            ]:
                if unique_id.endswith(self.supported_commands):
                    self.logger.debug(
                        "Updating LRCR Callback with value: %s for %s"
                        + "for device: %s",
                        value,
                        unique_id,
                        device_name,
                    )
                    self.long_running_result_callback(
                        self.command_id,
                        ResultCode.FAILED,
                        exception_msg=message,
                    )
                    self.observable.notify_observers(command_exception=True)
            if unique_id.endswith("Scan") and result_code == ResultCode.OK:
                self.scan_result = ResultCode.OK
                self.observable.notify_observers(attribute_value_change=True)

        except Exception as exception:
            self.logger.error(
                "Exception has occurred while processing"
                "long running command result event: %s",
                exception,
            )

    def is_command_allowed_callable(self, command_name: str):
        """
        Args:
            command_name (str): Name for the command for which the is_allowed
                check need to be applied.
        """
        self._check_if_csp_sa_is_responsive()

        def check_obs_state():
            """Return whether the command may be called in the current state.

            Returns:
                bool: whether the command may be called in the current device
                state
            """
            command_allowed_states = {
                "AssignResources": [ObsState.EMPTY, ObsState.IDLE],
                "ReleaseAllResources": [ObsState.IDLE],
                "Configure": [ObsState.IDLE, ObsState.READY],
                "End": [ObsState.READY],
                "Scan": [ObsState.READY],
                "EndScan": [ObsState.SCANNING],
                "Restart": [ObsState.FAULT, ObsState.ABORTED],
            }

            allowed_states = command_allowed_states.get(command_name, [])
            return self.get_device().obs_state in allowed_states

        return check_obs_state

    def _invoke_delay_model_callback(self):
        """This method calls delay model callback"""
        if self._update_delay_model_callback is not None:
            self._update_delay_model_callback(self._delay_model)

    @property
    def delay_model(self) -> str:
        """
        Returns the delayModel attribute.

        :return: the delayModel
        :rtype: str
        """
        return self._delay_model

    @delay_model.setter
    def delay_model(self, value: str):
        """
        Sets the delay model value

        :param value: the new subarray observation state
        :type value: str
        """
        if self._delay_model != value:
            self._delay_model = value
            self._invoke_delay_model_callback()

    def _check_if_csp_sa_is_responsive(self) -> None:
        """Checks if CspSubarray device is responsive."""
        if self._device is None or self._device.unresponsive:
            raise DeviceUnresponsive(
                f"{self.input_parameter.csp_subarray_dev_name} not available"
            )

    def assign_resources(
        self, argin: str, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submit the AssignResources command in queue.

        :return: a result code and message
        """
        assign_resources_command = AssignResources(self, logger=self.logger)
        self.command_id = f"{time.time()}-{AssignResources.__name__}"
        task_status, response = self.submit_task(
            assign_resources_command.assign_resources,
            kwargs={"argin": argin},
            is_cmd_allowed=self.is_command_allowed_callable("AssignResources"),
            task_callback=task_callback,
        )
        return task_status, response

    def configure(
        self, argin: str, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submit the Configure command in queue.

        :return: a result code and message
        """
        configure_command = Configure(self, logger=self.logger)
        self.command_id = f"{time.time()}-{Configure.__name__}"
        task_status, response = self.submit_task(
            configure_command.configure,
            kwargs={"argin": argin},
            is_cmd_allowed=self.is_command_allowed_callable("Configure"),
            task_callback=task_callback,
        )
        return task_status, response

    def release_all_resources(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submit the ReleaseAllResources command in queue.

        :return: a result code and message
        """
        release_all_resources_command = ReleaseAllResources(
            self, logger=self.logger
        )
        self.command_id = f"{time.time()}-{ReleaseAllResources.__name__}"
        task_status, response = self.submit_task(
            release_all_resources_command.release_all_resources,
            is_cmd_allowed=self.is_command_allowed_callable(
                "ReleaseAllResources"
            ),
            task_callback=task_callback,
        )
        return task_status, response

    def end(self, task_callback: TaskCallbackType) -> Tuple[TaskStatus, str]:
        """
        Submit the End command in queue.

        :return: a result code and message
        """
        end_command = End(self, logger=self.logger)
        task_status, response = self.submit_task(
            end_command.end,
            args=[self.logger],
            is_cmd_allowed=self.is_command_allowed_callable("End"),
            task_callback=task_callback,
        )
        return task_status, response

    def scan(
        self, argin: str, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submit the Scan command in queue.

        :return: a result code and message
        """
        scan_command = Scan(self, logger=self.logger)
        task_status, response = self.submit_task(
            scan_command.scan,
            kwargs={"argin": argin},
            is_cmd_allowed=self.is_command_allowed_callable("Scan"),
            task_callback=task_callback,
        )
        return task_status, response

    def end_scan(
        self, task_callback: TaskCallbackType
    ) -> Tuple[TaskStatus, str]:
        """
        Submit the EndScan command in queue.

        :return: a result code and message
        """
        end_scan_command = EndScan(self, logger=self.logger)
        task_status, response = self.submit_task(
            end_scan_command.end_scan,
            args=[self.logger],
            is_cmd_allowed=self.is_command_allowed_callable("EndScan"),
            task_callback=task_callback,
        )
        return task_status, response

    # pylint: disable=arguments-differ
    def abort_commands(self):
        """
        Invokes Abort command on lower level Csp Subarray
        and changes the obsstate
        """
        abort_command = Abort(
            self,
            logger=self.logger,
        )
        self.abort_event.set()
        self.observable.notify_observers(attribute_value_change=True)
        result_code, message = abort_command.invoke_abort()
        self.abort_event.clear()
        self.logger.info("Abort event cleared")
        return result_code, message

    # pylint: enable=arguments-differ

    def restart(self, task_callback: Optional[Callable] = None) -> tuple:
        """
        Submit the Restart command in queue.

        :return: a result code and message
        """
        restart_command = Restart(self, logger=self.logger)
        task_status, response = self.submit_task(
            restart_command.restart,
            args=[self.logger],
            is_cmd_allowed=self.is_command_allowed_callable("Restart"),
            task_callback=task_callback,
        )
        return task_status, response

    def raise_invalid_obsstate_error(
        self, command_name: str
    ) -> InvalidObsStateError:
        """
        checking the InvalidObsState of Csp Subarray device
        :param command_name: The name of command
        :type command_name: str
        """
        message = (
            f"{command_name} command is not allowed in current "
            + "observation state on device "
            + f"{self.input_parameter.csp_subarray_dev_name}\n"
            + "Reason: The current observation state of "
            + f"{self.input_parameter.csp_subarray_dev_name} "
            + f"for observation is {self.get_device().obs_state}\n"
            + f"The {command_name} command has NOT been executed. "
            + "This device will continue with normal operation."
        )
        raise InvalidObsStateError(message)

    def is_command_allowed(
        self, command_name: str
    ) -> Union[
        bool, InvalidObsStateError, DeviceUnresponsive, CommandNotAllowed
    ]:
        """
        Checks whether this command is allowed.
        It checks that the device is in a state to perform this command and
        that all the component needed for the operation are not unresponsive.

        :param command_name: name of the command
        :type command_name: str
        :return: True if this command is allowed

        :rtype: boolean
        """
        self._check_if_csp_sa_is_responsive()
        if self.op_state_model.op_state in [
            DevState.FAULT,
            DevState.UNKNOWN,
            DevState.DISABLE,
        ]:
            raise CommandNotAllowed(
                "Command is not allowed in current state "
                + f"{self.op_state_model.op_state.name}",
            )
        if command_name == "Abort":
            if self.get_device().obs_state not in [
                ObsState.SCANNING,
                ObsState.CONFIGURING,
                ObsState.RESOURCING,
                ObsState.IDLE,
                ObsState.READY,
            ]:
                self.raise_invalid_obsstate_error(command_name)
        return True

    def set_target(self, target):
        """
        Set the target with KatPoint Target object

        """
        self.target = target

    def update_exception_for_unresponsiveness(
        self, device_info: SubArrayDeviceInfo, exception: str
    ) -> None:
        """Set a device to failed and call the relative callback if available
        :param device_info: a device info
        :type device_info: DeviceInfo
        :param exception: an exception
        :type: Exception
        """
        with self.rlock:
            device_info.update_unresponsive(True, exception)
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(False)

    # pylint: disable=unused-argument
    def update_responsiveness_info(self, device_name: str) -> None:
        """
        Update a device with the correct availability information.

        :param dev_name: name of the device
        :type dev_name: str
        """
        with self.rlock:
            self._device.update_unresponsive(False, "")
            if self.update_availablity_callback is not None:
                self.update_availablity_callback(True)

    def update_device_admin_mode(
        self, device_name: str, admin_mode: AdminMode
    ) -> None:
        """
        Update a monitored device admin mode,
        and call the relative callbacks if available
        :param admin_state: admin mode of the device
        :type admin_mode: AdminMode
        """
        super().update_device_admin_mode(device_name, admin_mode)
        self.logger.info(
            "Admin Mode value updated to :%s", AdminMode(admin_mode).name
        )
        if self._update_admin_mode_callback:
            self._update_admin_mode_callback(admin_mode)

    def is_scan_completed(self):
        """
        A function to check whether Scan Command is completed succesfully."""
        if (
            self.get_device().obs_state == ObsState.SCANNING
            and self.scan_result == ResultCode.OK
        ):
            return True
        return False

    def __del__(self: "CspSLNComponentManager") -> None:
        """
        CSPSLN Component Manager Destructor method.
        This method is automatically called when the object is about to be
        destroyed.
        """
        self.delay_manager.stop()
