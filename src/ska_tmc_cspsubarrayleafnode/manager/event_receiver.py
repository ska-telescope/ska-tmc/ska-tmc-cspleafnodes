"""Event Receiver for CSP Subarray Leaf Node"""
import logging
from concurrent import futures
from time import sleep
from typing import Dict, Optional

import tango
from ska_ser_logging.configuration import configure_logging
from ska_tango_base.control_model import ObsState
from ska_tmc_common.device_info import SubArrayDeviceInfo
from ska_tmc_common.v1.event_receiver import EventReceiver

configure_logging()
LOGGER: logging.Logger = logging.getLogger(__name__)


class CspSLNEventReceiver(EventReceiver):
    """
    The CSP SLNEventReceiver class has the responsibility to receive events
    from the CSP Subarray managed by the CSP Subarray Leaf Node.

    The ComponentManager uses the handle events methods
    for the attribute of interest.
    For each of them a callback is defined.

    """

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
        proxy_timeout: int = 500,
        event_subscription_check_period: int = 1,
    ):
        super().__init__(
            component_manager=component_manager,
            logger=logger,
            proxy_timeout=proxy_timeout,
            event_subscription_check_period=event_subscription_check_period,
        )
        self._event_subscription_check_period = event_subscription_check_period
        self._stop = False
        self._component_manager = component_manager

    def run(self) -> None:
        while not self._stop:
            with futures.ThreadPoolExecutor() as executor:
                devInfo = self._component_manager.get_device()
                if devInfo.last_event_arrived is None:
                    executor.submit(
                        self.subscribe_events,
                        dev_info=devInfo,
                        attribute_dictionary=(self.attribute_dictionary),
                    )
            sleep(self._event_subscription_check_period)

    def subscribe_events(
        self,
        dev_info: SubArrayDeviceInfo,
        attribute_dictionary: Optional[Dict[str, str]] = None,
    ):
        try:
            cspsubarray_proxy = self._dev_factory.get_device(dev_info.dev_name)
            cspsubarray_proxy.subscribe_event(
                "obsState",
                tango.EventType.CHANGE_EVENT,
                self.handle_obs_state_event,
                stateless=True,
            )
            cspsubarray_proxy.subscribe_event(
                "longRunningCommandResult",
                tango.EventType.CHANGE_EVENT,
                self.handle_command_result_event,
                stateless=True,
            )

            cspsubarray_proxy.subscribe_event(
                "adminMode",
                tango.EventType.CHANGE_EVENT,
                self.handle_admin_mode_event,
                stateless=True,
            )

        except Exception as exception:
            self._logger.error(
                "Event not working for the device %s: %s",
                cspsubarray_proxy.dev_name,
                exception,
            )

    def handle_obs_state_event(
        self, event: tango.EventType.CHANGE_EVENT
    ) -> None:
        if event.err:
            error = event.errors[0]
            error_msg = f"{error.reason},{error.desc}"
            self._logger.error(error_msg)
            self._component_manager.update_event_failure(
                event.device.dev_name()
            )
            return
        new_value = event.attr_value.value
        self._logger.info(
            "Received an obsState event with value: %s for device: %s",
            new_value,
            event.device.dev_name(),
        )
        self._component_manager.update_device_obs_state(
            event.device.dev_name(), new_value
        )
        self._logger.info(
            "Obs State value updated to :%s", ObsState(new_value).name
        )

    def handle_command_result_event(
        self, event: tango.EventType.CHANGE_EVENT
    ) -> None:
        """Handle the LRCR event."""
        if event.err:
            error = event.errors[0]
            error_msg = (
                f"Error handling LRCR event: {error.reason}, {error.desc}"
            )
            self._logger.error(error_msg)
            self._component_manager.update_event_failure(
                event.device.dev_name()
            )
            return

        self._logger.info("LRCR event callback executed successfully.")
        new_value = event.attr_value.value
        self._component_manager.update_command_result(
            event.device.dev_name(), new_value
        )
