"""
CSP Subarray Leaf node low monitors the CSP Subarray and issues control
actions during an observation.
It also acts as a CSP contact point for Subarray Node for observation
execution for TMC.
"""

from tango.server import AttrWriteType, attribute, device_property, run

from ska_tmc_cspsubarrayleafnode.csp_subarray_leaf_node import (
    TmcLeafNodeCspSubarray,
)
from ska_tmc_cspsubarrayleafnode.manager import CspSLNComponentManagerLow
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow

__all__ = ["LowTmcLeafNodeCspSubarray", "main"]


class LowTmcLeafNodeCspSubarray(TmcLeafNodeCspSubarray):
    """
    Tango device calss for tmc csp subarray leafnode low
    """

    # -----------------
    # Device Properties
    # -----------------

    PSTDelayModelTimeInAdvance = device_property(
        dtype="DevShort",
        default_value=600,
        doc="Time in advance to calculate delays",
    )

    PSTDelayCadence = device_property(
        dtype="DevFloat",
        default_value=300.0,
        doc="Cadence for delay calculation",
    )

    PSTDelayValidityPeriod = device_property(
        dtype="DevFloat",
        default_value=600.0,
        doc="Validity period for calculated delays",
    )

    # -----------------
    # Attributes
    # -----------------

    delayModelPSTBeam1 = attribute(
        dtype="DevString",
        access=AttrWriteType.READ,
    )

    def update_delay_model_pst_beam_callback(self, delay_model_pst_beam):
        """Change event callback for delay model PST beam"""
        if delay_model_pst_beam != "":
            self.push_change_archive_events(
                "delayModelPSTBeam1", delay_model_pst_beam
            )

    def read_delayModelPSTBeam1(self):
        """Returns the delay model json for PST Beam."""
        return self.component_manager.delay_model_pst_beam

    def create_component_manager(self) -> CspSLNComponentManagerLow:
        """Method for creating component manager instance"""
        cm = CspSLNComponentManagerLow(
            self.TelmodelSource,
            self.TelmodelPath,
            InputParameterLow(None, device_name=self.CspSubarrayFQDN),
            delay_cadence=self.DelayCadence,
            pst_delay_cadence=self.PSTDelayCadence,
            delay_validity_period=self.DelayValidityPeriod,
            pst_delay_validity_period=self.PSTDelayValidityPeriod,
            logger=self.logger,
            communication_state_callback=None,
            component_state_callback=None,
            _update_delay_model_callback=self.update_delay_model_callback,
            _update_delay_model_pst_beam_callback=(
                self.update_delay_model_pst_beam_callback
            ),
            _update_csp_subarray_obs_state_callback=(
                self.update_csp_subarray_obs_state_callback
            ),
            _update_admin_mode_callback=self.update_admin_mode_callback,
            event_subscription_check_period=self.EventSubscriptionCheckPeriod,
            liveliness_check_period=self.LivelinessCheckPeriod,
            adapter_timeout=self.AdapterTimeOut,
            command_timeout=self.CommandTimeOut,
            _update_availablity_callback=self.update_availablity_callback,
            delay_model_time_in_advance=self.DelayModelTimeInAdvance,
            pst_delay_model_time_in_advance=self.PSTDelayModelTimeInAdvance,
        )
        return cm


def main(args=None, **kwargs):
    """
    Runs the LowTmcLeafNodeCspSubarray.
    :param args: Arguments internal to TANGO

    :param kwargs: Arguments internal to TANGO

    :return: LowTmcLeafNodeCspSubarray TANGO object.
    """
    return run((LowTmcLeafNodeCspSubarray,), args=args, **kwargs)


if __name__ == "__main__":
    main()
