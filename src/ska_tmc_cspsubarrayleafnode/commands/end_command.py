"""
End command class for CSPSubarrayLeafNode.
"""
import json
import logging
import time
from typing import Callable, Optional, Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from ska_tmc_cspsubarrayleafnode.utils import (
    INITIAL_LOW_DELAY_JSON,
    INITIAL_MID_DELAY_JSON,
)

configure_logging()
LOGGER = logging.getLogger(__name__)


class End(CspSLNCommand):
    """
    A class for CspSubarayLeafNode's End() command.

    It invokes End command on CSP Subarray.
    """

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
    ) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id: str = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

    @timeout_tracker
    @error_propagation_tracker("get_obs_state", [ObsState.IDLE])
    def end(self) -> Tuple[ResultCode, str]:
        """
        This is a long running method for End command, it
        executes do hook, invokes End command on CspSubarray.
        """
        return self.do()

    # Disabling the linting warning as End command does not take an input but
    # the signature of the methods requires one.
    # pylint: disable=arguments-differ
    def do_mid(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke End command on CSP Subarray.

        :param argin: None.

        :return: (ResultCode, message)

        :raises Exception: if the command execution is not successful

        """
        return_code, message = self.init_adapter()
        if return_code == ResultCode.FAILED:
            return return_code, message
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.End()

        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the End command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "End command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        self.component_manager.config_id = ""
        self.reset_delay_model_values(INITIAL_MID_DELAY_JSON)
        return result[0], unique_id_or_message[0]

    def do_low(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke End command on CSP Subarray.

        :param argin: None.

        :return: (ResultCode, message)

        :raises Exception: if the command execution is not successful

        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message
        try:
            (
                result,
                _,
            ) = self.csp_subarray_adapter.End()
        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the End command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "End command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        self.component_manager.config_id = ""
        self.component_manager.subarray_beam_id = None
        self.component_manager.station_ids.clear()
        self.reset_delay_model_values(INITIAL_LOW_DELAY_JSON)
        return result[0], "Command Completed"

    def reset_delay_model_values(self, delay_json: dict):
        """
        Method to reset delayModel attribute value
        to empty delayModel json
        :param delay_json: delay json
        :type delay_json: dict
        """
        delay_manager = self.component_manager.delay_manager
        delay_manager.stop()
        delay_manager.start_time_for_delay_calculation = None
        delay_manager.clear_next_polynomial_time()
        if isinstance(
            self.component_manager._input_parameter, InputParameterLow
        ):
            if self.component_manager.number_of_pst_beams > 0:
                delay_manager_pst_beam = (
                    self.component_manager.delay_manager_pst_beam
                )
                delay_manager_pst_beam.start_time_for_delay_calculation = None
                delay_manager_pst_beam.clear_next_polynomial_time()
                with delay_manager.delay_model_lock:
                    self.component_manager.delay_model_pst_beam = json.dumps(
                        delay_json
                    )
                self.component_manager.number_of_pst_beams = 0
        with delay_manager.delay_model_lock:
            self.component_manager.delay_model = json.dumps(delay_json)
