"""
Scan command class for CspSubarrayLeafNode.
"""
import json
import logging
import time
from json import JSONDecodeError
from typing import Callable, Optional, Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)

# pylint: enable=line-too-long
configure_logging()
LOGGER = logging.getLogger(__name__)


class Scan(CspSLNCommand):
    """
    A class for CspSubarrayLeafNode's Scan() command. Scan command is
    inherited from CspSLNCommand.

    This command invokes Scan command on CSP Subarray.
    """

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
    ) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id: str = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

    @timeout_tracker
    @error_propagation_tracker("is_scan_completed", [True])
    def scan(self, argin: str) -> Tuple[ResultCode, str]:
        """
        This is a long running method for Scan command, it
        executes do hook, invokes Scan command on CspSubarray.

        :param argin: Input JSON string
        :type argin: str
        """
        return self.do(argin)

    def do_mid(self, argin: str = "") -> Tuple[ResultCode, str]:
        """

        Method to invoke Scan command on CSP Mid Subarray.

        :param argin: JSON string consists of scan id (int).

        Example:
        {"interface":"https://schema.skao.int/ska-mid-csp-scan/2.0",
        "scan_id":1}

        Note: Enter the json string without spaces as a input.

        :return: (ResultCode, message)

        """
        result_code, message = self.init_adapter()

        if result_code == ResultCode.FAILED:
            return result_code, message

        try:
            json_argument = json.loads(argin)
        except JSONDecodeError as json_error:
            self.logger.exception(
                "Execution of Scan command is failed."
                + "Reason: JSON parsing failed"
                + f" with exception: {json_error}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            return (
                ResultCode.FAILED,
                f" JSON parsing failed with exception: {json_error}",
            )
        result_code, message = self.validate_json_argument(json_argument)
        if result_code == ResultCode.FAILED:
            return result_code, message
        self.logger.info(
            "Invoking Scan command on:%s", self.csp_subarray_adapter.dev_name
        )
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.Scan(json.dumps(json_argument))
        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the Scan command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "Scan command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        return result[0], unique_id_or_message[0]

    def do_low(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Method to invoke Scan command on CSP Low Subarray.

        :param argin: JSON string consists of scan id (int).

        Example:
        {"common":{"subarray_id":1},"lowcbf":{"scan_id":987654321,
        "scan_seconds":30}}

        Note: Enter the json string without spaces as a input.

        :return: (ResultCode, message)
        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        try:
            json_argument = json.loads(argin)
        except JSONDecodeError as json_error:
            self.logger.exception(
                "Execution of Scan command is failed."
                + f"Reason: JSON parsing failed with exception: {json_error}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            return (
                ResultCode.FAILED,
                f"JSON parsing failed with exception: {json_error}",
            )
        result_code, message = self.validate_json_argument_low(json_argument)
        if result_code == ResultCode.FAILED:
            return result_code, message

        self.logger.info(
            "Invoking Scan command on:%s", self.csp_subarray_adapter.dev_name
        )
        try:
            (
                result,
                _,
            ) = self.csp_subarray_adapter.Scan(argin)

        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                "The invocation of the Scan command is failed"
                + f"on {self.csp_subarray_adapter.dev_name}"
                + f"Reason: {exception}",
            )

        self.logger.info(
            "Scan command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )

        return result[0], "Command Completed"

    def validate_json_argument(self, input_argin: str) -> tuple:
        """Validates the json argument"""
        if "interface" not in input_argin:
            return (
                ResultCode.FAILED,
                "interface key is not present in the input json argument.",
            )

        if "scan_id" not in input_argin:
            return (
                ResultCode.FAILED,
                "scan_id key is not present in the input json argument.",
            )

        return (ResultCode.OK, "")

    def validate_json_argument_low(self, input_argin: dict) -> tuple:
        """Validates the json argument for low"""
        if "scan_id" not in input_argin["lowcbf"]:
            return (
                ResultCode.FAILED,
                "scan_id key is not present in the input json argument.",
            )

        return (ResultCode.OK, "")
