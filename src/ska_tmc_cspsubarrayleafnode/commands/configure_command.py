"""
Configure command class for CSPSubarrayLeafNode.
"""
import json
import logging
import time
from datetime import datetime
from json import JSONDecodeError
from typing import Callable, Optional, Tuple

import katpoint
from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)

# pylint: enable=line-too-long
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow

configure_logging()
LOGGER = logging.getLogger(__name__)


class Configure(CspSLNCommand):
    """
    A class for CspSubarayLeafNode's Configure() command.

    It accepts CSP configuration in JSON string format
    and invokes Configure command on CSP Subarray.
    """

    def __init__(
        self, component_manager, logger: logging.Logger = LOGGER
    ) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id: str = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

    @timeout_tracker
    @error_propagation_tracker(
        "get_obs_state", [ObsState.CONFIGURING, ObsState.READY]
    )
    def configure(self, argin: str) -> Optional[Tuple[ResultCode, str]]:
        """
        Executes do hook, invoking Configure command.

        :param argin: Input argument for the command
        :type argin: `str`
        """
        result_code, message = self.start_delay(argin)

        if result_code in [ResultCode.FAILED, ResultCode.REJECTED]:
            self.update_task_status(
                result=(ResultCode.FAILED, message), exception=message
            )
            return None  # Return type should match Tuple[ResultCode, str]

        return self.do(argin)

    def do_mid(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Method to invoke Configure command on CSP Subarray.

        :param argin: The string in JSON format. The JSON contains
            following values:

        Example:
        {"interface":"https://schema.skao.int/ska-csp-configure/2.0",
        "subarray":{"subarray_name":"science period 23"},"common":
        {"config_id":"sbi-mvp01-20200325-00001-science_A","frequency_band":
        "1","subarray_id":"1"},"cbf":{"delay_model_subscription_point":
        "mid-tmc/subarray-leaf-node-csp/01/delayModel","fsp":[{"fsp_id":1,
        "function_mode":"CORR","frequency_slice_id":1,"integration_factor":1,
        "zoom_factor":0,"channel_averaging_map":[[0,2],[744,0]],
        "channel_offset":0,"output_link_map":[[0,0],[200,1]],},{"fsp_id":2,
        "function_mode":"CORR","frequency_slice_id":2,"integration_factor":1,
        "zoom_factor":1,"zoom_window_tuning":650000,"channel_averaging_map":
        [[0,2],[744,0]],"channel_offset":744,"output_link_map":[[0,4],
        [200,5]],"output_host":[[0,"192.168.1.1"]],"output_port":
        [[0,9744,1]]}],"vlbi":{}},"pss":{},"pst":{},"pointing":{"target":
        {"reference_frame":"ICRS","target_name":"Polaris Australis",
        "ra":"21:08:47.92","dec":"-88:57:22.9"}}}

        :type argin: str
            Note: Enter the json string without spaces as a input.

        :return: (ResultCode, message)

        :raises Exception: if the command execution is not successful

        raises:
            DevFailed if the command execution is not successful

            ValueError if input argument json string contains invalid value
        """

        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        try:
            argin_json = json.loads(argin)
        except JSONDecodeError as json_error:
            self.logger.exception(
                "Execution of Configure command is failed."
                + f"Reason: JSON parsing failed with exception: {json_error}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            return (
                ResultCode.FAILED,
                f"JSON parsing failed with exception: {json_error}",
            )

        # Extract config_id from CSP block
        self.component_manager.config_id = argin_json["common"]["config_id"]

        pointing_params = argin_json["pointing"]
        # ADR-63 check
        if "target" not in pointing_params:
            for group in pointing_params.get("groups", []):
                if "field" in group and "target_name" in group["field"]:
                    target_name = group["field"]["target_name"]
                    reference_frame = (
                        group["field"].get("reference_frame", "").lower()
                    )
                    if reference_frame == "special":
                        target_str = f"{target_name}, special"
                    else:
                        target_ra = group["field"]["attrs"].get("c1")
                        target_dec = group["field"]["attrs"].get("c2")
                        target_str = f"radec , {target_ra} , {target_dec}"
                    break  # Exit loop once the target is found
        else:
            # Checking for non-sidereal tracking
            if pointing_params["target"]["reference_frame"].lower() == (
                "special"
            ):
                target_name = pointing_params["target"]["target_name"]
                target_str = f"{target_name}, special"
            else:
                target_ra = pointing_params["target"]["ra"]
                target_dec = pointing_params["target"]["dec"]
                target_str = f"radec , {target_ra} , {target_dec}"

        try:
            # Create target object
            self.component_manager.set_target(katpoint.Target(target_str))
        except Exception as exception:
            self.logger.exception(
                "Configure command invocation failed: %s", exception
            )
            return (
                ResultCode.FAILED,
                f"Exception occurred while creating the "
                f"Target object: {exception}",
            )

        csp_configuration = argin_json.copy()
        # Keep configuration specific to CSP and delete pointing
        # configuration
        csp_configuration.pop("pointing")
        self.logger.info(
            "Invoking Configure command on %s",
            self.csp_subarray_adapter.dev_name,
        )
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.Configure(
                json.dumps(csp_configuration)
            )
        except Exception as exception:
            self.logger.exception(
                "Configure command invocation failed: %s", exception
            )
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the Configure command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "Configure command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        return result[0], unique_id_or_message[0]

    # pylint:disable= too-many-return-statements
    def do_low(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Method to invoke Configure command on CSP low Subarray.

        :param argin: The string in JSON format. The JSON contains
            following values:

        Example:
        {"interface":"https://schema.skao.int/ska-low-csp-configure/3.2",
        "common":{"config_id":"sbi-mvp01-20200325-00001-science_A","eb_id":
        "eb-test-20220916-00000"},"lowcbf":{"stations":{"stns":[[1,1],[2,1],
        [3,1],[4,1],[5,1],[6,1]],"stn_beams":[{"beam_id":1,"freq_ids":[400],
        "delay_poly":"low-tmc/subarray-leaf-node-csp/01/delayModel"}]},
        "vis":{"fsp":{"firmware":"vis","fsp_ids":[1]},"stn_beams":[
        {"stn_beam_id":1,"integration_ms":849}]},"timing_beams":{"fsp":
        {"firmware":"pst","fsp_ids":[2]},"beams":[{"pst_beam_id":1,"field":
        {"target_id":"PSR J0024-7204R","reference_frame":"icrs","attrs":
        {"c1":6.023625,"c2":-72.08128333,"pm_c1":4.8,"pm_c2":-3.3}},
        "stn_beam_id":1,"stn_weights":[0.9,1,1,1,0.9,1]}]}},"pst":{"beams":
        [{"beam_id":1,"scan":{"activation_time":"2022-01-19T23:07:45Z",
        "bits_per_sample":32,"num_of_polarizations":2,"udp_nsamp":32,
        "wt_nsamp":32,"udp_nchan":24,"num_frequency_channels":432,
        "centre_frequency":200000000,"total_bandwidth":1562500,
        "observation_mode":"VOLTAGE_RECORDER","observer_id":"jdoe",
        "project_id":"project1","pointing_id":"pointing1","source":"J1921+2153"
        ,"itrf":[5109360.133,2006852.586,-3238948.127],"receiver_id":
        "receiver3","feed_polarization":"LIN","feed_handedness":1,"feed_angle":
        1.234,"feed_tracking_mode":"FA","feed_position_angle":10,
        "oversampling_ratio":[8,7],"coordinates":{"equinox":2000,"ra":
        "19:21:44.815","dec":"21:53:02.400"},"max_scan_length":20000,
        "subint_duration":30,"receptors":["receptor1","receptor2"],
        "receptor_weights":[0.4,0.6],"num_channelization_stages":2,
        "channelization_stages":[{"num_filter_taps":1,"filter_coefficients":
        [1],"num_frequency_channels":1024,"oversampling_ratio":[32,27]},
        {"num_filter_taps":1,"filter_coefficients":[1],"num_frequency_channels"
        :256,"oversampling_ratio":[4,3]}]},"delay_poly":
        "low-tmc/subarray-leaf-node-csp/01/delayModelPSTBeam1",
        "jones":"tango://jones.skao.int/low/stn-beam/1"}]}}

        Note: Enter the json string without spaces as a input.
        :type argin: str

        :return: (ResultCode, message)

        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        try:
            argin_json = json.loads(argin)
            subarray_beams = argin_json["mccs"]["subarray_beams"]
        except KeyError as key_error:
            log_msg = (
                "Missing subarray_beams key in MCCS configuration: "
                + f"{key_error}"
            )
            self.logger.exception(log_msg)
            return (
                ResultCode.FAILED,
                "Missing subarray_beams key, "
                + "Configure command failed on "
                + f"CspSubarrayLeafNode: {key_error}",
            )

        # Extract subarray_beam IDs from MCCS block
        self.component_manager.subarray_beam_id = argin_json["mccs"][
            "subarray_beams"
        ][0]["subarray_beam_id"]

        # Extract config_id from CSP block
        self.component_manager.config_id = argin_json["csp"]["common"][
            "config_id"
        ]
        # Extract target ra and dec coordinates from input JSON
        # considered c1 and c2 as Ra and Dec as per MCCS schema
        # https://gitlab.com/ska-telescope/mccs/ska-low-mccs/-/blob/main/src/
        # ska_low_mccs/schemas/subarray/MccsSubarray_Configure_3_0.json?
        # ref_type=heads#L99

        try:
            target_ra = subarray_beams[0]["sky_coordinates"]["c1"]
        except KeyError as key_error:
            log_msg = f"Missing Ra key in MCCS configuration: {key_error}"
            self.logger.exception(log_msg)
            return (
                ResultCode.FAILED,
                "Missing Ra key, Configure command failed "
                + f"on CspSubarrayLeafNode: {key_error}",
            )
        try:
            target_dec = subarray_beams[0]["sky_coordinates"]["c2"]
        except KeyError as key_error:
            log_msg = f"Missing Dec key in MCCS configuration: {key_error}"
            self.logger.exception(log_msg)
            return (
                ResultCode.FAILED,
                "Missing Dec key, Configure command "
                + f"failed on CspSubarrayLeafNode: {key_error}",
            )
        try:
            # Create target object. Currently the target object is created
            # with az, el coordinates
            self.component_manager.set_target(
                katpoint.Target(f"radec , {target_ra} , {target_dec}")
            )
        except Exception as exception:
            self.logger.exception(
                "Execution of Configure command is failed."
                "Reason: Creating Target object failed with exception: "
                f"{exception}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation",
            )
            return (
                ResultCode.FAILED,
                f"Exception occurred while creating the "
                f"Target object: {exception}",
            )
        # The values for PST beam related parameters are getting updated only
        # if the relevant section is present in the configure json
        if "timing_beams" in argin_json["csp"]["lowcbf"].keys():
            beams = argin_json["csp"]["lowcbf"]["timing_beams"]["beams"]
            no_of_beams = len(beams)
            number_of_pst_beams = 0
            for beam in range(no_of_beams):
                if "pst_beam_id" in beams[beam] and "field" in beams[beam]:
                    self.component_manager.pst_beam_id = beams[beam].get(
                        "pst_beam_id"
                    )
                    self.component_manager.pst_beam_right_ascension = beams[
                        beam
                    ]["field"]["attrs"].get("c1")
                    self.component_manager.pst_beam_declination = beams[beam][
                        "field"
                    ]["attrs"].get("c2")
                    number_of_pst_beams += 1
                    # This section is not required on CSP/CBF, hence removed
                    del beams[beam]["field"]
            self.component_manager.number_of_pst_beams = number_of_pst_beams
        else:
            self.logger.info("The timing_beams key is not present in the JSON")
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.Configure(
                json.dumps(argin_json["csp"])
            )
        except Exception as exception:
            self.logger.exception(
                f"Configure command invocation failed: {exception}"
            )
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the Configure command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )

        return result[0], unique_id_or_message[0]

    def load_low_configure_json(self, argin: str) -> Tuple[ResultCode, str]:
        """Method loads low configure json.

        :param argin: Input JSON string
        :type argin: str
        :return: Tuple[ResultCode, str]: result code and message
        """
        try:
            json.loads(argin)
            return ResultCode.OK, ""
        except JSONDecodeError as json_error:
            self.logger.exception(
                "Execution of Configure command is failed."
                + f"Reason: JSON parsing failed with exception: {json_error}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            return (
                ResultCode.FAILED,
                f"JSON parsing failed with exception: {json_error}",
            )

    def update_station_ids(self, argin_json: dict) -> Tuple[ResultCode, str]:
        """
        Method to extract station IDs from MCCS apertures
        and update station IDs from component manager.

        :param argin: MCCS configuration dict
        :type argin: dict
        :return: Tuple[ResultCode, str]: result code and message
        """
        try:
            low_cbf_stations = argin_json["csp"]["lowcbf"]["stations"]["stns"]
            self.component_manager.station_substation_ids = low_cbf_stations
            for entry in low_cbf_stations:
                self.component_manager.station_ids.append(entry[0])
        except KeyError as key_error:
            log_msg = f"Missing stns key in lowcbf configuration: {key_error} "
            self.logger.exception(log_msg)
            return (
                ResultCode.FAILED,
                "Missing stns key, "
                + "Configure command failed on CspSubarrayLeafNode:"
                + f"{key_error}",
            )
        return ResultCode.OK, ""

    def start_delay(self, argin: str) -> Tuple[ResultCode, str]:
        """A method to start delay thread
        and starts generating delay polynomials.

        :param argin: Input JSON string
        :type argin: str
        :return: Tuple[ResultCode, str]: result code and message
        """

        if isinstance(
            self.component_manager._input_parameter, InputParameterLow
        ):
            result_code, message = self.load_low_configure_json(argin)
            if result_code == ResultCode.FAILED:
                return ResultCode.FAILED, message
            # Extract stations IDs from lowcbf cofiguration
            result_code, message = self.update_station_ids(json.loads(argin))
            if result_code == ResultCode.FAILED:
                return ResultCode.FAILED, message

            # This section of code ensures that the delay calculation for pst
            # beam starts only if the pst beams section is present in json

            argin = json.loads(argin)
            if "timing_beams" in argin["csp"]["lowcbf"].keys():
                beams = argin["csp"]["lowcbf"]["timing_beams"]["beams"]
                no_of_beams = len(beams)
                number_of_pst_beams = 0
                delay_manager_pst = (
                    self.component_manager.delay_manager_pst_beam
                )
                for beam in range(no_of_beams):
                    if "pst_beam_id" in beams[beam]:
                        (
                            result_code,
                            _,
                        ) = delay_manager_pst.update_config_params()
                        number_of_pst_beams += 1
            else:
                self.logger.info(
                    "The timing_beams key is not present in the JSON"
                )
        (
            result_code,
            _,
        ) = self.component_manager.delay_manager.update_config_params()

        # This line code ensures that the start time of calculation of delays
        # for station beams as well as pst beams remains same
        self.component_manager.delay_start_time = datetime.today()

        self.component_manager.delay_manager.set_dummy_targets()
        if result_code == ResultCode.FAILED:
            return ResultCode.FAILED, "Update config parameter failed"

        # This section of code only executes if the pst beams section is
        # present in configure json
        if isinstance(
            self.component_manager._input_parameter, InputParameterLow
        ):
            if "timing_beams" in argin["csp"]["lowcbf"].keys():
                if number_of_pst_beams > 0:
                    delay_manager_pst.set_dummy_targets()
                    self.component_manager.delay_manager_pst_beam.start()

        self.component_manager.delay_manager.start()
        return (ResultCode.OK, "")
