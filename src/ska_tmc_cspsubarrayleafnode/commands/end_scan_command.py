"""
EndScan command class for CspSubarrayLeafNode.
"""
import logging
import time
from typing import Callable, Optional, Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)

configure_logging()
LOGGER = logging.getLogger(__name__)
# pylint: enable=line-too-long


class EndScan(CspSLNCommand):
    """
    A class for CspSubarrayLeafNode's EndScan() command.

    It invokes EndScan command on CSP Subarray.
    This command is allowed when CSP Subarray is in obsState SCANNING.

    """

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
    ) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id: str = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

    @timeout_tracker
    @error_propagation_tracker("get_obs_state", [ObsState.READY])
    def end_scan(self) -> Tuple[ResultCode, str]:
        """
        This is a long running method for EndScan command, it
        executes do hook, invokes EndScan command on CspSubarray.
        """
        return self.do()

    # Disabling the linting warning as EndScan command does not take an input
    # but the signature of the methods requires one.
    # pylint: disable=arguments-differ
    def do_mid(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke EndScan command on CSP Mid Subarray.

        :param argin: Json input string, defaults to None
        :type argin: String

        :return: (ResultCode, message)
        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.EndScan()
        except Exception as exception:
            self.logger.exception(
                f"Command invocation failed on \
                    {self.csp_subarray_adapter.dev_name}: {exception}"
            )
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the EndScan command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "EndScan command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        return result[0], unique_id_or_message[0]

    def do_low(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke EndScan command on CSP Low Subarray.

        :param argin: Json input string, defaults to None
        :type argin: String

        :return: (ResultCode, message)

        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message
        try:
            (
                result,
                _,
            ) = self.csp_subarray_adapter.EndScan()
        except Exception as exception:
            self.logger.exception(
                f"Command invocation failed on \
                    {self.csp_subarray_adapter.dev_name}: {exception}"
            )
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the EndScan command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "EndScan command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )

        return result[0], "Command Completed"
