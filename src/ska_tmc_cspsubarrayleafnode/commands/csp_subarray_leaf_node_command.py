"""Command class for CSP Subarray Leaf Node

This module defines a command class `CspSLNCommand` for the CSPSubarrayLeafNode
The `CspSLNCommand` class provides implementation for commands that interact
with the CSP Subarray Leaf Node
It includes methods for initializing adapters, checking device responsiveness,
and handling commands for both low-level and mid-level devices.

Attributes:
    LOGGER (logging.Logger): The logger for the module.
"""
import logging
import time
from typing import Dict, Tuple, Union

from ska_control_model.task_status import TaskStatus
from ska_ser_logging.configuration import configure_logging
from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tmc_common import SubarrayAdapter
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.tmc_command import TmcLeafNodeCommand
from tango import ConnectionFailed, DevFailed

from ska_tmc_cspsubarrayleafnode.model.input import InputParameterMid

configure_logging()
LOGGER = logging.getLogger(__name__)


class CspSLNCommand(TmcLeafNodeCommand):
    """Abstract command class for all CspSubarrayLeafNode"""

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
    ) -> None:
        super().__init__(component_manager, logger=logger)
        self.component_manager = component_manager
        self.csp_subarray_adapter = None
        self.task_callback: TaskCallbackType

    def init_adapter_low(self):
        """Initialize the adapter for the low-level device."""
        self.init_adapter()

    def init_adapter_mid(self):
        """Initialize the adapter for the low-level device."""
        self.init_adapter()

    def do_mid(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Abstract Method from TmcLeafNodeCommand is
        defined here but not utilized by this Class.
        Args:
            argin (_type_, optional): Accepts argument if required.
            Defaults to None.
        """
        raise NotImplementedError(
            "This method must be implemented by child class!"
        )

    def do_low(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Abstract Method from TmcLeafNodeCommand is
        defined here but not utilized by this Class.
        Args:
            argin (_type_, optional): Accepts argument if required.
            Defaults to None.
        """
        raise NotImplementedError(
            "This method must be implemented by child class!"
        )

    def do(self, argin: str = "") -> Tuple[ResultCode, str]:
        """
        Handle a command.

        Args:
            argin (str, optional): Command argument. Defaults to None.

        Returns:
            Tuple[ResultCode, str]: A tuple containing the result code and
            message.
        """

        if isinstance(
            self.component_manager._input_parameter, InputParameterMid
        ):
            if argin:
                return self.do_mid(argin)
            return self.do_mid()
        if argin:
            return self.do_low(argin)
        return self.do_low()

    def init_adapter(self) -> Tuple[ResultCode, str]:
        """
        Creates adapter for underlying CSP subarray device.

        Returns:
            Tuple[ResultCode, str]: A tuple containing the result code and
            message.
        """
        start_time = time.time()
        adapter_timeout: int = self.component_manager.adapter_timeout
        elapsed_time = 0
        device: str = (
            self.component_manager.input_parameter.csp_subarray_dev_name
        )
        while (
            self.csp_subarray_adapter is None
            and elapsed_time < adapter_timeout
        ):
            try:
                get_adapter = self.adapter_factory.get_or_create_adapter
                self.csp_subarray_adapter: SubarrayAdapter = get_adapter(
                    device,
                    AdapterType.CSPSUBARRAY,
                )
            except ConnectionFailed as cf:
                elapsed_time = time.time() - start_time
                if elapsed_time > adapter_timeout:
                    message = f"Error in creating adapter for {device}: {cf}"
                    return ResultCode.FAILED, message
            except DevFailed as df:
                elapsed_time = time.time() - start_time
                if elapsed_time > adapter_timeout:
                    message = f"Error in creating adapter for {device}: {df}"
                    return ResultCode.FAILED, message

            except Exception as exception:
                message = (
                    f"Error in creating adapter for {device}: {exception}"
                )
                return ResultCode.FAILED, message
        return ResultCode.OK, ""

    def update_task_status(
        self,
        **kwargs: Dict[str, Union[Tuple[ResultCode, str], TaskStatus, str]],
    ) -> None:
        """
        Update the status of a task.

        Args:
            **kwargs: Keyword arguments for task status update.
        """
        result = kwargs.get("result")
        status = kwargs.get("status", TaskStatus.COMPLETED)
        message = kwargs.get("exception")
        if status == TaskStatus.ABORTED:
            self.task_callback(status=status)
        if result:
            if result[0] == ResultCode.FAILED:
                self.task_callback(
                    status=status, result=result, exception=message
                )
            else:
                self.task_callback(status=status, result=result)
        self.component_manager.command_in_progress = ""
