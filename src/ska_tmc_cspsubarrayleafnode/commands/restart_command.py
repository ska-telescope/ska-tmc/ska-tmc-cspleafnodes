"""Restart command class for CspSubarrayLeafNode."""
# pylint: disable=abstract-method
import threading
from logging import Logger
from typing import Optional

from ska_tango_base.base import TaskCallbackType
from ska_tango_base.commands import ResultCode
from ska_tango_base.executor import TaskStatus

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)

# pylint: enable=line-too-long


class Restart(CspSLNCommand):
    """
    A class for CspSubarrayLeafNode's Restart command.
    Restarts the CSP Subarray device.
    """

    def restart(
        self,
        logger: Logger,
        task_callback: TaskCallbackType,
        # pylint:disable = unused-argument
        task_abort_event: Optional[threading.Event] = None,
    ) -> None:
        """This is a long running method for a Restart command, it
        executes do hook, invokes Restart command on CspSubarray.

        :param argin: Input JSON string
        :type argin: str
        :param logger: logger
        :type logger: logging.Logger
        :param task_callback: Update task state, defaults to None
        :type task_callback: TaskCallbackType
        :param task_abort_event: Check for abort, defaults to None
        :type task_abort_event: Event, optional
        """
        # Indicate that the task has started
        task_callback(status=TaskStatus.IN_PROGRESS)
        result_code, message = self.do()
        if result_code in [ResultCode.FAILED, ResultCode.REJECTED]:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.FAILED, message),
                exception=message,
            )
        else:
            task_callback(
                status=TaskStatus.COMPLETED,
                result=(ResultCode.OK, message),
            )

    # Disabling the linting warning as Restart command does not take an input
    # but the signature of the methods requires one.
    # pylint: disable=arguments-differ
    def do(self) -> tuple[ResultCode, str]:
        """
        Method to invoke Restart command on CSP  Subarray.

        :param None:

        Note: Enter the json string without spaces as a input.

        :return: (ResultCode, message)

        """
        result_code, message = self.init_adapter()

        if result_code == ResultCode.FAILED:
            return result_code, message
        self.logger.info(
            "Invoking Restart command on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        try:
            (
                result,
                _,
            ) = self.csp_subarray_adapter.Restart()
        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the Restart command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "Restart command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        return result[0], "Command Completed"
