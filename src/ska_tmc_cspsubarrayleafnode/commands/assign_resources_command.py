"""
AssignResources command class for CspSubarrayLeafNode.
"""
import json
import logging
import time
from json import JSONDecodeError
from typing import Any, Callable, Optional, Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import TimeKeeper, TimeoutCallback, TimeoutState
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)

# pylint: enable=line-too-long

configure_logging()
LOGGER = logging.getLogger(__name__)


class AssignResources(CspSLNCommand):
    """
    A class for CspSubarayLeafNode's AssignResources() command.

    It accepts subarrayID and receptor IDs in JSON string format and invokes
    the AssignResources command on the CSP Subarray.
    """

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
    ) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id: str = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback: Callable[
            [str, TimeoutState], Optional[ValueError]
        ] = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

    @timeout_tracker
    @error_propagation_tracker(
        "get_obs_state", [ObsState.RESOURCING, ObsState.IDLE]
    )
    def assign_resources(
        self,
        argin: str,
    ) -> Tuple[ResultCode, str]:
        """This is a long running method for AssignResources command, it
            executes do hook, invokes AssignResources command on CspSubarray.

        :param argin: Input JSON string
        :type argin: str

        :return: A tuple containing the result code and a message.
        :rtype: Tuple[ResultCode, str]
        """
        return self.do(argin)

    def do_mid(self, argin: str = "") -> Tuple[ResultCode, str]:
        """Method to invoke AssignResources command on CSP Subarray.

        :param argin: DevString. The string in JSON format. The JSON contains
            the following values:
            - subarray_id: integer
            - dish: Mandatory JSON object consisting of receptor_ids,
            DevVarString
            The individual string should contain dish numbers in string
            format with preceding zeroes up to 3 digits.
            E.g. SKA001, SKA002.
            Example:
            {
            "interface":
            "https://schema.skao.int/ska-mid-csp-assignresources/2.0",
            "subarray_id":1,"dish":{"receptor_ids":["SKA001","SKA002"]}}

        :return: A tuple containing the result code and a message.
        :rtype: Tuple[ResultCode, str]

        :raises Exception: If the command execution is not successful.
        """
        result_code, message = self.init_adapter()

        if result_code == ResultCode.FAILED:
            return result_code, message

        try:
            json_argument = json.loads(argin)
        except JSONDecodeError as json_error:
            self.logger.exception(
                "Execution of AssignResources command failed. "
                + f"Reason: JSON parsing failed with exception: {json_error}. "
                + "The command was not executed successfully. "
                + "The device will continue with normal operation."
            )
            return (
                ResultCode.FAILED,
                f"Exception occurred while parsing the JSON: {json_error}",
            )

        result_code, message = self.validate_json_argument(json_argument)
        self.component_manager.receptor_ids_str = json_argument["dish"][
            "receptor_ids"
        ]
        if result_code == ResultCode.FAILED:
            return result_code, message

        self.component_manager.subarray_id = json_argument["subarray_id"]
        self.logger.info(
            "Invoking AssignResources command on %s",
            self.csp_subarray_adapter.dev_name,
        )
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.AssignResources(argin)
        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the AssignResources command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "AssignResources command successfully invoked on %s",
            self.csp_subarray_adapter.dev_name,
        )
        return result[0], unique_id_or_message[0]

    def validate_json_argument(
        self, input_argin: dict[str, Any]
    ) -> Tuple[ResultCode, str]:
        """Validates the JSON argument for the AssignResources command.

        :param input_argin: Input JSON argument as a dictionary.
        :type input_argin: dict[str, Any]

        :return: A tuple containing the result code and a message.
        :rtype: Tuple[ResultCode, str]
        """
        if "interface" not in input_argin:
            return (
                ResultCode.FAILED,
                "interface key is not present in the input json argument.",
            )

        if "subarray_id" not in input_argin:
            return (
                ResultCode.FAILED,
                "subarray_id key is not present in the "
                + "input json argument.",
            )
        if "receptor_ids" not in input_argin.get("dish", ""):
            return (
                ResultCode.FAILED,
                "receptor_ids key is not present in the "
                + "input json argument.",
            )

        return (ResultCode.OK, "")

    def validate_json_argument_low(
        self, input_argin: dict[str, Any]
    ) -> Tuple[ResultCode, str]:
        """Validates the JSON argument for the AssignResources command for
        CSP low.

        :param input_argin: Input JSON argument as a dictionary.
        :type input_argin: dict[str, Any]

        :return: A tuple containing the result code and a message.
        :rtype: Tuple[ResultCode, str]
        """
        if "interface" not in input_argin:
            return (
                ResultCode.FAILED,
                "interface key is not present in the input json argument.",
            )
        if "subarray_id" not in input_argin.get("common", ""):
            return (
                ResultCode.FAILED,
                "subarray_id key is not present in the "
                + "input json argument.",
            )

        return (ResultCode.OK, "")

    def do_low(self, argin: str = "") -> Tuple[ResultCode, str]:
        """Method to invoke the AssignResources command on the CSP low Subarray

        :param argin: DevString. The string in JSON format. The JSON contains
            the following values:
            subarray_id: integer

            Example:
            {"interface":
            "https://schema.skao.int/ska-low-csp-assignresources/2.0",
            "common":{"subarray_id":1},"lowcbf":{"resources":
            [{"device":"fsp_01",
            "shared":true, "fw_image":"pst","fw_mode":"unused"},
            {"device":"p4_01",
            "shared":true,"fw_image":"p4.bin","fw_mode":"p4"}]},}

            Note: Enter the JSON string without spaces as an input.

        :return: A tuple containing the result code and a message.
        :rtype: Tuple[ResultCode, str]

        :raises Exception: If the command execution is not successful.
        """
        result_code, message = self.init_adapter()

        if result_code == ResultCode.FAILED:
            return result_code, message

        try:
            json_argument = json.loads(argin)
        except JSONDecodeError as json_error:
            self.logger.exception(
                "Execution of AssignResources command is failed."
                + f"Reason: JSON parsing failed with exception: {json_error}"
                + "The command is not executed successfully."
                + "The device will continue with normal operation"
            )
            return (
                ResultCode.FAILED,
                f"Exception occurred while parsing the JSON: {json_error}",
            )

        result_code, message = self.validate_json_argument_low(json_argument)
        if result_code == ResultCode.FAILED:
            return result_code, message

        self.component_manager.subarray_id = json_argument["common"][
            "subarray_id"
        ]
        self.logger.info(
            "Invoking AssignResources command on %s",
            self.csp_subarray_adapter.dev_name,
        )
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.AssignResources(argin)
        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the AssignResources command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "AssignResources command successfully invoked on %s",
            self.csp_subarray_adapter.dev_name,
        )
        return result[0], unique_id_or_message[0]
