"""
Abort Command for CspSubarrayLeafNode.
"""
# pylint: disable=abstract-method
import json
from logging import Logger

from ska_tango_base.commands import ResultCode

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterMid
from ska_tmc_cspsubarrayleafnode.utils import (
    INITIAL_LOW_DELAY_JSON,
    INITIAL_MID_DELAY_JSON,
)

# pylint: enable=line-too-long


class Abort(CspSLNCommand):
    """
    A class for CspSubarrayLeafNode's Abort() command.
    Aborts the CSP Subarray device.
    """

    def __init__(
        self,
        component_manager,
        logger: Logger,
    ):
        super().__init__(
            component_manager=component_manager,
            logger=logger,
        )
        self.component_manager = component_manager
        self.logger = logger

    def invoke_abort(self):
        """This method calls do for Abort command"""
        self.reset_delay_model_values()
        result_code, message = self.do()
        return result_code, message

    # Disabling the linting warning as Abort command does not take an input but
    # the signature of the methods requires one.
    # pylint: disable=arguments-differ
    def do(self):
        """
        This method invokes Abort command on CSP Subarray

        return:
            A tuple containing a return code and a string
            message indicating status.
            The message is for information purpose only.

        rtype:
            (ResultCode, str)

        raises:
            Exception if error occurs in invoking command
            on any of the devices like CspSubarrayLeafNode
        """
        # Edge case if timer is started but not the tracker thread.
        try:
            if self.component_manager.tracker_thread:
                if (
                    not self.component_manager.tracker_thread.is_alive()
                    and self.component_manager.timer_object
                ):
                    self.component_manager.stop_timer()
            elif self.component_manager.timer_object:
                self.component_manager.stop_timer()
        except Exception as exception:
            self.logger.error(
                "Error occurred during timer clean up: %s", exception
            )

        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        result_code, message = self.call_adapter_method(
            "CSP Subarray", self.csp_subarray_adapter, "Abort"
        )
        return result_code[0], message[0]

    def reset_delay_model_values(self):
        """
        Method to reset delayModel attribute value
        to empty delayModel json

        :param None
        :return None
        """
        self.logger.info("Disabling the delay generator")
        delay_manager = self.component_manager.delay_manager
        delay_manager.stop()
        delay_manager.start_time_for_delay_calculation = None
        delay_manager.clear_next_polynomial_time()
        if isinstance(
            self.component_manager._input_parameter, InputParameterMid
        ):
            with delay_manager.delay_model_lock:
                if (
                    self.component_manager.delay_model
                    != INITIAL_MID_DELAY_JSON
                ):
                    self.component_manager.delay_model = json.dumps(
                        INITIAL_MID_DELAY_JSON
                    )
        else:
            with delay_manager.delay_model_lock:
                if (
                    self.component_manager.delay_model
                    != INITIAL_LOW_DELAY_JSON
                ):
                    self.component_manager.delay_model = json.dumps(
                        INITIAL_LOW_DELAY_JSON
                    )
            if self.component_manager.number_of_pst_beams > 0:
                delay_manager_pst_beam = (
                    self.component_manager.delay_manager_pst_beam
                )
                delay_manager_pst_beam.clear_next_polynomial_time()
                with delay_manager.delay_model_lock:
                    if (
                        self.component_manager.delay_model_pst_beam
                        != INITIAL_LOW_DELAY_JSON
                    ):
                        self.component_manager.delay_model_pst_beam = (
                            json.dumps(INITIAL_LOW_DELAY_JSON)
                        )
                self.component_manager.number_of_pst_beams = 0
