"""
SetAdminMode command class for CSPSubarrayLeafNode.
"""

# pylint: disable=line-too-long
from typing import Tuple

from ska_control_model import AdminMode
from ska_tango_base.commands import ArgumentValidator, FastCommand, ResultCode

from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)


# pylint: disable = abstract-method
class SetAdminMode(CspSLNCommand, FastCommand):
    """
    A class for CspSubarrayLeafNode's SetAdminMode() command.

    SetAdminMode command on CspSubarrayLeafNode enables to set the adminMode of
    the csp subarray device
    """

    def __init__(self, logger, component_manager):
        """Initialization.

        Args:
            logger (logging.Logger): Used for logging.
            component_manager (CSPSLNcomponentManager): Instance of
            CSPSLNComponentManager.
        """
        super().__init__(logger)
        self.component_manager = component_manager
        self._validator = ArgumentValidator()
        self._name = "SetAdminMode"

    # pylint: disable=signature-differs
    def do(self, argin: AdminMode) -> Tuple[ResultCode, str]:
        """
        A method to set the adminMode of the CSP Subarray device
        :param argin: A adminMode enum value to be set for subarray device
        """
        if self.component_manager.is_admin_mode_enabled:
            return_code, message = self.init_adapter()
            if return_code == ResultCode.FAILED:
                return return_code, message
            try:
                self.csp_subarray_adapter.adminMode = argin
                self.logger.info(
                    "Invoking SetAdminMode command on %s",
                    self.csp_subarray_adapter.dev_name,
                )
            except Exception as e:
                self.logger.info(
                    "Failed to set the adminMode of the CSP Subarray."
                    + " Error while setting the adminMode : %s",
                    e,
                )
                return (ResultCode.FAILED, "Command Failed")
            return (ResultCode.OK, "Command Completed")

        self.logger.info(
            "AdminMode functionality is disabled, "
            + "Device will function normally"
        )
        return ResultCode.NOT_ALLOWED, (
            "AdminMode functionality is disabled, "
            + "Device will function normally"
        )
