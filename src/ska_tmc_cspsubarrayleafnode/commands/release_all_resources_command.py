"""
ReleaseAllResources command class for CSPSubarrayLeafNode.
"""
import logging
import time
from typing import Tuple

from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import TimeKeeper, TimeoutCallback
from ska_tmc_common.v1.error_propagation_tracker import (
    error_propagation_tracker,
)
from ska_tmc_common.v1.timeout_tracker import timeout_tracker

# pylint: disable=line-too-long
from ska_tmc_cspsubarrayleafnode.commands.csp_subarray_leaf_node_command import (  # noqa: E501
    CspSLNCommand,
)

# pylint: enable=line-too-long

configure_logging()
LOGGER = logging.getLogger(__name__)


class ReleaseAllResources(CspSLNCommand):
    """
    A class for CspSubarayLeafNode's ReleaseAllResources() command.

    Releases all the resources of given CSP Subarray Leaf Node.
    """

    def __init__(
        self,
        component_manager,
        logger: logging.Logger = LOGGER,
    ) -> None:
        super().__init__(component_manager, logger)
        self.timeout_id = f"{time.time()}_{__class__.__name__}"
        self.timeout_callback = TimeoutCallback(self.timeout_id, self.logger)
        self.timekeeper = TimeKeeper(
            self.component_manager.command_timeout, logger
        )

    @timeout_tracker
    @error_propagation_tracker(
        "get_obs_state", [ObsState.RESOURCING, ObsState.EMPTY]
    )
    def release_all_resources(
        self,
    ) -> Tuple[ResultCode, str]:
        """
        it executes do hook, invoking ReleaseAllResources command.

        :returns: Result code and message
        :rtype: `Tuple[ResultCode, str]`
        """
        return self.do()

    # Disabling the linting warning as ReleaseAllResources command does not
    # take an input but the signature of the methods requires one.
    # pylint: disable=arguments-differ
    def do_mid(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke ReleaseAllResources command on CSP Subarray.

        :param argin: DevString. The string in JSON format. Defaults to None.

        :return: (ResultCode, message)

        :raises Exception: if the command execution is not successful

        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message
        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.ReleaseAllResources()
        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the ReleaseAllResources "
                    "command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "ReleaseAllResources command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )

        self.component_manager.receptor_ids_str = []
        self.component_manager.subarray_id = None

        return result[0], unique_id_or_message[0]

    def do_low(self) -> Tuple[ResultCode, str]:
        """
        Method to invoke ReleaseAllResources command on CSP Subarray.

        :param argin: DevString. The string in JSON format. Defaults to None.

        :return: (ResultCode, message)

        :raises Exception: if the command execution is not successful

        """
        result_code, message = self.init_adapter()
        if result_code == ResultCode.FAILED:
            return result_code, message

        try:
            (
                result,
                unique_id_or_message,
            ) = self.csp_subarray_adapter.ReleaseAllResources()
        except Exception as exception:
            self.logger.exception(f"Command invocation failed: {exception}")
            return (
                ResultCode.FAILED,
                (
                    "The invocation of the ReleaseAllResources "
                    "command failed on "
                    f"Csp Subarray Device "
                    f"{self.csp_subarray_adapter.dev_name}. "
                    f"Reason: {exception}. The command has NOT been executed. "
                    "This device will continue with normal operation."
                ),
            )
        self.logger.info(
            "ReleaseAllResources command successfully invoked on: %s",
            self.csp_subarray_adapter.dev_name,
        )
        self.component_manager.subarray_id = None

        return result[0], unique_id_or_message[0]
