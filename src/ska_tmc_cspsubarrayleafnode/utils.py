"""Utility module"""
import logging
import math

from ska_ser_logging.configuration import configure_logging

configure_logging()
logger = logging.getLogger(__name__)


def dd_to_dms(argin):
    """
    Converts a number in degree decimal to Deg:Min:Sec.

    :param argin: A number in decimal degrees.
        Example: 30.7129252
    :return: Number in deg:min:sec format.
        Example: 30:42:46.5307 is returned value for input 30.7129252.
    """
    try:
        sign = 1
        if argin < 0:
            sign = -1
        frac_min, degrees = math.modf(abs(argin))
        frac_sec, minutes = math.modf(frac_min * 60)
        seconds = frac_sec * 60
        return f"{int(degrees * sign)}:{int(minutes)}:{round(seconds, 4)}"
    except SyntaxError as error:
        log_msg = (
            f"Error while converting decimal degree to dig:min:sec.{error}"
        )
        logger.error(log_msg)
        return ""  # Return a default value


INITIAL_MID_DELAY_JSON = {
    "interface": "https://schema.skao.int/ska-mid-csp-delaymodel/3.0",
    "start_validity_sec": 0.1,
    "cadence_sec": 0.1,
    "validity_period_sec": 0.1,
    "config_id": "",
    "subarray": 1,
    "receptor_delays": [
        {"receptor": "", "xypol_coeffs_ns": [], "ypol_offset_ns": 0.0},
        {"receptor": "", "xypol_coeffs_ns": [], "ypol_offset_ns": 0.0},
    ],
}

INITIAL_LOW_DELAY_JSON = {
    "interface": "https://schema.skao.int/ska-low-csp-delaymodel/1.0",
    "start_validity_sec": 0.1,
    "cadence_sec": 0.1,
    "validity_period_sec": 0.1,
    "config_id": "",
    "station_beam": 1,
    "subarray": 1,
    "station_beam_delays": [
        {
            "station_id": 1,
            "substation_id": 1,
            "xypol_coeffs_ns": [],
            "ypol_offset_ns": 0.0,
        },
        {
            "station_id": 1,
            "substation_id": 1,
            "xypol_coeffs_ns": [],
            "ypol_offset_ns": 0.0,
        },
    ],
}
