"""DelayManagerLow class"""
import logging
import typing
from datetime import datetime, timedelta
from typing import Any

import katpoint
import numpy as np
from astropy.time import Time
from ska_tango_base.commands import ResultCode

from ska_tmc_cspsubarrayleafnode.const import (
    LOW_DELAYMODEL_VERSION,
    YPOL_OFFSET_NS,
)
from ska_tmc_cspsubarrayleafnode.delay_model import (
    DelayManager,
    get_antenna_params,
)

from .utils import dd_to_dms

if typing.TYPE_CHECKING:
    from ska_tmc_cspsubarrayleafnode.manager.component_manager_low import (
        CspSLNComponentManagerLow,
    )


class DelayManagerLow(DelayManager):
    """A class that calculates geometric delays for SKA Low"""

    def __init__(
        self,
        csp_sln_cm: "CspSLNComponentManagerLow",
        telmodel_source: str,
        telmodel_path: str,
        delay_model_time_in_advance: int,
        delay_cadence: int,
        delay_validity_period: float,
        logger: logging.Logger,
    ):
        """
        Initialize the DelayManagerLow class.

        :param csp_sln_cm: CSP Subarray Leaf Node Configuration Manager
        :param delay_model_time_in_advance: Time in advance to calculate delays
        :param delay_cadence: Cadence for delay calculation
        :param delay_validity_period: Validity period for the calculated delays
        :param logger: Logger for logging information and errors
        """
        super().__init__(
            csp_sln_cm,
            telmodel_source,
            telmodel_path,
            delay_model_time_in_advance,
            delay_cadence,
            logger,
        )
        self.station_names = None
        self._delay_cadence = delay_cadence
        self._delay_validity_period = delay_validity_period

    def create_antenna_objects(
        self, antenna_params: dict[str, Any]
    ) -> list[katpoint.Antenna]:
        """
        Creates KatPoint Antenna objects from antenna parameters.

        Args:
            antenna_params (dict): Dictionary containing antenna parameters.

        Returns:
            list: List of KatPoint Antenna objects.
        """
        antennas: list[katpoint.Antenna] = []
        for antenna in antenna_params["receptors"]:
            antenna_param = get_antenna_params(antenna)
            input_to_antenna = ",".join(
                str(v)
                for v in [
                    antenna_param.station_id,
                    dd_to_dms(antenna_param.location.lat),
                    dd_to_dms(antenna_param.location.lon),
                    antenna_param.location.height,
                    antenna_param.diameter,
                    " ".join(
                        str(x)
                        for x in [0.0, 0.0, 0.0, *antenna_param.fixed_delays]
                    ),
                ]
            )
            antennas.append(katpoint.Antenna(input_to_antenna))
        return antennas

    def update_config_params(self) -> tuple[ResultCode, str]:
        """
        Update configuration parameters related to the resources assigned.

        This method updates parameters every time assign, release, or configure
        commands are executed.

        :return: Tuple of ResultCode and message string
        """
        assigned_stations_dict = {}
        assigned_stations: list[katpoint.Antenna] = []

        # Retrieve telescope model data
        antenna_params, message = self.fetch_antenna_params()
        if message:
            return (ResultCode.FAILED, message + "Number of retries exhausted")

        if "receptors" not in antenna_params:
            return (
                ResultCode.FAILED,
                "Missing receptors key in antenna parameters dictionary.",
            )

        stations = self.create_antenna_objects(antenna_params)

        stations_dict = {ant.name: ant for ant in stations}
        stations_keys_list = stations_dict.keys()
        # station_ids is a list of station IDs lists for example. [1,2]
        for station in self.csp_sln_cm.get_station_ids():
            if str(station) in stations_keys_list:
                assigned_stations.append(stations_dict[str(station)])
                # Create a dictionary including antennas (objects)
                # for the stations assigned to
                # the Subarray
                assigned_stations_dict[str(station)] = stations_dict[
                    str(station)
                ]

        self.logger.info(
            "Assigned stations dictionary: %s", assigned_stations_dict
        )

        ref_station = stations_dict["0"]

        # Create DelayCorrection Object
        self.delay_correction_object = katpoint.DelayCorrection(
            assigned_stations, ref_station
        )
        self.station_names = list(
            self.delay_correction_object.ant_models.keys()
        )

        self.set_dummy_targets()

        self.logger.info("Completed updating config parameters.")
        return (ResultCode.OK, "")

    def set_dummy_targets(self) -> tuple[ResultCode, str]:
        """
        Set dummy targets for the delay correction object.

        :return: Tuple of ResultCode and message string
        """
        # load delay correction for dummy target
        time_t0 = 0
        timestamp_array = [
            time_t0 - 1200,
            time_t0 - 600,
            time_t0,
            time_t0 + 600,
            time_t0 + 1200,
        ]
        dummy_target = katpoint.Target("radec, 180.0, 45.0")
        self.delay_correction_object.delays(dummy_target, timestamp_array)
        return (ResultCode.OK, "Completed setting dummy targets")

    def calculate_geometric_delays(
        self, time_t0: datetime, antenna_names: list[str]
    ) -> dict[str, Any]:
        """
        Calculate geometric delay values using the KATPoint library.

        :param time_t0: Initial time for delay calculation
        :param antenna_names: List of antenna names
        :return: Dictionary containing fifth order polynomial coefficients
          per antenna
        """
        delay_corrections_h_array_dict: dict[str, Any] = {}
        # Create arrays of 20 points array having t0, and 10 points between
        # t0 and validity period 4 points are before t0 and
        # 5 points are comsidered after t0+validity period
        timesteps = self._delay_validity_period / 10
        delta_times = [timestep * timesteps for timestep in range(-4, 16)]
        timestamp_array = [
            (time_t0 + timedelta(seconds=delta_times[i]))
            for i in range(len(delta_times))
        ]
        self.logger.debug("The timestamp_array is %s : ", timestamp_array)
        # Calculate geometric delay values
        # Two values per antenna per timestamp received: horizontal and
        # vertical delay values.
        # So if there are n number of antennas and t timestamps,
        # the matrix received id of dimension (2n x t)
        delays = self.delay_correction_object.delays(
            self.csp_sln_cm.target, timestamp_array
        )

        # Convert delays in seconds to 5th order polynomial coefficients
        # pylint: disable = duplicate-code
        for antenna, antenna_name in enumerate(antenna_names):
            # Array including horizontal delay values per antenna for the
            # given timestamps
            horizontal_delays = np.array(delays[antenna * 2])
            # Fit polynomial to the values over 50-second range
            # Calculating fifth order polynomial coefficient
            polynomial = np.polynomial.Polynomial.fit(
                delta_times, horizontal_delays, 5
            )
            polynomial_coefficients = polynomial.convert().coef
            delay_corrections_h_array_dict[
                antenna_name
            ] = polynomial_coefficients

        return delay_corrections_h_array_dict

    def calculate_delay_model(self) -> None:
        """
        Calculate delays using the KatPoint library and update the delay model
        JSON.
        """
        try:
            self.logger.info("Calculating low delays.")
            # As per ADR-78 Updated time format to TAI format.
            SKA_EPOCH = "1999-12-31T23:59:28Z"
            ska_epoch_utc = Time(SKA_EPOCH, scale="utc")
            ska_epoch_tai = ska_epoch_utc.unix_tai

            if self.start_time_for_delay_calculation is None:
                self.start_time_for_delay_calculation = (
                    self.csp_sln_cm.delay_start_time
                    + timedelta(seconds=self._delay_model_time_in_advance)
                )

            else:
                self.start_time_for_delay_calculation += timedelta(
                    seconds=self._delay_cadence
                )
            self.logger.info(
                "The start time  is %s", self.csp_sln_cm.delay_start_time
            )
            time_t0_utc = Time(
                self.start_time_for_delay_calculation, scale="utc"
            )
            offset_since_epoch = time_t0_utc.unix_tai - ska_epoch_tai

            delay_corrections_h_array_dict = self.calculate_geometric_delays(
                self.start_time_for_delay_calculation, self.station_names
            )

            self.delay_model_json["interface"] = LOW_DELAYMODEL_VERSION
            self.delay_model_json["start_validity_sec"] = offset_since_epoch
            self.delay_model_json["cadence_sec"] = self._delay_cadence
            self.delay_model_json[
                "validity_period_sec"
            ] = self._delay_validity_period
            self.delay_model_json["config_id"] = self.csp_sln_cm.config_id
            self.delay_model_json[
                "station_beam"
            ] = self.csp_sln_cm.subarray_beam_id
            self.delay_model_json["subarray"] = self.csp_sln_cm.subarray_id

            delay_details: list[dict[str, Any]] = []

            # station is a list of station IDs and substation IDs
            # e.g [[1,1],[2,1]]
            for station in self.csp_sln_cm.get_station_substation_ids():
                station_delay_coeffs = delay_corrections_h_array_dict[
                    str(station[0])
                ]
                delay_details_dict: dict[str, Any] = {}
                delay_coeff_array = list(station_delay_coeffs)
                self.logger.info("delay_coeff_array: %s", delay_coeff_array)
                # change suggested by Parentie Team for skb-327
                delay_coeff_array_ns = [
                    coeff * 1e9 for coeff in delay_coeff_array
                ]
                delay_details_dict["station_id"] = int(station[0])
                delay_details_dict["substation_id"] = int(station[-1])
                delay_details_dict["xypol_coeffs_ns"] = delay_coeff_array_ns
                delay_details_dict["ypol_offset_ns"] = YPOL_OFFSET_NS
                delay_details.append(delay_details_dict)

            self.delay_model_json["station_beam_delays"] = delay_details
            log_msg = f"Low Delay Model JSON: {self.delay_model_json}"
            self.logger.info(log_msg)
        except (AttributeError, ValueError, TypeError) as exception:
            self.logger.exception(exception)
