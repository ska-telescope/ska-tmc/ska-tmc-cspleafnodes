"""
Input file to diffrentiate between Low and Mid
"""


class InputParameter:
    """
    Method to Check CSP Subarray Device
    """

    def __init__(self, changed_callback=None, device_name: str = "") -> None:
        self._csp_subarray_dev_name = device_name
        self._changed_callback = changed_callback

    @property
    def csp_subarray_dev_name(self) -> str:
        """Returns device name for the CSP subarray Low Device."""
        return self._csp_subarray_dev_name

    @csp_subarray_dev_name.setter
    def csp_subarray_dev_name(self, device_name: str) -> None:
        """Sets the device name for CSP Subarray Low Device ."""
        self._csp_subarray_dev_name = device_name
        if self._changed_callback is not None:
            self._changed_callback()


class InputParameterLow(InputParameter):
    """
    Method to Check CSP subarray Low Device
    """


class InputParameterMid(InputParameter):
    """
    Method to Check CSP Subarray Mid Device
    """
