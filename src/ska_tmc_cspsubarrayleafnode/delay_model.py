"""
DelayManager class for delay calculation in SKA Mid and SKA Low.
"""
import datetime
import json
import logging
import threading
import time
import typing
from typing import Any, Tuple

from katpoint.delay_model import FIXEDSPEED
from retry import retry
from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_telmodel.data import TMData

if typing.TYPE_CHECKING:
    from ska_tmc_cspsubarrayleafnode.manager.component_manager_low import (
        CspSLNComponentManagerLow,
    )
    from ska_tmc_cspsubarrayleafnode.manager.component_manager_mid import (
        CspSLNComponentManagerMid,
    )

configure_logging()


# pylint: disable=too-few-public-methods
class Location:
    """Location class for storing geodetic coordinates."""

    def __init__(self):
        """Constructor for Location class."""
        self.lat: float = 0.0
        self.lon: float = 0.0
        self.height: float = 0.0


class AntennaParams:
    """Antenna parameters class."""

    def __init__(self):
        """Constructor for AntennaParams class."""
        self.station_id: str = ""
        self.location: Location = Location()
        self.diameter: float = 0.0
        self.station_label: str = ""
        self.fixed_delays: Tuple[float, float] = (0.0, 0.0)


# pylint: enable=too-few-public-methods


def get_antenna_params(antenna_params: dict[str, Any]):
    """
    Returns AntennaParams object from provided antenna parameters.

    :param antenna_params: Dictionary containing antenna parameters.
    :return: AntennaParams object.
    """
    location_param = Location()
    param = AntennaParams()
    location_param.lat = float(antenna_params["location"]["geodetic"]["lat"])
    location_param.lon = float(antenna_params["location"]["geodetic"]["lon"])
    location_param.height = antenna_params["location"]["geodetic"]["h"]
    param.location = location_param
    param.diameter = antenna_params["diameter"]
    param.station_id = antenna_params["station_id"]
    param.station_label = antenna_params["station_label"]

    # Katpoint expects fixed delays in meters instead of nanoseconds and
    # assumes the refractive index. To convert nanoseconds to meters and
    # fix the assumed refractive index, we multiply the delay by the
    # FIXEDSPEED constant and using the delay in seconds.
    fixed_delays: dict[str, float] = {
        d["fixed_delay_id"]: (
            d["delay"] * FIXEDSPEED if d["units"] == "s" else d["delay"]
        )
        for d in antenna_params["fixed_delays"]
    }
    param.fixed_delays = (fixed_delays["FIX_H"], fixed_delays["FIX_V"])
    return param


class DelayManager:
    """
    The class that includes common methods required for delay calculation
    in SKA Mid and SKA Low.
    """

    def __init__(
        self,
        csp_sln_cm: "CspSLNComponentManagerLow | CspSLNComponentManagerMid",
        telmodel_source: str,
        telmodel_path: str,
        delay_model_time_in_advance: int,
        delay_cadence: int,
        logger: logging.Logger | None,
    ):
        """
        Initializes DelayManager with CSP Subarray Leaf Node instance,
        delay calculation parameters, and optional logger.

        :param csp_sln_cm: Instance of CSP Subarray Leaf Node.
        :param delay_model_time_in_advance: Time in seconds to calculate
            advance delay coefficients.
        :param delay_cadence: Delay model update interval in seconds.
        :param logger: Optional logger instance.
        """
        self.logger = logger or logging.getLogger(__name__)
        self.csp_sln_cm = csp_sln_cm
        self.delay_model_json: dict[str, Any] = {}
        self._DELAY_UPDATE_INTERVAL = delay_cadence
        # Create event
        self._stop_delay_model_event = threading.Event()
        # create lock
        self.delay_model_lock = threading.Lock()
        # create thread
        self.delay_model_calculator_thread: threading.Thread | None = None
        self.delay_correction_object = None
        # _delay_model_time_in_advance variable (in seconds) is added to
        # current timestamp and is used to calculate advance delay
        # coefficients.
        self._delay_model_time_in_advance = delay_model_time_in_advance
        self._delay_cadence = delay_cadence
        self.start_time_for_delay_calculation = None

        self._source = telmodel_source
        self._layout_path = telmodel_path
        self.next_polynomial_time = None

    @retry(tries=3, delay=1)
    def fetch_data(self) -> dict[Any, Any]:
        """
        Method to retry using python retry library at frequency of 1 sec.
        This also logs a warning message with the error faced while trying.
        """
        data = TMData([self._source])[self._layout_path].get_dict()
        self.logger.info("Data fetched successfully from telmodel.")
        return data

    def fetch_antenna_params(self) -> Tuple[dict[Any, Any], str]:
        """Fetch antenna params from telmodel."""
        try:
            data = self.fetch_data()
            return data, ""
        except Exception as exception:
            self.logger.error(exception)
            log_msg = (
                f"Exception occurred while fetching the "
                f"station locations: {exception}"
            )
            return {}, log_msg

    def start(self) -> None:
        """Starts the delay model calculator thread."""
        with self.delay_model_lock:
            self.logger.info("Starting delay model calculation.")
            if not self.delay_model_calculator_thread:
                self.delay_model_calculator_thread = threading.Thread(
                    target=self.delay_model_handler,
                    args=[self._DELAY_UPDATE_INTERVAL],
                    daemon=False,
                )
                self.delay_model_calculator_thread.start()

    def stop(self) -> None:
        """Stops the delay model calculator thread."""
        if (
            self.delay_model_calculator_thread
            and self.delay_model_calculator_thread.is_alive()
        ):
            self.logger.info("Finished with delay model calculations.")
            self._stop_delay_model_event.set()
            self.delay_model_calculator_thread.join()
            self.delay_model_calculator_thread = None
            self._stop_delay_model_event.clear()
            self.logger.info("Delay model thread stopped.")

    def update_config_params(self) -> Tuple[ResultCode, str]:
        """
        Updates configuration parameters related to assigned resources
        during assign, release, or configure commands execution.

        Raises:
            NotImplementedError: If the method is not implemented by
                the respective delay manager class.
        """
        raise NotImplementedError(
            "This method must be implemented by respective delay manager class"
        )

    def set_dummy_targets(self) -> Tuple[ResultCode, str]:
        """
        Sets dummy targets for delay calculation.

        Raises:
            NotImplementedError: If the method is not implemented by
                the respective delay manager class.
        """
        raise NotImplementedError(
            "This method must be implemented by respective delay manager class"
        )

    def clear_next_polynomial_time(self):
        """
        This method resets the value of next polynomial time after configure
        command End or Aborted
        """
        self.next_polynomial_time = None

    def delay_model_handler(self, delay_cadence: int):
        """
        Handles the periodic calculation of the delay model.

        Args:
            argin (int): Delay model update interval in seconds.
        """
        cadence_seconds = float(delay_cadence)
        while not self._stop_delay_model_event.is_set():
            if self.next_polynomial_time is None:
                self.next_polynomial_time = time.time()
            self.calculate_delay_model()
            # update the attribute
            with self.delay_model_lock:
                self.csp_sln_cm.delay_model = json.dumps(self.delay_model_json)
            self.next_polynomial_time += cadence_seconds
            next_timestamp = datetime.datetime.utcfromtimestamp(
                self.next_polynomial_time
            ).strftime("%Y-%m-%d %H:%M:%S")
            self.logger.debug("The next polynomial time is %s", next_timestamp)
            # How long to wait before next polynomial is due
            wait_interval = self.next_polynomial_time - time.time()
            # graceful fail if polynomial can't be calculated in time
            if wait_interval < 0.0:
                miss_count = 0
                self.logger.debug(
                    "Wait interval is %s and next polynomial time is %s",
                    wait_interval,
                    self.next_polynomial_time,
                )
                while wait_interval < 0.0:
                    self.next_polynomial_time += cadence_seconds
                    wait_interval += cadence_seconds
                    miss_count += 1
                self.logger.warning("Missed %d polynomials.", miss_count)
            # wait for timer event
            self._stop_delay_model_event.wait(wait_interval)
        self.logger.info("Stop event received. Thread exit.")

    def create_antenna_objects(
        self, antenna_params: AntennaParams
    ) -> NotImplementedError:
        """
        Creates KatPoint Antenna objects from antenna parameters.

        Args:
            antenna_params (dict): Dictionary containing antenna parameters.

        Raises:
            NotImplementedError: If the method is not implemented by
                the respective delay manager class.
        """
        raise NotImplementedError(
            "This method must be implemented by respective delay manager class"
        )

    def calculate_delay_model(self) -> None:
        """
        Calculates the delay model using KatPoint library.

        Raises:
            NotImplementedError: If the method is not implemented by
                the respective delay manager class.
        """
        raise NotImplementedError(
            "This method must be implemented by respective delay manager class"
        )
