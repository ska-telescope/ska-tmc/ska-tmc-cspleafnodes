# -*- coding: utf-8 -*-
#
# This file is part of the CspMasterLeafNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.
# pylint: disable=redefined-builtin

"""Release information for Python Package"""

name = """ska_tmc_cspmasterleafnode"""
version = "0.24.0"
version_info = version.split(".")
description = """ The primary responsibility of the CSP Master
                  Leaf node is to monitor the CSP Master
                  and issue control actions during an observation."""
author = "Team HIMALAYA, Team SAHYADRI"
author_email = "telmgt-internal@googlegroups.com"
license = """BSD-3-Clause"""
url = """https://www.skaobservatory.org"""
copyright = """"""
