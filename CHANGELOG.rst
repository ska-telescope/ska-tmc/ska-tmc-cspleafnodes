###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

## [0.23.7]
### Fixed
- The fixed delays stored in tel-model are now considered for the delay calculation on low
- Released based off of tag 0.23.6

[0.24.0]
* Updated FQDNs and device servers as per ADR-9 in process of resolution of bug SKB-690

## [0.23.6]
### Fixed
- Updated PSTBeamDelayManager class to use the same **start** method as the parent class

## [0.23.5]
### Fixed
- Updated delay Manager class to Resolve SKB-744
- Updated delay Manager class to Resolve SKB-757 by stopping the thread after every **End** command execution and spawning a new thread for the next **Configure** command

## [0.23.4]
### Fixed
- Updated latest common repository to include changes related to index error

## [0.23.3]
### Fixed
- Fixed ReadTheDocs documentation

## [0.23.2]
### Fixed
- Added notify observer in the Abort command to fix SKB-525.

## [0.23.1]
### Fixed
- Resolved bug SKB-658

## [0.23.0]
### Added
- Added functionality to set the adminMode of CSP Controller and CSP Subarray

## [0.21.4]
### Added
- Utilized 0.21.0 of common to fix SKB-525.
- Updated Katpoint to v1.0a3 to fix SKB-666.

## [0.21.3]
### Added
- Added Error Propagation for End, Scan and EndScan Commands.
- Updated delay model handler script to resolve SKB-329.

## [0.21.2]
### Added
- Added retry mechanism while downloading tel-model resources to resolve SKB-495.

## [0.21.1]
### Added
- Updated "timing_beams" configuration as an optional key in Configure JSON

## [0.21.0]
### Added
- Utilized ska-telmodel 1.19.3 to support latest Configure schema for PST Beam Delay Calculation
- Utilized improved liveliness probe from common v0.20.2

## [0.20.0]
### Added
- Utilized improved liveliness probe from common

## [0.19.5]
### Fixed
- Created TANGO properties TelmodelSource and TelmodelPath to configure the telmodel sources

## [0.19.4]
### Fixed
- Removed timeout from DishVCCInit
- Fixed Logger strings.

## [0.19.3]
### Added
- Utilized the latest ska-tmc-common 0.19.7 which clears abort event in tracker thread.

## [0.19.2]
### Added
- Enable polling of all attributes by setting and pushing archive events to resolve SKB-434

## [0.19.1]
### Added
- Fixed SKB-413: Mid Delay Model code pointing to wrong dishes
- Implemented antenna parameters objects to generate according to mid_json layout
- Corrected Mid Delay Model to point to SKA or MKT dish according to assigned receptors
- Modified the unit tests
- Implemented integration test case XTP-58507 to verify SKB-413 fix

### Fixed
- Fixed SKB-413: Mid Delay Model code pointing to wrong dishes

## [0.19.0]
### Added
- CspSubarrayLeafNode now supports **Non-sidereal** Tracking

## [0.18.4]
### Added
- Improved logger statements and update tag of common

## [0.18.3]
### Added
- Update condition for LoadDishCfg command.

## [0.18.2]
### Added
- Fixed edge case scenario for abort by stopping timer thread if tracker thread is not alive
- Added command timeout property
- Fixed commands result code failed handling

## [0.18.1]
### Added
-Enable obsState check for Abort command

## [0.18.0]
### Added
-Made it compliant with base class version 1.0.0

## [0.17.2]
### Added
-Added push mechanism for sourceDishVccConfig

## [0.17.1]
### Added
- Release v0.17.1 since the sha identifier for image ska-tmc-cspleafnodes v0.17.0 on harbor is not matching with the one on gitlab image build job.
- No update in functionality.

## [0.17.0]
### Added
- Implemented a BDD test to verify bug SKB-330
- Fix default interface version for mid and low delaymodel

## [0.16.2]
### Added
- Fixed SKB-328: The low delay calculations are updated to use substation_ids from the lowcbf configuration.
- The station and substation ids are now extracted from lowcbf configuration and accordingly used in low delays calculation.
- Added unit test case to cover scenario with missing "stns" key in lowcbf configuration
- Implemented BDD test case XTP-43414 to verify the SKB-328 bug Fix
- Worked on reducing the intermediate failures for CspSubarrayLeafNode low and mid unit test cases.

## [0.16.1]
### Added
- Restart delay calculation on full configure command
- Stops the delay calculation on End command
- In CSP SA LN Configure, 1st start delay calculation and then invoke Configure on CSP with delay subscription point
- Fix SKB-329
- Fix SKB-327
- Modify the delay integration tests to remove wait added to check delayModel attribute values
- Set initial value for delayModel attribute as json in ADR-88 format instead "" (empty string)
- Fix bug in low delayModel for multiple configure command. Bug- repeated entries in polynomials with same station ids

## [0.16.0]
### Added
- Utilised ska-telmodel v1.15.1 with latest OSO-TMC low Configure schema
- Implement low delay calculations to include Ra-Dec instead Az-El
- Improved mid and low delay start time T0 in accordance with delayCadence
- Corrected the input json for LOW configure command
- As per OSO-TMC low configure interface, extracted stations from apertures for low delay calculations

## [0.15.3]
### Added
- Utilised ska-telmodel v1.15.0

## [0.15.2]
### Fixed
- Fixed commond not allowed exception in ska-tmc-common and utilized ska-tmc-common version 0.15.4

## [0.15.1]
### Added
- Resolved SKB-300
- Implemented test case to verify SKB-300
- Improved Mid and Low delay implementation.

## [0.15.0]
### Added
- Update pytango v9.4.2
- Update ska-tango-base library v0.19.1
- Update ska-tango-base chart v0.4.9
- Update pylint version v3.1.0
- Update ska-tango-util chart v0.4.10

## [0.14.0]
### Added
- Update the low and mid delay json value updated to attribute is incline with ADR-88
  Make delay_cadence and delay_validity period values configurable for both mid and low.

## [0.13.4]
### Added
- When Admin Mode changed to ONLINE then send DishVccValidationResult event

## [0.13.3]
### Added
- Fix Abort related issue in the CSP SLN, where the abort event is not set,
  leading to incorrect event generation.

## [0.13.2]
### Added
- Fixed Interface Key Deletion Issue in Scan Command Class.
