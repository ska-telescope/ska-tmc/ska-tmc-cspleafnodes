import pytest
import tango
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import HealthState
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory
from tango import Database, DeviceProxy

from tests.conftest import CSP_MASTER, CSP_MASTER_LOW
from tests.settings import COMMAND_COMPLETED, logger


@given(
    "a TANGO ecosystem with a set of devices deployed",
    target_fixture="device_list",
)
def device_list():
    db = Database()
    return db.get_device_exported("*")


@given(
    parsers.parse("a CspMasterLeafNode device"),
    target_fixture="cspmasterleaf_node",
)
def cspmasterleaf_node():
    database = Database()
    instance_list = database.get_device_exported_for_class(
        "TmcLeafNodeCspControl"
    )
    for instance in instance_list.value_string:
        return DeviceProxy(instance)

    instance_list = database.get_device_exported_for_class(
        "MidTmcLeafNodeCspControl"
    )
    for instance in instance_list.value_string:
        return DeviceProxy(instance)


@when(parsers.parse("I call the command {command_name}"))
def call_command(cspmasterleaf_node, command_name):
    try:
        if "mid" in cspmasterleaf_node.dev_name():
            csp_master_device = tango.DeviceProxy(CSP_MASTER)
            if csp_master_device.adminMode != 0:
                csp_master_device.adminMode = 0
        else:
            csp_master_device = tango.DeviceProxy(CSP_MASTER_LOW)
            if csp_master_device.adminMode != 0:
                csp_master_device.adminMode = 0
        pytest.command_result = cspmasterleaf_node.command_inout(command_name)
    except Exception as ex:
        assert "DeviceUnresponsive" in str(ex)
        pytest.command_result = "DeviceUnresponsive"


@then(parsers.parse("the command is queued and executed successfully"))
def check_command(
    cspmasterleaf_node,
    group_callback,
):
    dev_factory = DevFactory()
    cspmasterleaf_node_dev = dev_factory.get_device(cspmasterleaf_node)
    cspmasterleaf_node_healthState = cspmasterleaf_node_dev.read_attribute(
        "healthState"
    ).value
    logger.info(
        "Current cspmasterleaf_node_healthState is%s",
        cspmasterleaf_node_healthState,
    )
    assert cspmasterleaf_node_healthState == HealthState.OK

    if pytest.command_result == "DeviceUnresponsive":
        return

    assert pytest.command_result[0][0] == ResultCode.QUEUED
    unique_id = pytest.command_result[1][0]
    lrcr_id = cspmasterleaf_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )

    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id, COMMAND_COMPLETED), lookahead=2
    )
    cspmasterleaf_node.unsubscribe_event(lrcr_id)


scenarios("../ska_tmc_cspmasterleafnode/features/cspmasterleafnode.feature")
