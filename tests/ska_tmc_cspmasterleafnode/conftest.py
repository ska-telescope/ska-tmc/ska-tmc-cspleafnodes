"""Conftest for CSPMLN"""
from os.path import dirname, join

import pytest


def get_input_str(path) -> str:
    """
    Returns input json string
    """
    with open(path, "r", encoding="utf-8") as file:
        input_str = file.read()
    return input_str


@pytest.fixture()
def json_factory():
    """
    Json factory for getting json files
    """

    def _get_json(slug):
        return get_input_str(
            join(dirname(__file__), "..", "data", f"{slug}.json")
        )

    return _get_json
