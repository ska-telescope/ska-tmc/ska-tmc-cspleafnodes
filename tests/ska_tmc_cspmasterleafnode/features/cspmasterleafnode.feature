Feature: CspMasterLeafNode acceptance

	#Test the ability to generically run a a set of commands and that the execution is completed withing 5 seconds.
	@XTP-5330 @post_deployment @acceptance @SKA_mid @SKA_low
	Scenario: Ability to run commands on CspMasterLeafNode
		Given a CspMasterLeafNode device
		When I call the command <command_name>
		Then the command is queued and executed successfully

		Examples:
		| command_name		   |
		| On                   |
        | Off                  |
        | Standby              |


	#Check CspMasterLeafNode node correctly report failed and working devices defined within its scope of monitoring (internal model)
	@post_deployment @acceptance @SKA_mid @SKA_low
	Scenario Outline: Monitor CspMasterLeafNode sub-devices
		Given a TANGO ecosystem with a set of devices deployed