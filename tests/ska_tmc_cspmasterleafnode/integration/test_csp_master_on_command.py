import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory
from tango import DeviceProxy, DevState

from tests.settings import (
    COMMAND_COMPLETED,
    CSP_MASTER_DEVICE_LOW,
    CSP_MASTER_DEVICE_MID,
    CSP_MASTER_LEAF_DEVICE_LOW,
    CSP_MASTER_LEAF_DEVICE_MID,
    logger,
    wait_and_validate_device_attribute_value,
)


def on_command(
    tango_context, cspmln_name, csp_controller_name, group_callback
):
    dev_factory = DevFactory()
    cspmln_node = dev_factory.get_device(cspmln_name)

    csp_controller = DeviceProxy(csp_controller_name)
    result, unique_id = cspmln_node.On()
    logger.info(f"Command ID: {unique_id} Returned result: {result}")
    assert result[0] == ResultCode.QUEUED
    lrcr_id = cspmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=2,
    )
    assert wait_and_validate_device_attribute_value(
        csp_controller, "State", DevState.ON
    )
    cspmln_node.unsubscribe_event(lrcr_id)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_on_command_mid(
    tango_context, group_callback, set_csp_mid_device_admin_mode
):
    on_command(
        tango_context,
        CSP_MASTER_LEAF_DEVICE_MID,
        CSP_MASTER_DEVICE_MID,
        group_callback,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_on_command_low(
    tango_context, group_callback, set_csp_low_device_admin_mode
):
    on_command(
        tango_context,
        CSP_MASTER_LEAF_DEVICE_LOW,
        CSP_MASTER_DEVICE_LOW,
        group_callback,
    )
