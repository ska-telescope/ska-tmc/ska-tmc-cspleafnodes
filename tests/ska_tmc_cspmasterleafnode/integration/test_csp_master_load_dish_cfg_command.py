import json

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory
from tango.db import Database

from tests.settings import (
    COMMAND_COMPLETED,
    CSP_MASTER_DEVICE_MID,
    CSP_MASTER_LEAF_DEVICE_MID,
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_OCCURRED,
    RESET_DEFECT,
    add_device_to_db,
    logger,
    wait_and_validate_device_attribute_value,
)


def invoke_load_dish_cfg(
    cspmln_node, group_callback, dish_cfg_str, csp_master
):
    """Invoke LoadDishCfg command and validate command completed"""
    result, unique_id = cspmln_node.LoadDishCfg(dish_cfg_str)
    logger.info(f"Command ID: {unique_id} Returned result: {result}")
    assert result[0] == ResultCode.QUEUED
    lrcr_id = cspmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=2,
    )
    cspmln_node.unsubscribe_event(lrcr_id)

    # Validate dishVccConfigs attributes
    assert json.loads(cspmln_node.sourceDishVccConfig) == json.loads(
        dish_cfg_str
    )
    # Validate csp Controller leaf node sourceDishVccConfig and cspmln
    # param are same
    assert json.loads(cspmln_node.sourceDishVccConfig) == json.loads(
        csp_master.sourceDishVccConfig
    )


def load_dish_cfg_command(
    tango_context, cspmln_name, group_callback, dish_cfg_str
):
    dev_factory = DevFactory()
    cspmln_node = dev_factory.get_device(cspmln_name)
    csp_master = dev_factory.get_device(CSP_MASTER_DEVICE_MID)

    result, unique_id = cspmln_node.LoadDishCfg(dish_cfg_str)
    logger.info(f"Command ID: {unique_id} Returned result: {result}")
    assert result[0] == ResultCode.QUEUED
    lrcr_id = cspmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    dish_vcc_id = cspmln_node.subscribe_event(
        "DishVccMapValidationResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["DishVccMapValidationResult"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=2,
    )

    group_callback["DishVccMapValidationResult"].assert_change_event(
        str(int(ResultCode.OK)), lookahead=5
    )

    # Validate dishVccConfigs attributes
    assert json.loads(cspmln_node.sourceDishVccConfig) == json.loads(
        dish_cfg_str
    )

    # Validate csp Controller leaf node sourceDishVccConfig and cspmln param
    # are same
    assert json.loads(cspmln_node.sourceDishVccConfig) == json.loads(
        csp_master.sourceDishVccConfig
    )
    cspmln_node.unsubscribe_event(lrcr_id)
    cspmln_node.unsubscribe_event(dish_vcc_id)


def load_dish_cfg_command_csp_defective(
    tango_context, cspmln_name, group_callback, dish_cfg_str
):
    dev_factory = DevFactory()
    cspmln_node = dev_factory.get_device(cspmln_name)
    csp_master = dev_factory.get_device(CSP_MASTER_DEVICE_MID)

    csp_master.SetDefective(ERROR_PROPAGATION_DEFECT)
    result, unique_id = cspmln_node.LoadDishCfg(dish_cfg_str)
    logger.info(f"Command ID: {unique_id} Returned result: {result}")
    assert result[0] == ResultCode.QUEUED
    lrcr_id = cspmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    dish_vcc_id = cspmln_node.subscribe_event(
        "DishVccMapValidationResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["DishVccMapValidationResult"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], EXCEPTION_OCCURRED),
        lookahead=5,
    )

    group_callback["DishVccMapValidationResult"].assert_change_event(
        str(int(ResultCode.FAILED)), lookahead=5
    )

    csp_master.SetDefective(RESET_DEFECT)
    cspmln_node.unsubscribe_event(lrcr_id)
    cspmln_node.unsubscribe_event(dish_vcc_id)


def load_dish_cfg_command_csp_master_leaf_node_init(
    tango_context, cspmln_name, group_callback, dish_cfg_str
):
    """Validate after Csp Controller leaf node restart dish vcc config is set
    properly using memorized attribute
    """
    dev_factory = DevFactory()
    cspmln_node = dev_factory.get_device(cspmln_name)
    csp_master = dev_factory.get_device(CSP_MASTER_DEVICE_MID)
    cspmln_node.set_timeout_millis(5000)
    invoke_load_dish_cfg(cspmln_node, group_callback, dish_cfg_str, csp_master)

    cspmln_node.memorizedDishVccMap = csp_master.sourceDishVccConfig

    # Restart Csp Controller leaf node and assert dish vcc validation
    cspmln_node.init()

    dish_vcc_id = cspmln_node.subscribe_event(
        "DishVccMapValidationResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["DishVccMapValidationResult"],
    )

    group_callback["DishVccMapValidationResult"].assert_change_event(
        str(int(ResultCode.OK)), lookahead=5
    )

    assert json.loads(cspmln_node.sourceDishVccConfig) == json.loads(
        csp_master.sourceDishVccConfig
    )

    assert json.loads(cspmln_node.dishVccConfig) == json.loads(
        csp_master.dishVccConfig
    )

    source_vcc_id = cspmln_node.subscribe_event(
        "sourceDishVccConfig",
        tango.EventType.CHANGE_EVENT,
        group_callback["sourceDishVccConfig"],
    )

    group_callback["sourceDishVccConfig"].assert_change_event(
        csp_master.sourceDishVccConfig,
        lookahead=5,
    )

    cspmln_node.unsubscribe_event(source_vcc_id)
    cspmln_node.unsubscribe_event(dish_vcc_id)


def dish_vcc_validation_when_dish_vcc_version_mismatch(
    tango_context, cspmln_name, group_callback, dish_cfg_str
):
    """ """
    dev_factory = DevFactory()
    cspmln_node = dev_factory.get_device(cspmln_name)
    csp_master = dev_factory.get_device(CSP_MASTER_DEVICE_MID)

    invoke_load_dish_cfg(cspmln_node, group_callback, dish_cfg_str, csp_master)

    cspmln_node.memorizedDishVccMap = '{"a": 1}'

    # Restart Csp Controller leaf node and assert dish vcc validation
    cspmln_node.init()

    dish_vcc_id = cspmln_node.subscribe_event(
        "DishVccMapValidationResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["DishVccMapValidationResult"],
    )

    group_callback["DishVccMapValidationResult"].assert_change_event(
        str(int(ResultCode.FAILED)), lookahead=5
    )

    cspmln_node.unsubscribe_event(dish_vcc_id)


def dish_vcc_validation_when_csp_master_device_unavailable(
    tango_context, cspmln_name, group_callback
):
    """Validate Dish Vcc Validation result set to Not Allowed when
    csp master device is not available
    """
    dev_factory = DevFactory()
    csp_master_device = tango.DeviceProxy("dserver/mocks/01")
    csp_master = dev_factory.get_device(CSP_MASTER_DEVICE_MID)
    db = Database()
    # Delete Csp Master Device
    db.delete_device(CSP_MASTER_DEVICE_MID)
    # Restart the server
    csp_master_device.RestartServer()
    cspmln_node = dev_factory.get_device(cspmln_name)
    dish_vcc_id = cspmln_node.subscribe_event(
        "DishVccMapValidationResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["DishVccMapValidationResult"],
    )
    # Restart Csp Controller leaf node Device so it will run initialization
    # code
    csp_mln_server = tango.DeviceProxy("dserver/csp_master_leaf_node_mid/01")
    csp_mln_server.RestartServer()

    assert wait_and_validate_device_attribute_value(
        cspmln_node,
        "DishVccMapValidationResult",
        str(int(ResultCode.NOT_ALLOWED)),
    )

    # Add Device back to DB
    add_device_to_db(
        device_name="mid-csp/control/0",
        server_name="mocks/01",
        class_name="HelperCspMasterDevice",
    )
    csp_master_device.RestartServer()
    assert wait_and_validate_device_attribute_value(
        csp_master, "State", tango.DevState.DISABLE
    )
    cspmln_node.unsubscribe_event(dish_vcc_id)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_load_dish_cfg_command_mid(
    tango_context, group_callback, json_factory
):
    load_dish_cfg_command(
        tango_context,
        CSP_MASTER_LEAF_DEVICE_MID,
        group_callback,
        json_factory("cspmln_load_dish_cfg_command"),
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_load_dish_cfg_command_mid_when_csp_is_defective(
    tango_context, group_callback, json_factory
):
    load_dish_cfg_command_csp_defective(
        tango_context,
        CSP_MASTER_LEAF_DEVICE_MID,
        group_callback,
        json_factory("cspmln_load_dish_cfg_command"),
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_load_dish_cfg_command_mid_init(
    tango_context, group_callback, json_factory, set_admin_mode_to_online
):
    load_dish_cfg_command_csp_master_leaf_node_init(
        tango_context,
        CSP_MASTER_LEAF_DEVICE_MID,
        group_callback,
        json_factory("cspmln_load_dish_cfg_command"),
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_dish_vcc_validation_when_dish_vcc_version_mismatch(
    tango_context, group_callback, json_factory, set_admin_mode_to_online
):
    dish_vcc_validation_when_dish_vcc_version_mismatch(
        tango_context,
        CSP_MASTER_LEAF_DEVICE_MID,
        group_callback,
        json_factory("cspmln_load_dish_cfg_command"),
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_dish_vcc_validation_when_csp_master_unavailable(
    tango_context, group_callback, json_factory, set_admin_mode_to_online
):
    dish_vcc_validation_when_csp_master_device_unavailable(
        tango_context, CSP_MASTER_LEAF_DEVICE_MID, group_callback
    )
