import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory

from tests.settings import (
    COMMAND_COMPLETED,
    CSP_MASTER_LEAF_DEVICE_LOW,
    CSP_MASTER_LEAF_DEVICE_MID,
    logger,
)


def on_command(
    cspmln_name,
    group_callback,
):
    dev_factory = DevFactory()

    cspmln_node = dev_factory.get_device(cspmln_name)
    sa_id = cspmln_node.subscribe_event(
        "isSubsystemAvailable",
        tango.EventType.CHANGE_EVENT,
        group_callback["isSubsystemAvailable"],
    )
    group_callback["isSubsystemAvailable"].assert_change_event(
        True,
        lookahead=4,
    )
    result, unique_id = cspmln_node.On()
    logger.info(f"Command ID: {unique_id} Returned result: {result}")
    assert result[0] == ResultCode.QUEUED
    lrcr_id = cspmln_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        group_callback["longRunningCommandResult"],
    )
    group_callback["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=2,
    )
    cspmln_node.unsubscribe_event(lrcr_id)
    cspmln_node.unsubscribe_event(sa_id)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_on_command_mid(group_callback, set_csp_mid_device_admin_mode):
    on_command(CSP_MASTER_LEAF_DEVICE_MID, group_callback)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_on_command_low(group_callback, set_csp_low_device_admin_mode):
    on_command(CSP_MASTER_LEAF_DEVICE_LOW, group_callback)
