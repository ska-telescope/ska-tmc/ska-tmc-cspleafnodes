import pytest
from ska_control_model import AdminMode
from ska_tango_base.commands import ResultCode

from ska_tmc_cspmasterleafnode.csp_master_leaf_node import SetAdminMode
from tests.settings import create_cm_cspmln, logger


@pytest.mark.cspmln_mid
def test_set_admin_mode_command_mid(
    tango_context,
    csp_master_device_mid,
):
    """Test to set the adminMode on csp master"""
    cm, _ = create_cm_cspmln(csp_master_device_mid)
    argin = AdminMode.ONLINE
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)

    assert result_code == ResultCode.OK
    assert message == "Command Completed"


@pytest.mark.cspmln_mid
def test_invalid_admin_mode_command_mid(
    tango_context,
    csp_master_device_mid,
):
    """Test to set the adminMode on csp master"""
    cm, _ = create_cm_cspmln(csp_master_device_mid)
    argin = 7  # arbitary adminMode value
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)

    assert result_code == ResultCode.FAILED
    assert message == "Command Failed"


@pytest.mark.cspmln_mid
def test_feature_toggle_adminMode(tango_context, csp_master_device_mid):
    """Test to set the adminMode on csp master"""
    cm, _ = create_cm_cspmln(csp_master_device_mid)
    cm.is_admin_mode_enabled = False
    argin = AdminMode.ONLINE  # arbitary adminMode value
    set_admin_mode = SetAdminMode(logger=logger, component_manager=cm)
    result_code, message = set_admin_mode.do(argin)

    assert result_code == ResultCode.NOT_ALLOWED
    assert message == (
        "AdminMode functionality is disabled, "
        + "Device will function normally"
    )
