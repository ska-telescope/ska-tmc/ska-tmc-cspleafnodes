import json

import mock
import pytest
from ska_tango_base.commands import ResultCode, TaskStatus

from ska_tmc_cspmasterleafnode.manager.dish_vcc_validation_manager import (
    DishVccMapValidationManager,
)
from tests.settings import create_cm_cspmln


@pytest.mark.cspmln_mid
def test_mid_load_dish_cfg_cmd(
    tango_context,
    csp_master_device_mid,
    task_callback,
    json_factory,
    set_admin_mode_csp_to_online_mid,
):
    """Validate Load Dish Cfg command"""
    dish_cfg_str = json_factory("cspmln_load_dish_cfg_command")
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")

    cm.sys = mock.Mock()
    cm.update_dish_vcc_config_callback = mock.Mock()
    cm.handle_updated_dish_vcc_config = mock.Mock()
    assert cm.is_command_allowed("LoadDishCfg")

    cm.load_dish_cfg_command(dish_cfg_str, task_callback=task_callback)
    cm.received_source_dish_vcc_config_event_value = dish_cfg_str
    cm.received_dish_vcc_config_event_value = json.dumps(
        {
            "interface": "https://schema.skao.int/"
            "ska-mid-cbf-initial-parameters/2.2",
            "dish_parameters": {
                "SKA001": {"vcc": 1, "k": 11},
                "SKA100": {"vcc": 2, "k": 101},
                "SKA036": {"vcc": 3, "k": 1127},
                "SKA063": {"vcc": 4, "k": 620},
            },
        }
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


@pytest.mark.cspmln_mid
def test_mid_load_dish_cfg_cmd_invalid_params(
    tango_context,
    csp_master_device_mid,
    task_callback,
    json_factory,
    set_admin_mode_csp_to_online_mid,
):
    """Validate Load Dish Cfg command"""
    dish_cfg_str = json_factory("cspmln_load_dish_cfg_invalid")
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")

    assert cm.is_command_allowed("LoadDishCfg")
    return_code, message = cm.load_dish_cfg_command(
        dish_cfg_str, task_callback=task_callback
    )
    assert return_code == TaskStatus.REJECTED
    expected_message = (
        "Error in Loading Dish VCC map json file"
        " 'No telescope model data with key "
        "instrument/dishid_vcc_map_configuration"
        "/mid_cbf_initial_parameters_invalid.json exists!'"
    )
    assert message == expected_message


@pytest.mark.cspmln_mid
def test_mid_load_dish_cfg_cmd_is_dish_vcc_config_valid(
    tango_context,
    csp_master_device_mid,
    task_callback,
    json_factory,
    set_admin_mode_csp_to_online_mid,
):
    """Validate Load Dish Cfg command"""
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")
    is_valid, error_message = cm.is_dish_vcc_config_valid()
    assert not is_valid
    assert error_message == "Dish VCC config not received from CSP"

    # Test when same value set is dish vcc config return true
    cm.source_dish_vcc_config_sent = {"a": 1}
    cm.received_dish_vcc_config_event_value = '{"b": 1}'
    cm.dish_vcc_config_sent = {"b": 1}
    cm.received_source_dish_vcc_config_event_value = '{"a": 1}'
    is_valid, error_message = cm.is_dish_vcc_config_valid()
    assert is_valid

    cm.received_source_dish_vcc_config_event_value = '{"c": 1}'
    expected_error_message = (
        " Sent source dish VCC config and received source"
        "dish VCC config do not match."
    )
    is_valid, error_message = cm.is_dish_vcc_config_valid()
    assert not is_valid
    assert error_message == expected_error_message

    cm.dish_vcc_config_sent = {"d": 1}
    is_valid, error_message = cm.is_dish_vcc_config_valid()
    expected_error_message = (
        "Sent dish VCC config and received dish VCC config "
        "from CSP controller do not match. Sent source dish VCC config "
        "and received sourcedish VCC config do not match."
    )
    assert not is_valid
    assert error_message == expected_error_message


@pytest.mark.cspmln_mid
def test_dish_vcc_validation_manager(
    tango_context, csp_master_device_mid, set_admin_mode_csp_to_online_mid
):
    """Validate when dish vcc map version same on csp master and
    csp Controller leaf node then validation result set to OK
    """
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")
    cm.memorized_dish_vcc_map = '{"a": 1}'
    dish_vcc_validation_manager = DishVccMapValidationManager(cm, cm.logger)
    dish_vcc_validation_manager.csp_master_source_dish_vcc = '{"a": 1}'
    dish_vcc_validation_manager.validate_dish_vcc_version()

    assert cm.dish_vcc_validation_result == ResultCode.OK


@pytest.mark.cspmln_mid
def test_dish_vcc_validation_manager_for_fail_result(
    tango_context, csp_master_device_mid, set_admin_mode_csp_to_online_mid
):
    """Validate when dish vcc map version different on csp master
    and csp Controller leaf node then validation result set to FAILED
    """
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")
    cm.memorized_dish_vcc_map = '{"b": 1}'
    dish_vcc_validation_manager = DishVccMapValidationManager(cm, cm.logger)
    dish_vcc_validation_manager.csp_master_source_dish_vcc = '{"a": 1}'
    dish_vcc_validation_manager.validate_dish_vcc_version()

    assert cm.dish_vcc_validation_result == ResultCode.FAILED


@pytest.mark.cspmln_mid
def test_dish_vcc_validation_manager_for_fail(
    tango_context, csp_master_device_mid, set_admin_mode_csp_to_online_mid
):
    """Validate when dish vcc map version different on csp master
    and csp Controller leaf node then validation result set to FAILED
    """
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")
    cm.memorized_dish_vcc_map = '{"b": 1}'
    dish_vcc_validation_manager = DishVccMapValidationManager(cm, cm.logger)
    dish_vcc_validation_manager.csp_master_source_dish_vcc = '{"a": 1}'
    dish_vcc_validation_manager.validate_dish_vcc_version()

    assert cm.dish_vcc_validation_result == ResultCode.FAILED


@pytest.mark.cspmln_mid
def test_dish_vcc_validation_manager_missing_vcc(
    tango_context, csp_master_device_mid, set_admin_mode_csp_to_online_mid
):
    """Validate when one of the dish vcc map versions is missing,
    then validation result is set to FAILED.
    """
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")
    cm.memorized_dish_vcc_map = ""
    dish_vcc_validation_manager = DishVccMapValidationManager(cm, cm.logger)
    dish_vcc_validation_manager.csp_master_source_dish_vcc = '{"a": 1}'
    dish_vcc_validation_manager.validate_dish_vcc_version()

    assert cm.dish_vcc_validation_result == ResultCode.FAILED


@pytest.mark.cspmln_mid
def test_dish_vcc_validation_manager_both_missing_vcc(
    tango_context, csp_master_device_mid, set_admin_mode_csp_to_online_mid
):
    """Validate when both dish vcc map versions are missing,
    then validation result is set to UNKNOWN.
    """
    cm, _ = create_cm_cspmln(csp_master_device_mid, device_type="mid")
    cm.memorized_dish_vcc_map = ""
    dish_vcc_validation_manager = DishVccMapValidationManager(cm, cm.logger)
    dish_vcc_validation_manager.csp_master_source_dish_vcc = ""
    dish_vcc_validation_manager.validate_dish_vcc_version()

    assert cm.dish_vcc_validation_result == ResultCode.UNKNOWN
