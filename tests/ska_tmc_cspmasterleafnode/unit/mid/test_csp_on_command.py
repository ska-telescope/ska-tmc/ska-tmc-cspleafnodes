import mock
import pytest
from ska_tango_base.commands import ResultCode, TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import CommandNotAllowed, DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_cspmasterleafnode.commands.on_command import On
from tests.settings import create_cm_cspmln, logger, wait_for_unresponsive


@pytest.mark.cspmln_mid
def test_mid_on(
    tango_context,
    csp_master_device_mid,
    task_callback,
    set_admin_mode_csp_to_online_mid,
):
    cm, _ = create_cm_cspmln(csp_master_device_mid)
    assert cm.is_command_allowed("On")

    cm.on_command(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        call_kwargs={
            "status": TaskStatus.COMPLETED,
            "result": (ResultCode.OK, "Command Completed"),
        }
    )


@pytest.mark.cspmln_mid
def test_mid_on_not_allowed(
    tango_context, csp_master_device_mid, set_admin_mode_csp_to_online_mid
):
    cm, _ = create_cm_cspmln(csp_master_device_mid)
    cm.op_state_model._op_state = DevState.FAULT
    with pytest.raises(CommandNotAllowed):
        cm.is_command_allowed("On")


@pytest.mark.cspmln_mid
def test_mid_on_fail_csp_master(
    tango_context,
    csp_master_device_mid,
    task_callback,
    set_admin_mode_csp_to_online_mid,
):
    cm, _ = create_cm_cspmln(csp_master_device_mid)
    adapter_factory = HelperAdapterFactory()
    cm.csp_master_device_name = csp_master_device_mid

    # include exception in On command
    attrs = {"On.side_effect": Exception}
    cspcontrollerMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_master_device_mid, AdapterType.CSPMASTER, proxy=cspcontrollerMock
    )

    on_command = On(cm, logger)
    on_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("On")
    on_command.on(logger, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the On command is failed on CSP Master"
            + " device mid-csp/control/0.\nThe following exception"
            + " occurred - .",
        ),
        exception="The invocation of the On command is failed on"
        + " CSP Master device "
        + "mid-csp/control/0.\nThe following exception occurred - .",
    )


@pytest.mark.cspmln_mid
def test_on_command_is_allowed_device_unresponsive(
    tango_context, csp_master_device_mid
):
    cm, _ = create_cm_cspmln(csp_master_device_mid)
    cm._device = DeviceInfo(csp_master_device_mid, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(DeviceUnresponsive):
        cm.is_command_allowed("On")
