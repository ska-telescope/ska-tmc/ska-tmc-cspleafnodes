import pytest
from ska_control_model import AdminMode, HealthState
from ska_tango_base.control_model import ControlMode, SimulationMode, TestMode
from tango import DevState

from ska_tmc_cspmasterleafnode import release


@pytest.mark.cspmln_mid
def test_attributes(cspmln_device):
    assert cspmln_device.State() == DevState.ON
    cspmln_device.loggingTargets = ["console::cout"]
    assert "console::cout" in cspmln_device.loggingTargets
    cspmln_device.testMode = TestMode.NONE
    assert cspmln_device.testMode == TestMode.NONE
    cspmln_device.simulationMode = SimulationMode.FALSE
    assert cspmln_device.testMode == SimulationMode.FALSE
    cspmln_device.controlMode = ControlMode.REMOTE
    assert cspmln_device.controlMode == ControlMode.REMOTE
    cspmln_device.cspMasterDevName = "cspmln"
    assert cspmln_device.cspMasterDevName == "cspmln"
    assert cspmln_device.versionId == release.version
    assert cspmln_device.buildState == (
        "{},{},{}".format(release.name, release.version, release.description)
    )
    assert cspmln_device.healthState == HealthState.OK
    assert cspmln_device.cspControllerAdminMode == AdminMode.ONLINE
    assert cspmln_device.isAdminModeEnabled is True
