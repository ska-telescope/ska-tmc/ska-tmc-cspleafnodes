import mock
import pytest
from ska_tango_base.commands import ResultCode, TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import CommandNotAllowed, DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)
from tango import DevState

from ska_tmc_cspmasterleafnode.commands.off_command import Off
from tests.settings import create_cm_cspmln, logger, wait_for_unresponsive


@pytest.mark.cspmln_low
def test_low_off(
    tango_context,
    csp_master_device_low,
    task_callback,
    set_admin_mode_csp_to_online_low,
):
    cm, _ = create_cm_cspmln(csp_master_device_low)
    assert cm.is_command_allowed("Off")
    cm.off_command(task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspmln_low
def test_off_not_allowed_low(
    tango_context, csp_master_device_low, set_admin_mode_csp_to_online_low
):
    cm, _ = create_cm_cspmln(csp_master_device_low)
    cm.op_state_model._op_state = DevState.FAULT
    with pytest.raises(CommandNotAllowed):
        cm.is_command_allowed("Off")


@pytest.mark.cspmln_low
def test_low_off_fail_csp_master(
    tango_context,
    csp_master_device_low,
    task_callback,
    set_admin_mode_csp_to_online_low,
):
    cm, _ = create_cm_cspmln(csp_master_device_low)
    adapter_factory = HelperAdapterFactory()
    cm.csp_master_device_name = csp_master_device_low

    # include exception in Off command
    attrs = {"Off.side_effect": Exception}
    cspcontrollerMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_master_device_low, AdapterType.CSPMASTER, proxy=cspcontrollerMock
    )

    off_command = Off(cm, logger)
    off_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("Off")
    off_command.off(logger, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            "The invocation of the Off command is failed on CSP Master "
            + "device low-csp/control/0.\nThe following exception "
            + "occurred - .",
        ),
        exception="The invocation of the Off command is"
        + " failed on CSP Master device"
        + " low-csp/control/0.\nThe following exception occurred - .",
    )


@pytest.mark.cspmln_low
def test_off_command_is_not_allowed_device_unresponsive(csp_master_device_low):
    cm, _ = create_cm_cspmln(csp_master_device_low)
    cm._device = DeviceInfo(csp_master_device_low, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(DeviceUnresponsive):
        cm.is_command_allowed("Off")
