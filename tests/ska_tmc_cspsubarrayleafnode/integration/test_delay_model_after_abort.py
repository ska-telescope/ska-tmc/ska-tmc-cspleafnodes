import json

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_telmodel.schema import validate as telmodel_validate
from ska_tmc_common.dev_factory import DevFactory

from src.ska_tmc_cspsubarrayleafnode.const import (
    LOW_DELAYMODEL_VERSION,
    MID_DELAYMODEL_VERSION,
)
from tests.conftest import (
    CSPSUBARRAYLEAFNODE_LOW,
    CSPSUBARRAYLEAFNODE_MID,
    LOW_CSP_SUBARRAY,
    MID_CSP_SUBARRAY,
)
from tests.settings import (
    COMMAND_COMPLETED,
    LOW_DELAY_JSON,
    MID_DELAY_JSON,
    logger,
    wait_and_validate_device_attribute_value,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)


def delay_model_after_abort(
    tango_context,
    cspsln_name,
    csp_sa_name,
    assign_input_str,
    configure_input_str,
    change_event_callbacks,
    json_factory,
    expected_delaymodel_json,
    delay_version,
):
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(cspsln_name)

    csp_subarray = dev_factory.get_device(csp_sa_name)
    try:
        result, unique_id = invoke_assign_resource(
            cspsal_node, assign_input_str
        )

        lrcr_id = cspsal_node.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)
        result, unique_id = cspsal_node.Configure(configure_input_str)
        assert unique_id[0].endswith("Configure")
        assert result[0] == ResultCode.QUEUED

        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        cspsal_node.unsubscribe_event(lrcr_id)
        delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
        assert delay_json != json.dumps(expected_delaymodel_json)
        assert wait_and_validate_device_attribute_value(
            cspsal_node,
            "delayModel",
            MID_DELAY_JSON,
            negation=True,
            is_json=True,
        )
        delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
        logger.info("The DelayJson is %s", delay_json)
        telmodel_validate(
            version=delay_version,
            config=delay_json,
            strictness=2,
        )
        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)
        # Invoke Abort command
        result, unique_id = cspsal_node.Abort()
        delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
        assert delay_json == expected_delaymodel_json
        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.ABORTED)
        tear_down(
            dev_factory, csp_subarray, cspsal_node, change_event_callbacks
        )

    except Exception:
        tear_down(
            dev_factory, csp_subarray, cspsal_node, change_event_callbacks
        )
        raise


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_low_delay_model_after_abort(
    tango_context,
    json_factory,
    change_event_callbacks,
    json_schema_factory,
    set_csp_low_device_admin_mode,
):
    return delay_model_after_abort(
        tango_context,
        CSPSUBARRAYLEAFNODE_LOW,
        LOW_CSP_SUBARRAY,
        json_factory("cspsln_assignresources_command_low"),
        json_factory("cspsln_configure_command_low"),
        change_event_callbacks,
        json_schema_factory,
        LOW_DELAY_JSON,
        LOW_DELAYMODEL_VERSION,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_mid_delay_model_after_abort(
    tango_context,
    json_factory,
    change_event_callbacks,
    json_schema_factory,
    set_csp_mid_device_admin_mode,
):
    return delay_model_after_abort(
        tango_context,
        CSPSUBARRAYLEAFNODE_MID,
        MID_CSP_SUBARRAY,
        json_factory("cspsln_AssignResources_command"),
        json_factory("cspsln_configure_command"),
        change_event_callbacks,
        json_schema_factory,
        MID_DELAY_JSON,
        MID_DELAYMODEL_VERSION,
    )
