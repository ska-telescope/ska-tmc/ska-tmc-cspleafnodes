import json

import pytest
import tango
from pytest_bdd import given, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from tango import DevState

from tests.conftest import (
    CSPSUBARRAYLEAFNODE_MID,
    LOW_CSP_SUBARRAY,
    MID_CSP_SUBARRAY,
)
from tests.settings import (
    COMMAND_COMPLETED,
    LOW_DELAY_JSON,
    MID_DELAY_JSON,
    dev_factory,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    check_differences_between_delay_times,
    get_multiple_generated_delay_time_values,
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)

TIMEOUT = 15


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
@pytest.mark.SKA_low
@scenario(
    "../ska_tmc_cspsubarrayleafnode/features/skb-330.feature",
    "Fix bug SKB-330 in TMC",
)
def test_skb330() -> None:
    """
    Test case to verify SKB-330
    """


@given("the CSP Subarray Leaf Node is in ON state")
def check_csp_subarray_leaf_node_is_on(
    tango_context,
    cspsal_node,
):
    if "mid" in cspsal_node.dev_name():
        csp_subarray_device = tango.DeviceProxy(MID_CSP_SUBARRAY)
        if csp_subarray_device.adminMode != 0:
            csp_subarray_device.adminMode = 0
    else:
        csp_subarray_device = tango.DeviceProxy(LOW_CSP_SUBARRAY)
        if csp_subarray_device.adminMode != 0:
            csp_subarray_device.adminMode = 0

    assert cspsal_node.State() == DevState.ON


@given("CSP Subarray Leaf Node in obsState IDLE")
def check_csp_subarray_leaf_obsstate_in_idle(
    json_factory, change_event_callbacks, cspsal_node
):
    if cspsal_node.dev_name() == CSPSUBARRAYLEAFNODE_MID:
        assign_input_str = json_factory("cspsln_AssignResources_command")
    else:
        assign_input_str = json_factory("cspsln_assignresources_command_low")

    _, unique_id = invoke_assign_resource(cspsal_node, assign_input_str)

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=5,
    )
    logger.info(f"AssignResources {unique_id}  completed")

    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)
    cspsal_node.unsubscribe_event(lrcr_id)


@when(
    "Configure command is successfully executed on the CSP subarray "
    + "Leaf Node"
)
def execute_configure_command(
    json_factory,
    change_event_callbacks,
    cspsal_node,
) -> None:
    if cspsal_node.dev_name() == CSPSUBARRAYLEAFNODE_MID:
        configure_input_str = json_factory("cspsln_configure_command")
    else:
        configure_input_str = json_factory("cspsln_configure_command_low")
    result, unique_id = cspsal_node.Configure(configure_input_str)
    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")
    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=8
    )

    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)
    cspsal_node.unsubscribe_event(lrcr_id)


@then("CSP subarray leaf node starts generating delay values")
def check_if_delay_values_are_generating(cspsal_node) -> None:
    delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
    logger.info(f"Delay Json:{delay_json}")
    if cspsal_node.dev_name() == CSPSUBARRAYLEAFNODE_MID:
        assert delay_json != json.dumps(MID_DELAY_JSON)
    else:
        assert delay_json != json.dumps(LOW_DELAY_JSON)


@then(
    "in each delay iteration the start validity time should be incremented "
    + "by delay cadence"
)
def check_the_difference_between_start_validity_and_cadence(
    cspsal_node, change_event_callbacks
) -> None:
    if cspsal_node.dev_name() == CSPSUBARRAYLEAFNODE_MID:
        csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
        # Get the multiple delay generation times vs start validity time values
        (
            start_validity_time_list,
            delay_cadence,
        ) = get_multiple_generated_delay_time_values(
            cspsal_node, TIMEOUT, MID_DELAY_JSON, 5
        )
    else:
        csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
        # Get the multiple start validity time values
        (
            start_validity_time_list,
            delay_cadence,
        ) = get_multiple_generated_delay_time_values(
            cspsal_node, TIMEOUT, LOW_DELAY_JSON, 2
        )

    # Check whether the difference between the consecutive start validity
    # times is equal to delay cadence
    assert check_differences_between_delay_times(
        start_validity_time_list, delay_cadence
    )

    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)
