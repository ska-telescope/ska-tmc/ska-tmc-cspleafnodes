"""Test module to verify SKB-328"""
import pytest
import tango
from pytest_bdd import given, scenario, then, when
from ska_control_model import AdminMode
from ska_tango_base.control_model import ObsState
from ska_tango_testing.mock.placeholders import Anything

from tests.settings import (
    COMMAND_COMPLETED,
    CSPSUBARRAYLEAFNODE_LOW,
    LOW_CSP_SUBARRAY,
    dev_factory,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
@scenario(
    "../ska_tmc_cspsubarrayleafnode/features/test_admin_mode.feature",
    "Test admin mode functionality csp subarray",
)
def test_admin_mode_verification() -> None:
    """
    Test case to verify admin mode functionality
    """


@given("the CSP Subarray is in adminMode ONLINE set by leafnode")
def set_csp_subarray_admin_mode(
    tango_context,
) -> None:
    """Set the adminMode of csp subarray to ONLINE"""
    low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
    low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    try:
        low_csp_saln.setadminmode(AdminMode.ONLINE)
        assert low_csp_subarray.adminMode == AdminMode.ONLINE
    except Exception as e:
        logger.info("not able to set the adminMode %s", e)


@given("CSP Subarray is in IDLE obsState after AssignResources command")
def invoke_assign_resources_command(
    tango_context, json_factory, change_event_callbacks
):
    """Ensure CspSubarrayLeafNode is in obsState IDLE"""
    assign_json_str = json_factory("cspsln_assignresources_command_low")
    low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
    try:
        _, unique_id = invoke_assign_resource(low_csp_saln, assign_json_str)
        lrcr_id = low_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        wait_for_final_csp_subarray_obsstate(low_csp_saln, ObsState.IDLE)
        low_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, low_csp_subarray, low_csp_saln, change_event_callbacks
        )
        raise


@when("adminMode of CSP Subarray is set to OFFLINE by leafnode")
def set_admin_mode_to_offline(tango_context):
    """set the admin mode to offline"""
    low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
    low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    try:
        low_csp_saln.setadminmode(AdminMode.OFFLINE)
        assert low_csp_subarray.adminMode == AdminMode.OFFLINE
    except Exception as e:
        logger.info("not able to set the adminMode %s", e)


@then("CSP Subarray denies invocation of releaseresources command")
def execute_configure(json_factory, change_event_callbacks) -> None:
    """Execute Configure on CspSubarrayLeafNode"""
    low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
    try:
        _, unique_id = low_csp_saln.ReleaseAllResources()
        lrcr_id = low_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], Anything),
            lookahead=5,
        )
        low_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, low_csp_subarray, low_csp_saln, change_event_callbacks
        )
        raise


@then("CSP Subarray remains in IDLE obsState")
def check_csp_subarray_obsstate(change_event_callbacks):
    """check the csp subarray obsstate"""
    low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
    low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    assert low_csp_subarray.obsstate == ObsState.IDLE
    # tear down
    low_csp_saln.setadminmode(AdminMode.ONLINE)
    tear_down(
        low_csp_saln,
        low_csp_subarray,
        low_csp_saln,
        change_event_callbacks,
    )
