import json

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory

from tests.settings import (
    COMMAND_NOT_ALLOWED_AFTER_QUEUING,
    RESET_DEFECT,
    get_csp_devices,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import tear_down

dev_factory = DevFactory()


def assign_resources_not_allowed(
    tango_context,
    deployment,
    assign_input_str,
    change_event_callbacks,
) -> None:
    cspsal_node, csp_subarray = get_csp_devices(deployment)

    csp_subarray.SetDefective(COMMAND_NOT_ALLOWED_AFTER_QUEUING)

    result, unique_id = cspsal_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (
            unique_id[0],
            json.dumps([ResultCode.FAILED, "Command is not allowed"]),
        ),
        lookahead=12,
    )

    csp_subarray.SetDefective(RESET_DEFECT)
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_assign_resources_not_allowed_mid(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_mid_device_admin_mode,
):
    return assign_resources_not_allowed(
        tango_context,
        "mid",
        json_factory("cspsln_AssignResources_command"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assign_resources_not_allowed_low(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_low_device_admin_mode,
):
    return assign_resources_not_allowed(
        tango_context,
        "low",
        json_factory("cspsln_assignresources_command_low"),
        change_event_callbacks,
    )
