import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common.dev_factory import DevFactory

from tests.conftest import (
    CSPSUBARRAYLEAFNODE_LOW,
    CSPSUBARRAYLEAFNODE_MID,
    LOW_CSP_SUBARRAY,
    MID_CSP_SUBARRAY,
)
from tests.settings import (
    COMMAND_COMPLETED,
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_OCCURRED,
    RESET_DEFECT,
    TIMEOUT_DEFECT,
    TIMEOUT_OCCURRED,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)

dev_factory = DevFactory()


def get_csp_devices(cspsln_name):
    """Return csp devices proxy object"""
    cspsal_node = dev_factory.get_device(cspsln_name)
    if cspsln_name == CSPSUBARRAYLEAFNODE_MID:
        csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    else:
        csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    return cspsal_node, csp_subarray


def configure_resources_timeout(
    tango_context,
    cspsln_name,
    assign_input_str,
    configure_input_str,
    change_event_callbacks,
) -> None:
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(cspsln_name)

    if cspsln_name == CSPSUBARRAYLEAFNODE_MID:
        csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    else:
        csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)

    result, unique_id = invoke_assign_resource(cspsal_node, assign_input_str)

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=5,
    )
    logger.info(
        f"AssignResources {unique_id}  completed , starting with configure"
    )
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)

    csp_subarray.SetDefective(TIMEOUT_DEFECT)

    result, unique_id = cspsal_node.Configure(configure_input_str)
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")

    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], TIMEOUT_OCCURRED),
        lookahead=12,
    )

    csp_subarray.SetDefective(RESET_DEFECT)
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


def configure_error_propogation(
    tango_context,
    cspsln_name,
    assign_input_str,
    configure_input_str,
    change_event_callbacks,
) -> None:
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(cspsln_name)

    if cspsln_name == CSPSUBARRAYLEAFNODE_MID:
        csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    else:
        csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)

    result, unique_id = invoke_assign_resource(cspsal_node, assign_input_str)

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=5,
    )
    logger.info(
        f"AssignResources {unique_id}  completed , starting with configure"
    )
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)

    csp_subarray.SetDefective(ERROR_PROPAGATION_DEFECT)

    result, unique_id = cspsal_node.Configure(configure_input_str)
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")

    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (
            unique_id[0],
            EXCEPTION_OCCURRED,
        ),
        lookahead=4,
    )
    csp_subarray.SetDefective(RESET_DEFECT)
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_command_timeout_mid(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_mid_device_admin_mode,
):
    return configure_resources_timeout(
        tango_context,
        CSPSUBARRAYLEAFNODE_MID,
        json_factory("cspsln_AssignResources_command"),
        json_factory("cspsln_configure_command"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_command_exception_propagation_mid(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_mid_device_admin_mode,
):
    return configure_error_propogation(
        tango_context,
        CSPSUBARRAYLEAFNODE_MID,
        json_factory("cspsln_AssignResources_command"),
        json_factory("cspsln_configure_command"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_configure_command_timeout_low(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_low_device_admin_mode,
):
    return configure_resources_timeout(
        tango_context,
        CSPSUBARRAYLEAFNODE_LOW,
        json_factory("cspsln_assignresources_command_low"),
        json_factory("cspsln_configure_command_low"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_configure_command_exception_propagation_low(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_low_device_admin_mode,
):
    return configure_error_propogation(
        tango_context,
        CSPSUBARRAYLEAFNODE_LOW,
        json_factory("cspsln_assignresources_command_low"),
        json_factory("cspsln_configure_command_low"),
        change_event_callbacks,
    )
