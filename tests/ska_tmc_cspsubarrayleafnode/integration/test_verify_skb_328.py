"""Test module to verify SKB-328"""
import ast
import json

import pytest
import tango
from pytest_bdd import given, parsers, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_telmodel.schema import validate as telmodel_validate

from src.ska_tmc_cspsubarrayleafnode.const import LOW_DELAYMODEL_VERSION
from tests.settings import (
    COMMAND_COMPLETED,
    CSPSUBARRAYLEAFNODE_LOW,
    LOW_CSP_SUBARRAY,
    LOW_DELAY_JSON,
    dev_factory,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_low
@scenario(
    "../ska_tmc_cspsubarrayleafnode/features/skb_328.feature",
    "Verify correct station/sub-station IDs in low delay "
    + "polynomials(SKB-328)",
)
def test_skb_328_verification() -> None:
    """
    Test case to verify low delay polynomials
    uses stations and substation ids from low cbf section.
    """


@given("the CspSubarrayLeafNode is in obsState IDLE")
def check_csp_subarray_leaf_node_obsState_idle(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_low_device_admin_mode,
) -> None:
    """Ensure CspSubarrayLeafNode is in obsState IDLE"""
    assign_json_str = json_factory("cspsln_assignresources_command_low")
    low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
    try:
        _, unique_id = invoke_assign_resource(low_csp_saln, assign_json_str)
        lrcr_id = low_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        wait_for_final_csp_subarray_obsstate(low_csp_saln, ObsState.IDLE)
        low_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, low_csp_subarray, low_csp_saln, change_event_callbacks
        )
        raise


@when(
    parsers.parse(
        "I execute configure on CspSubarrayLeafNode with "
        + "lowcbf {stations_substation_ids}"
    )
)
def execute_configure(
    json_factory, stations_substation_ids, change_event_callbacks
) -> None:
    """Execute Configure on CspSubarrayLeafNode"""
    try:
        low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
        low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
        configure_json_str = json_factory("cspsln_configure_command_low")
        logger.info(type(configure_json_str))
        logger.info("configure json is:%s", configure_json_str)
        configure_json = json.loads(configure_json_str)
        logger.info(type(configure_json))
        logger.info(configure_json["csp"])
        # Converting string to list
        stations_substation_ids_list = ast.literal_eval(
            stations_substation_ids
        )

        configure_json["csp"]["lowcbf"]["stations"][
            "stns"
        ] = stations_substation_ids_list

        result, unique_id = low_csp_saln.Configure(json.dumps(configure_json))
        logger.info(
            f"Configure Command ID: {unique_id} Returned result: {result}"
        )
        lrcr_id = low_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        assert unique_id[0].endswith("Configure")
        assert result[0] == ResultCode.QUEUED
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        low_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, low_csp_subarray, low_csp_saln, change_event_callbacks
        )
        raise


@then(
    parsers.parse(
        "CspSubarrayLeafNode starts generating low "
        + "delay polynomials for {stations_substation_ids} received "
        + "from lowcbf"
    )
)
def check_low_delay_polynomials_generation(
    stations_substation_ids, change_event_callbacks
) -> None:
    """
    Check low delay polynomials uses station and
    substation IDs from lowcbf
    """
    try:
        low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
        low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
        delay_json = json.loads(
            low_csp_saln.read_attribute("delayModel").value
        )
        assert delay_json != json.dumps(LOW_DELAY_JSON)
        telmodel_validate(
            version=LOW_DELAYMODEL_VERSION,
            config=delay_json,
            strictness=2,
        )

        first_delay_polynomial = delay_json["station_beam_delays"][0]
        first_station_id = first_delay_polynomial["station_id"]
        first_sub_station_id = first_delay_polynomial["substation_id"]
        received_stations_substation_ids_list = ast.literal_eval(
            stations_substation_ids
        )
        assert [
            first_station_id,
            first_sub_station_id,
        ] in received_stations_substation_ids_list
        wait_for_final_csp_subarray_obsstate(low_csp_saln, ObsState.READY)
    except Exception:
        tear_down(
            dev_factory, low_csp_subarray, low_csp_saln, change_event_callbacks
        )
        raise


@when("I execute End on CspSubarrayLeafNode")
def execute_end(change_event_callbacks) -> None:
    """Execute End on CspSubarrayLeafNode"""
    try:
        low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
        low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
        result, unique_id = low_csp_saln.End()
        lrcr_id = low_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        assert unique_id[0].endswith("End")
        assert result[0] == ResultCode.QUEUED
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        low_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, low_csp_subarray, low_csp_saln, change_event_callbacks
        )
        raise


@then("CspSubarrayLeafNode stops generating low delay polynomials")
def checks_low_delay_polynomials_generation_stop(
    change_event_callbacks,
) -> None:
    """Check delay polynomial generation is stopped"""
    try:
        low_csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
        low_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
        delay_json = json.loads(
            low_csp_saln.read_attribute("delayModel").value
        )
        assert delay_json == LOW_DELAY_JSON
        wait_for_final_csp_subarray_obsstate(low_csp_saln, ObsState.IDLE)
        tear_down(
            low_csp_saln,
            low_csp_subarray,
            low_csp_saln,
            change_event_callbacks,
        )
    except Exception:
        tear_down(
            dev_factory, low_csp_subarray, low_csp_saln, change_event_callbacks
        )
        raise
