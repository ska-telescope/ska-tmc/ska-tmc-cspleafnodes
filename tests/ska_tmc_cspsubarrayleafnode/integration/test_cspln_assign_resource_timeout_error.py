import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tmc_common.dev_factory import DevFactory

from tests.settings import (
    ERROR_PROPAGATION_DEFECT,
    EXCEPTION_OCCURRED,
    RESET_DEFECT,
    TIMEOUT_DEFECT,
    TIMEOUT_OCCURRED,
    get_csp_devices,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import tear_down

dev_factory = DevFactory()


def assign_resources_timeout(
    deployment,
    assign_input_str,
    change_event_callbacks,
) -> None:
    cspsal_node, csp_subarray = get_csp_devices(deployment)

    csp_subarray.SetDefective(TIMEOUT_DEFECT)
    result, unique_id = cspsal_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], TIMEOUT_OCCURRED),
        lookahead=12,
    )

    csp_subarray.SetDefective(RESET_DEFECT)
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


def assign_resources_error_propogation(
    deployment,
    assign_input_str,
    change_event_callbacks,
) -> None:
    cspsal_node, csp_subarray = get_csp_devices(deployment)
    csp_subarray.SetDefective(ERROR_PROPAGATION_DEFECT)

    result, unique_id = cspsal_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )

    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (
            unique_id[0],
            EXCEPTION_OCCURRED,
        ),
        lookahead=8,
    )
    csp_subarray.SetDefective(RESET_DEFECT)
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_assign_res_command_timeout_mid(
    json_factory,
    change_event_callbacks,
):
    return assign_resources_timeout(
        "mid",
        json_factory("cspsln_AssignResources_command"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_assign_res_command_error_propagation_mid(
    json_factory, change_event_callbacks, set_csp_mid_device_admin_mode
):
    return assign_resources_error_propogation(
        "mid",
        json_factory("cspsln_AssignResources_command"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assign_res_command_timeout_low(
    json_factory, change_event_callbacks, set_csp_low_device_admin_mode
):
    return assign_resources_timeout(
        "low",
        json_factory("cspsln_assignresources_command_low"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_assign_res_command_error_propagation_low(
    json_factory, change_event_callbacks, set_csp_low_device_admin_mode
):
    return assign_resources_error_propogation(
        "low",
        json_factory("cspsln_assignresources_command_low"),
        change_event_callbacks,
    )
