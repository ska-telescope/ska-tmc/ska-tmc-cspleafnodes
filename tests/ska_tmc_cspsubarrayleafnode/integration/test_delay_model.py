import json
from typing import Any, Callable

import pytest
import tango
import tango.test_context
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import AdminMode, ObsState
from ska_telmodel.schema import validate as telmodel_validate
from ska_tmc_common.dev_factory import DevFactory

from src.ska_tmc_cspsubarrayleafnode.const import (
    LOW_DELAYMODEL_VERSION,
    MID_DELAYMODEL_VERSION,
)
from tests.conftest import (
    CSPSUBARRAYLEAFNODE_LOW,
    CSPSUBARRAYLEAFNODE_MID,
    LOW_CSP_SUBARRAY,
    MID_CSP_SUBARRAY,
)
from tests.settings import COMMAND_COMPLETED
from tests.settings import LOW_DELAY_JSON as DEFAULT_LOW_DELAY_JSON
from tests.settings import MID_DELAY_JSON as DEFAULT_MID_DELAY_JSON
from tests.settings import logger, wait_and_validate_device_attribute_value
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    calculate_epoch_difference,
    generate_ska_epoch_tai_value,
    get_generated_delay_time_values,
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)

CSP_SUBARRAY_DEVICE_MID = "mid-csp/subarray/01"
TIMEOUT = 15


def delay_json_validation(
    tango_context,
    cspsln_name,
    assign_input_str,
    configure_input_str,
    change_event_callbacks,
    json_schema_factory,
):
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(cspsln_name)
    csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)

    try:
        result, unique_id = invoke_assign_resource(
            cspsal_node, assign_input_str
        )

        lrcr_id = cspsal_node.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        logger.info(
            f"AssignResources {unique_id}  completed , starting with configure"
        )

        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)
        result, unique_id = cspsal_node.Configure(configure_input_str)
        logger.info(
            f"Configure Command ID: {unique_id} Returned result: {result}"
        )
        assert unique_id[0].endswith("Configure")
        assert result[0] == ResultCode.QUEUED

        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
        assert delay_json != json.dumps(DEFAULT_LOW_DELAY_JSON)
        telmodel_validate(
            version=LOW_DELAYMODEL_VERSION,
            config=delay_json,
            strictness=2,
        )
        pst_beam_delay_json = json.loads(
            cspsal_node.read_attribute("delayModelPSTBeam1").value
        )
        assert pst_beam_delay_json != json.dumps(DEFAULT_LOW_DELAY_JSON)
        telmodel_validate(
            version=LOW_DELAYMODEL_VERSION,
            config=pst_beam_delay_json,
            strictness=2,
        )
        logger.info("The PSTDelayJson is %s", pst_beam_delay_json)
        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)
        # Invoke End command
        result, unique_id = cspsal_node.End()
        logger.info(
            f"Configure Command ID: {unique_id} Returned result: {result}"
        )
        assert unique_id[0].endswith("End")
        assert result[0] == ResultCode.QUEUED

        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
        assert delay_json == DEFAULT_LOW_DELAY_JSON
        pst_beam_delay_json = json.loads(
            cspsal_node.read_attribute("delayModelPSTBeam1").value
        )
        assert pst_beam_delay_json == DEFAULT_LOW_DELAY_JSON
        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)
        cspsal_node.unsubscribe_event(lrcr_id)
        tear_down(
            dev_factory, csp_subarray, cspsal_node, change_event_callbacks
        )
    except Exception:
        tear_down(
            dev_factory, csp_subarray, cspsal_node, change_event_callbacks
        )
        raise


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_delay_json_low(
    tango_context,
    json_factory,
    change_event_callbacks,
    json_schema_factory,
    set_csp_low_device_admin_mode,
):
    return delay_json_validation(
        tango_context,
        CSPSUBARRAYLEAFNODE_LOW,
        json_factory("cspsln_assignresources_command_low"),
        json_factory("cspsln_configure_command_low"),
        change_event_callbacks,
        json_schema_factory,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_delay_json_mid(
    json_factory, change_event_callbacks, set_csp_mid_device_admin_mode
):
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)

    csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    csp_subarray.ResetDelayInfo()
    try:
        result, unique_id = invoke_assign_resource(
            cspsal_node, json_factory("cspsln_AssignResources_command")
        )

        lrcr_id = cspsal_node.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        logger.info(
            f"AssignResources {unique_id}  completed , starting with configure"
        )

        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)
        result, unique_id = cspsal_node.Configure(
            json_factory("cspsln_configure_command")
        )
        logger.info(
            f"Configure Command ID: {unique_id} Returned result: {result}"
        )
        assert unique_id[0].endswith("Configure")
        assert result[0] == ResultCode.QUEUED

        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )

        assert wait_and_validate_device_attribute_value(
            cspsal_node,
            "delayModel",
            DEFAULT_MID_DELAY_JSON,
            negation=True,
            is_json=True,
        )
        delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
        telmodel_validate(
            version=MID_DELAYMODEL_VERSION,
            config=delay_json,
            strictness=2,
        )
        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)
        # Invoke End command
        result, unique_id = cspsal_node.End()
        logger.info(
            f"Configure Command ID: {unique_id} Returned result: {result}"
        )
        assert unique_id[0].endswith("End")
        assert result[0] == ResultCode.QUEUED

        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
        assert delay_json == DEFAULT_MID_DELAY_JSON
        telmodel_validate(
            version=MID_DELAYMODEL_VERSION,
            config=delay_json,
            strictness=1,
        )
        wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)
        cspsal_node.unsubscribe_event(lrcr_id)
        tear_down(
            dev_factory, csp_subarray, cspsal_node, change_event_callbacks
        )
    except Exception:
        tear_down(
            dev_factory, csp_subarray, cspsal_node, change_event_callbacks
        )
        raise


# Test verifies the resolution of SKB-300.
@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_delay_json_epoch_mid(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_mid_device_admin_mode,
):
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)

    csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)

    # Invoke AssignResource command
    result, unique_id = invoke_assign_resource(
        cspsal_node, json_factory("cspsln_AssignResources_command")
    )

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=5,
    )
    logger.info(
        f"AssignResources {unique_id}  completed , starting with configure"
    )

    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)

    # Invoke Configure command
    result, unique_id = cspsal_node.Configure(
        json_factory("cspsln_configure_command")
    )
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")
    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=8
    )

    # Ska Epoch value in TAI format
    ska_epoch_tai = generate_ska_epoch_tai_value()
    (
        delay_generated_time,
        delay_json_dict,
    ) = get_generated_delay_time_values(
        cspsal_node, TIMEOUT, DEFAULT_MID_DELAY_JSON
    )

    # Calculated difference between Epoch in json
    # and Epoch calculated with Timestamp
    epoch_difference = calculate_epoch_difference(
        delay_generated_time, ska_epoch_tai, delay_json_dict
    )
    # check whether difference is less than delayadvancetime
    assert epoch_difference < 30
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)

    # Invoke End command
    result, unique_id = cspsal_node.End()
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")
    assert unique_id[0].endswith("End")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=8
    )
    delay_json = json.loads(cspsal_node.read_attribute("delayModel").value)
    assert delay_json == DEFAULT_MID_DELAY_JSON
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)

    # Invoke Configure command
    result, unique_id = cspsal_node.Configure(
        json_factory("cspsln_configure_command")
    )
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")
    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=8
    )
    (
        delay_generated_time,
        delay_json_dict,
    ) = get_generated_delay_time_values(
        cspsal_node, TIMEOUT, DEFAULT_MID_DELAY_JSON
    )
    # Calculated difference between Epoch in json
    # and Epoch calculated with Timestamp
    epoch_difference = calculate_epoch_difference(
        delay_generated_time, ska_epoch_tai, delay_json_dict
    )
    # check whether difference is less than delayadvancetime
    assert epoch_difference < 30
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


# Test verifies the resolution of SKB-300.
@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_delay_json_epoch_low(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_low_device_admin_mode,
):
    logger.info(f"{tango_context}")
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)

    csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)

    # Invoke AssignResource command
    result, unique_id = invoke_assign_resource(
        cspsal_node, json_factory("cspsln_assignresources_command_low")
    )

    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=5,
    )
    logger.info(
        f"AssignResources {unique_id}  completed , starting with configure"
    )

    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)

    # Invoke Configure command
    result, unique_id = cspsal_node.Configure(
        json_factory("cspsln_configure_command_low")
    )
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")
    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=8
    )

    # Ska Epoch value in TAI format
    ska_epoch_tai = generate_ska_epoch_tai_value()
    (
        delay_generated_time,
        delay_json_dict,
    ) = get_generated_delay_time_values(
        cspsal_node, TIMEOUT, DEFAULT_LOW_DELAY_JSON
    )

    # Calculated difference between
    # Epoch in json and Epoch calculated with Timestamp
    epoch_difference = calculate_epoch_difference(
        delay_generated_time, ska_epoch_tai, delay_json_dict
    )
    # check whether difference is less than delayadvancetime
    assert epoch_difference < 600
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)

    # Invoke End command
    result, unique_id = cspsal_node.End()
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")
    assert unique_id[0].endswith("End")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=8
    )
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)

    # Invoke Configure command
    result, unique_id = cspsal_node.Configure(
        json_factory("cspsln_configure_command_low")
    )
    logger.info(f"Configure Command ID: {unique_id} Returned result: {result}")
    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=8
    )

    (
        delay_generated_time,
        delay_json_dict,
    ) = get_generated_delay_time_values(
        cspsal_node, TIMEOUT, DEFAULT_LOW_DELAY_JSON
    )
    # Calculated difference between Epoch in json
    # and Epoch calculated with Timestamp
    epoch_difference = calculate_epoch_difference(
        delay_generated_time, ska_epoch_tai, delay_json_dict
    )
    # check whether difference is less than delayadvancetime
    assert epoch_difference < 600
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


def delay_model_cadence(
    tango_context: Callable[
        [Any], tango.test_context.MultiDeviceTestContext | None
    ],
    csp_subarray_leaf_node_fqdn: str,
    csp_subarray_fqdn: str,
    assign_resources_json_str: str,
    configure_json_str: str,
    default_delay_json: dict[str, Any],
    schema_version: str,
    change_event_callbacks: Callable[[Any], None],
) -> None:
    """Function to test the delay model cadence."""
    # Setup
    dev_factory = DevFactory()
    CSP_SUBARRAY_LEAF_NODE_PROXY: tango.DeviceProxy = dev_factory.get_device(
        csp_subarray_leaf_node_fqdn
    )
    CSP_SUBARRAY_LEAF_NODE_PROXY.adminMode = AdminMode.ONLINE
    CSP_SUBARRAY_PROXY: tango.DeviceProxy = dev_factory.get_device(
        csp_subarray_fqdn
    )
    CSP_SUBARRAY_PROXY.adminMode = AdminMode.ONLINE
    CSPSLN_LRCR_ID: int = CSP_SUBARRAY_LEAF_NODE_PROXY.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )

    # Assign Resources
    RESULT_CODE, UNIQUE_ID = CSP_SUBARRAY_LEAF_NODE_PROXY.AssignResources(
        assign_resources_json_str
    )
    assert RESULT_CODE == ResultCode.QUEUED
    assert UNIQUE_ID[0].endswith("AssignResources")

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (UNIQUE_ID[0], COMMAND_COMPLETED), lookahead=2
    )
    assert CSP_SUBARRAY_PROXY.obsState == ObsState.IDLE

    # Configure
    RESULT_CODE, UNIQUE_ID = CSP_SUBARRAY_LEAF_NODE_PROXY.Configure(
        configure_json_str
    )
    assert RESULT_CODE == ResultCode.QUEUED
    assert UNIQUE_ID[0].endswith("Configure")

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (UNIQUE_ID[0], COMMAND_COMPLETED), lookahead=2
    )
    assert CSP_SUBARRAY_PROXY.obsState == ObsState.READY

    # Validation for the first Configure
    delay_json = json.loads(CSP_SUBARRAY_LEAF_NODE_PROXY.delayModel)
    logger.info(f"Delay JSON after first Configure: {delay_json}")
    assert delay_json != json.dumps(default_delay_json)
    telmodel_validate(
        version=schema_version,
        config=delay_json,
        strictness=2,
    )
    start_validity_sec = delay_json["start_validity_sec"]

    # End
    RESULT_CODE, UNIQUE_ID = CSP_SUBARRAY_LEAF_NODE_PROXY.End()
    assert RESULT_CODE == ResultCode.QUEUED
    assert UNIQUE_ID[0].endswith("End")

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (UNIQUE_ID[0], COMMAND_COMPLETED), lookahead=2
    )
    assert CSP_SUBARRAY_PROXY.obsState == ObsState.IDLE

    # Configure 2
    RESULT_CODE, UNIQUE_ID = CSP_SUBARRAY_LEAF_NODE_PROXY.Configure(
        configure_json_str
    )
    assert RESULT_CODE == ResultCode.QUEUED
    assert UNIQUE_ID[0].endswith("Configure")

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (UNIQUE_ID[0], COMMAND_COMPLETED), lookahead=2
    )
    assert CSP_SUBARRAY_PROXY.obsState == ObsState.READY

    # Validation for the second Configure
    delay_json = json.loads(CSP_SUBARRAY_LEAF_NODE_PROXY.delayModel)
    logger.info("Delay JSON after second Configure: %s", delay_json)
    assert delay_json != json.dumps(default_delay_json)
    telmodel_validate(
        version=schema_version,
        config=delay_json,
        strictness=2,
    )
    logger.info(
        "The delay cadence is %s seconds. Validating the difference between "
        + "delay jsons to be not equal to cadence.",
        delay_json["cadence_sec"],
    )
    assert (
        round(delay_json["start_validity_sec"] - start_validity_sec)
        != delay_json["cadence_sec"]
    )

    # Cleanup
    CSP_SUBARRAY_LEAF_NODE_PROXY.unsubscribe_event(CSPSLN_LRCR_ID)
    tear_down(
        dev_factory,
        CSP_SUBARRAY_PROXY,
        CSP_SUBARRAY_LEAF_NODE_PROXY,
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.parametrize(
    "csp_subarray_leaf_node_fqdn, csp_subarray_fqdn, "
    + "assign_resources_json_file_name, configure_json_file_name, "
    + "default_delay_json, schema_version,",
    [
        pytest.param(
            CSPSUBARRAYLEAFNODE_LOW,
            LOW_CSP_SUBARRAY,
            "cspsln_assignresources_command_low",
            "cspsln_configure_command_low",
            DEFAULT_LOW_DELAY_JSON,
            LOW_DELAYMODEL_VERSION,
            marks=[pytest.mark.SKA_low],
        ),
        pytest.param(
            CSPSUBARRAYLEAFNODE_MID,
            MID_CSP_SUBARRAY,
            "cspsln_AssignResources_command",
            "cspsln_configure_command",
            DEFAULT_MID_DELAY_JSON,
            MID_DELAYMODEL_VERSION,
            marks=[pytest.mark.SKA_mid],
        ),
    ],
)
def test_delay_model_cadence(
    tango_context: Callable[
        [Any], tango.test_context.MultiDeviceTestContext | None
    ],
    json_factory: Callable[[str], str],
    csp_subarray_leaf_node_fqdn: str,
    csp_subarray_fqdn: str,
    assign_resources_json_file_name: str,
    configure_json_file_name: str,
    default_delay_json: dict[str, Any],
    schema_version: str,
    change_event_callbacks: Callable[[Any], None],
) -> None:
    """Test the cadence of delay model calculations for multiple scans."""
    return delay_model_cadence(
        tango_context,
        csp_subarray_leaf_node_fqdn,
        csp_subarray_fqdn,
        json_factory(assign_resources_json_file_name),
        json_factory(configure_json_file_name),
        default_delay_json,
        schema_version,
        change_event_callbacks,
    )
