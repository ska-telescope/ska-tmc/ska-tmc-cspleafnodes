import json

import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common.dev_factory import DevFactory

from tests.conftest import (
    CSPSUBARRAYLEAFNODE_LOW,
    CSPSUBARRAYLEAFNODE_MID,
    LOW_CSP_SUBARRAY,
    MID_CSP_SUBARRAY,
)
from tests.settings import COMMAND_COMPLETED
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)


def invoke_scan_endscan_command(
    tango_context,
    cspsln_name,
    assign_input_str,
    configure_input_str,
    scan_input_str,
    change_event_callbacks,
):
    dev_factory = DevFactory()
    cspsal_node = dev_factory.get_device(cspsln_name)
    if cspsln_name == CSPSUBARRAYLEAFNODE_MID:
        csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    else:
        csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)

    result, unique_id = cspsal_node.AssignResources(assign_input_str)
    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.IDLE)
    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED),
        lookahead=2,
    )
    result, unique_id = cspsal_node.Configure(configure_input_str)

    assert unique_id[0].endswith("Configure")
    assert result[0] == ResultCode.QUEUED
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=2
    )
    wait_for_final_csp_subarray_obsstate(cspsal_node, ObsState.READY)
    csp_subarray.SetDelayInfo(json.dumps({"Scan": 10}))
    result, unique_id = cspsal_node.Scan(scan_input_str)

    assert unique_id[0].endswith("Scan")
    assert result[0] == ResultCode.QUEUED
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=2
    )
    result, unique_id = cspsal_node.EndScan()

    assert unique_id[0].endswith("EndScan")
    assert result[0] == ResultCode.QUEUED
    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id[0], COMMAND_COMPLETED), lookahead=4
    )
    csp_subarray.ResetDelayInfo()
    cspsal_node.unsubscribe_event(lrcr_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_scan_endscan_command(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_mid_device_admin_mode,
):
    return invoke_scan_endscan_command(
        tango_context,
        CSPSUBARRAYLEAFNODE_MID,
        json_factory("cspsln_AssignResources_command"),
        json_factory("cspsln_configure_command"),
        json_factory("cspsln_scan_command"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_scan_endscan_command_lowcsp(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_low_device_admin_mode,
):
    return invoke_scan_endscan_command(
        tango_context,
        CSPSUBARRAYLEAFNODE_LOW,
        json_factory("cspsln_assignresources_command_low"),
        json_factory("cspsln_configure_command_low"),
        json_factory("cspsln_scan_command_low"),
        change_event_callbacks,
    )
