import pytest
import tango
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common.dev_factory import DevFactory

from tests.conftest import (
    CSPSUBARRAYLEAFNODE_LOW,
    CSPSUBARRAYLEAFNODE_MID,
    LOW_CSP_SUBARRAY,
    MID_CSP_SUBARRAY,
)
from tests.settings import RESET_DEFECT
from tests.ska_tmc_cspsubarrayleafnode.integration.common import tear_down


def abort(
    tango_context,
    cspsal_node,
    csp_subarray,
    assign_input_str,
    change_event_callbacks,
) -> None:
    dev_factory = DevFactory()
    csp_subarray.SetDefective(RESET_DEFECT)
    result, unique_id = cspsal_node.AssignResources(assign_input_str)
    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED
    obsstate_id = cspsal_node.subscribe_event(
        "cspSubarrayObsState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["cspSubarrayObsState"],
    )
    change_event_callbacks["cspSubarrayObsState"].assert_change_event(
        ObsState.RESOURCING,
        lookahead=4,
    )
    cspsal_node.unsubscribe_event(obsstate_id)
    result, unique_id = cspsal_node.Abort()
    obsstate_id = cspsal_node.subscribe_event(
        "cspSubarrayObsState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["cspSubarrayObsState"],
    )
    change_event_callbacks["cspSubarrayObsState"].assert_change_event(
        ObsState.ABORTED,
        lookahead=8,
    )
    cspsal_node.unsubscribe_event(obsstate_id)
    tear_down(dev_factory, csp_subarray, cspsal_node, change_event_callbacks)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_abort_command_mid(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_mid_device_admin_mode,
):
    dev_factory = DevFactory()
    return abort(
        tango_context,
        dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID),
        dev_factory.get_device(MID_CSP_SUBARRAY),
        json_factory("cspsln_AssignResources_command"),
        change_event_callbacks,
    )


@pytest.mark.post_deployment
@pytest.mark.SKA_low
def test_abort_command_low(
    tango_context,
    json_factory,
    change_event_callbacks,
    set_csp_low_device_admin_mode,
):
    dev_factory = DevFactory()
    return abort(
        tango_context,
        dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW),
        dev_factory.get_device(LOW_CSP_SUBARRAY),
        json_factory("cspsln_assignresources_command_low"),
        change_event_callbacks,
    )
