"""Test module to verify SKB-413"""
import ast
import json

import pytest
import tango
from pytest_bdd import given, parsers, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_telmodel.schema import validate as telmodel_validate

from src.ska_tmc_cspsubarrayleafnode.const import MID_DELAYMODEL_VERSION
from tests.settings import (
    COMMAND_COMPLETED,
    CSPSUBARRAYLEAFNODE_MID,
    MID_CSP_SUBARRAY,
    MID_DELAY_JSON,
    dev_factory,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
@scenario(
    "../ska_tmc_cspsubarrayleafnode/features/skb_413.feature",
    "Verify SKB-413 with TMC CspSubarrayLeafNode as entrypoint",
)
def test_skb_413_verification() -> None:
    """
    Test case to verify skb-413
    Mid Delay Model points to correct assigned Dishes
    """


@given(
    parsers.parse(
        "the CspSubarrayLeafNode is in obsState IDLE "
        + "with receptors {receptors}"
    )
)
def move_csp_subarray_leaf_node_to_idle(
    tango_context,
    json_factory,
    change_event_callbacks,
    receptors,
    set_csp_mid_device_admin_mode,
) -> None:
    """Ensure CspSubarrayLeafNode is in obsState IDLE"""
    assign_json_str = json_factory("cspsln_AssignResources_command")
    assign_json = json.loads(assign_json_str)
    # Converting string to list
    receptors_ids_list = ast.literal_eval(receptors)
    assign_json["dish"]["receptor_ids"] = receptors_ids_list
    mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
    try:
        _, unique_id = invoke_assign_resource(
            mid_csp_saln, json.dumps(assign_json)
        )
        lrcr_id = mid_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        wait_for_final_csp_subarray_obsstate(mid_csp_saln, ObsState.IDLE)
        mid_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise


@when("I execute configure on CspSubarrayLeafNode")
def execute_configure(json_factory, change_event_callbacks) -> None:
    """Execute Configure on CspSubarrayLeafNode"""
    try:
        mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
        mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
        configure_json_str = json_factory("cspsln_configure_command")
        result, unique_id = mid_csp_saln.Configure(configure_json_str)
        logger.info(
            f"Configure Command ID: {unique_id} Returned result: {result}"
        )
        lrcr_id = mid_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        assert unique_id[0].endswith("Configure")
        assert result[0] == ResultCode.QUEUED
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        mid_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise


@then(
    parsers.parse(
        "CspSubarrayLeafNode starts generating delay "
        + "polynomials for correct {receptors}"
    )
)
def check_delay_model_points_to_correct_receptors(
    receptors, change_event_callbacks
) -> None:
    """
    Check mid delay model points to correct
    assigned receptors
    """
    try:
        mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
        mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
        delay_json = json.loads(
            mid_csp_saln.read_attribute("delayModel").value
        )
        assert delay_json != json.dumps(MID_DELAY_JSON)
        telmodel_validate(
            version=MID_DELAYMODEL_VERSION,
            config=delay_json,
            strictness=2,
        )
        receptors_ids_list = ast.literal_eval(receptors)
        for receptor_delay in delay_json["receptor_delays"]:
            assert receptor_delay["receptor"] in receptors_ids_list
        wait_for_final_csp_subarray_obsstate(mid_csp_saln, ObsState.READY)
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise
