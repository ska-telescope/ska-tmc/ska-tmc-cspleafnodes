"""Test module to verify SKB-329"""
import json

import pytest
import tango
from pytest_bdd import given, scenario, then, when
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_telmodel.schema import validate as telmodel_validate

from src.ska_tmc_cspsubarrayleafnode.const import MID_DELAYMODEL_VERSION
from tests.settings import (
    COMMAND_COMPLETED,
    CSPSUBARRAYLEAFNODE_MID,
    MID_CSP_SUBARRAY,
    MID_DELAY_JSON,
    dev_factory,
    logger,
)
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    invoke_assign_resource,
    tear_down,
    wait_for_final_csp_subarray_obsstate,
)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
@scenario(
    "../ska_tmc_cspsubarrayleafnode/features/skb_329.feature",
    "Verify SKB-329",
)
def test_verify_skb_329() -> None:
    """Test case to verify SKB-329."""


@given("the CspSubarrayLeafNode is in obsState IDLE")
def check_csp_subarray_leaf_node_obsState_idle(
    json_factory,
    change_event_callbacks,
) -> None:
    assign_json_str = json_factory("cspsln_AssignResources_command")
    mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)

    try:
        _, unique_id = invoke_assign_resource(mid_csp_saln, assign_json_str)
        lrcr_id = mid_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        wait_for_final_csp_subarray_obsstate(mid_csp_saln, ObsState.IDLE)
        mid_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise


@given("I configure the TMC CspSubarrayLeafNode")
def execute_configure(json_factory, change_event_callbacks) -> None:
    """Execute Configure on CspSubarrayLeafNode"""
    try:
        mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
        mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
        configure_json_str = json_factory("cspsln_configure_command")
        logger.info(type(configure_json_str))
        logger.info("configure json is:%s", configure_json_str)

        result, unique_id = mid_csp_saln.Configure(configure_json_str)
        logger.info(
            f"Configure Command ID: {unique_id} Returned result: {result}"
        )
        lrcr_id = mid_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        assert unique_id[0].endswith("Configure")
        assert result[0] == ResultCode.QUEUED
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        mid_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise


@given("CspSubarrayLeafNode immediately starts generating delay polynomials")
def check_mid_delay_polynomials_generation(change_event_callbacks) -> None:
    """Check delay polynomial generation is started"""
    try:
        mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
        mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
        delay_json = json.loads(
            mid_csp_saln.read_attribute("delayModel").value
        )
        assert delay_json != json.dumps(MID_DELAY_JSON)
        telmodel_validate(
            version=MID_DELAYMODEL_VERSION,
            config=delay_json,
            strictness=2,
        )

        wait_for_final_csp_subarray_obsstate(mid_csp_saln, ObsState.READY)

    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise


@when("I end the observation")
def execute_end(change_event_callbacks) -> None:
    """Execute End on CspSubarrayLeafNode"""
    try:
        mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
        mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
        result, unique_id = mid_csp_saln.End()
        lrcr_id = mid_csp_saln.subscribe_event(
            "longRunningCommandResult",
            tango.EventType.CHANGE_EVENT,
            change_event_callbacks["longRunningCommandResult"],
        )
        assert unique_id[0].endswith("End")
        assert result[0] == ResultCode.QUEUED
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        mid_csp_saln.unsubscribe_event(lrcr_id)
    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise


@then("CspSubarrayLeafNode Node stops generating delay values without waiting")
def checks_mid_delay_polynomials_generation_stop(
    change_event_callbacks,
) -> None:
    """Check delay polynomial generation is stopped"""
    try:
        mid_csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
        mid_csp_saln = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
        delay_json = json.loads(
            mid_csp_saln.read_attribute("delayModel").value
        )
        assert delay_json == MID_DELAY_JSON
        telmodel_validate(
            version=MID_DELAYMODEL_VERSION,
            config=delay_json,
            strictness=1,
        )

        wait_for_final_csp_subarray_obsstate(mid_csp_saln, ObsState.IDLE)
        tear_down(
            dev_factory,
            mid_csp_subarray,
            mid_csp_saln,
            change_event_callbacks,
        )
    except Exception:
        tear_down(
            dev_factory, mid_csp_subarray, mid_csp_saln, change_event_callbacks
        )
        raise
