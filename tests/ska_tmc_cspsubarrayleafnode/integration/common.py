import json
import time
from datetime import datetime
from typing import Any, Callable, Tuple

import pytest
import tango
from astropy.time import Time
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common import DevFactory, HelperSubArrayDevice

from ska_tmc_cspsubarrayleafnode.csp_subarray_leaf_node import (
    TmcLeafNodeCspSubarray,
)
from tests.settings import COMMAND_COMPLETED, TIMEOUT, logger

pytest.event_arrived = False


@pytest.fixture()
def devices_to_load() -> Tuple[dict[str, Any], dict[str, Any]]:
    return (
        {
            "class": HelperSubArrayDevice,
            "devices": [
                {"name": "low-csp/subarray/01"},
                {"name": "mid-csp/subarray/01"},
            ],
        },
        {
            "class": TmcLeafNodeCspSubarray,
            "devices": [
                {
                    "name": "mid-tmc/subarray-leaf-node-csp/01",
                    "properties": {
                        "TelmodelSource": "gitlab://gitlab.com/ska-telescope/ska-telmodel-data?main#tmdata",  # noqa: E501
                        "TelmodelPath": "instrument/ska1_mid/layout/mid-layout.json",  # noqa: E501
                        "CspSubarrayFQDN": ["mid-csp/subarray/01"],
                    },
                },
                {
                    "name": "low-tmc/subarray-leaf-node-csp/01",
                    "properties": {
                        "TelmodelSource": "gitlab://gitlab.com/ska-telescope/ska-telmodel-data?main#tmdata",  # noqa: E501
                        "TelmodelPath": "instrument/ska1_low/layout/low-layout.json",  # noqa: E501
                        "CspSubarrayFQDN": ["low-csp/subarray/01"],
                    },
                },
            ],
        },
    )


def checked_devices(json_model: dict) -> int:
    result = 0
    for dev in json_model["devices"]:
        if int(dev["ping"]) > 0 and dev["unresponsive"] == "False":
            result += 1
    return result


def tear_down(
    dev_factory: DevFactory,
    csp_subarray: tango.DeviceProxy,
    cspsal_node: tango.DeviceProxy,
    change_event_callbacks: Callable[[Any], None],
) -> None:
    lrcr_id = cspsal_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )
    obs_id = cspsal_node.subscribe_event(
        "cspSubarrayObsState",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["cspSubarrayObsState"],
    )
    csp_subarray_obsstate = cspsal_node.read_attribute("cspSubarrayObsState")
    logger.info(
        f"CSP Subarray Current ObsState: {csp_subarray_obsstate.value}"
    )

    if csp_subarray_obsstate.value == ObsState.IDLE:
        logger.info("Invoking ReleaseAllResources() on %s.", cspsal_node)
        result, unique_id = cspsal_node.ReleaseAllResources()
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        change_event_callbacks["cspSubarrayObsState"].assert_change_event(
            ObsState.EMPTY,
            lookahead=8,
        )
        csp_subarray_obsstate = csp_subarray.read_attribute("obsState")
        logger.info(
            "Tear down: After ReleaseAllResources, CSP Subarray ObsState: "
            "{}".format(csp_subarray_obsstate.value)
        )
    elif csp_subarray_obsstate.value == ObsState.READY:
        logger.info("Invoking End() on %s.", cspsal_node)
        cspsal_node.End()
        change_event_callbacks["cspSubarrayObsState"].assert_change_event(
            ObsState.IDLE,
            lookahead=8,
        )
        logger.info("Invoking ReleaseAllResources on %s.", cspsal_node)
        result, unique_id = cspsal_node.ReleaseAllResources()
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED), lookahead=8
        )
        change_event_callbacks["cspSubarrayObsState"].assert_change_event(
            ObsState.EMPTY,
            lookahead=8,
        )
        csp_subarray_obsstate = csp_subarray.read_attribute("obsState")
        logger.info(
            "Tear down: After ReleaseAllResources, CSP Subarray ObsState: "
            "{}".format(csp_subarray_obsstate.value)
        )
    elif csp_subarray_obsstate.value == ObsState.ABORTED:
        logger.info("Invoking Restart() on %s.", cspsal_node)
        cspsal_node.Restart()
        change_event_callbacks["cspSubarrayObsState"].assert_change_event(
            ObsState.EMPTY,
            lookahead=8,
        )
        csp_subarray_obsstate = csp_subarray.read_attribute("obsState")
        logger.info(
            "Tear down: After Restart, CSP Subarray ObsState:"
            "{}".format(csp_subarray_obsstate.value)
        )

    elif csp_subarray_obsstate.value in (
        ObsState.RESOURCING,
        ObsState.CONFIGURING,
        ObsState.SCANNING,
    ):
        logger.info("Invoking Abort() on %s", cspsal_node)
        result, unique_id = cspsal_node.Abort()
        change_event_callbacks["cspSubarrayObsState"].assert_change_event(
            ObsState.ABORTED,
            lookahead=8,
        )
        result_code, unique_id = cspsal_node.Restart()
        change_event_callbacks["longRunningCommandResult"].assert_change_event(
            (unique_id[0], COMMAND_COMPLETED),
            lookahead=5,
        )
        change_event_callbacks["cspSubarrayObsState"].assert_change_event(
            ObsState.RESTARTING,
            lookahead=8,
        )
        change_event_callbacks["cspSubarrayObsState"].assert_change_event(
            ObsState.EMPTY,
            lookahead=10,
        )
        csp_subarray_obsstate = csp_subarray.read_attribute("obsState")
        logger.info(
            "Tear down: After Restart, CSP Subarray ObsState:"
            "{}".format(csp_subarray_obsstate.value)
        )
    # clear the commandCallInfo on CSP Subarray
    cspsal_node.unsubscribe_event(lrcr_id)
    cspsal_node.unsubscribe_event(obs_id)
    csp_subarray.ClearCommandCallInfo()


def wait_for_final_csp_subarray_obsstate(csp_subarray_leaf_node, obs_state):
    logger.debug(f"Waiting for CspSubarray obsState to be {obs_state}")
    csp_subarray_obsstate = csp_subarray_leaf_node.read_attribute(
        "cspSubarrayObsState"
    )
    logger.debug(f"Csp Subarray obsState is {csp_subarray_obsstate}")
    wait_time = 0
    while (csp_subarray_obsstate.value) != obs_state:
        time.sleep(0.5)
        csp_subarray_obsstate = csp_subarray_leaf_node.read_attribute(
            "cspSubarrayObsState"
        )
        logger.debug(
            f"Csp Subarray obsState in loop: {csp_subarray_obsstate.value}"
        )
        logger.debug(f"Expected Csp Subarray obsState: {obs_state}")
        wait_time = wait_time + 1
        logger.debug(f"wait_time in teardown  {wait_time}")
        if wait_time > TIMEOUT:
            pytest.fail(
                f"Timeout occurred in transitioning Csp Subarray\
                     obsState to {obs_state}"
            )


def set_csp_subarray_obsstate(dev_factory, obs_state, csp_subarray):
    logger.debug("Setting Obsstate to : %s", obs_state)
    csp_subarray.SetDirectObsState(obs_state)
    time.sleep(0.1)
    logger.debug("ObsState of csp subarray: %s", csp_subarray.ObsState)


def invoke_assign_resource(cspsal_node, assign_input_str):
    """Invoke assign resource command and wait for complete"""
    result, unique_id = cspsal_node.AssignResources(assign_input_str)
    logger.info(
        f"AssignResources Command ID: {unique_id} Returned result: {result}"
    )
    assert unique_id[0].endswith("AssignResources")
    assert result[0] == ResultCode.QUEUED

    return result, unique_id


def generate_ska_epoch_tai_value() -> Time:
    """
    Generating SKA Epoch in TAI format

    :return: ska_epoch_tai
    :rtype: Time
    """
    SKA_EPOCH = "1999-12-31T23:59:28Z"
    ska_epoch_utc = Time(SKA_EPOCH, scale="utc")
    ska_epoch_tai = ska_epoch_utc.unix_tai
    return ska_epoch_tai


def calculate_epoch_difference(
    delay_generated_time: Time, ska_epoch_tai: Time, delay_json_dict: dict
) -> int:
    """
    Calculate Difference between epoch in delay model json and
    epoch calculated through delay generated timestamp

    :return: epoch_difference
    :rtype: int
    """
    delay_generated_time_utc = Time(delay_generated_time, scale="utc")
    delay_generated_time_tai = delay_generated_time_utc.unix_tai
    epoch = delay_generated_time_tai - ska_epoch_tai
    epoch_in_json = delay_json_dict["start_validity_sec"]
    epoch_difference = epoch_in_json - epoch
    return epoch_difference


def get_generated_delay_time_values(
    cspsal_node: str, timeout: int, json_value: dict
) -> Tuple[Time, dict]:
    """
    Get values from delayModel attribute.

    :return: delay_generated_time
    :rtype: Time
    :return: delay_json_dict
    :rtype: dict
    """
    elapsed_time = 0
    start_time = time.time()
    delay_generated_time = None
    while (
        cspsal_node.delayModel == json.dumps(json_value)
        or elapsed_time <= timeout
    ):
        elapsed_time = time.time() - start_time
        time.sleep(0.1)
    delay_generated_time = datetime.now()
    delay_json = cspsal_node.read_attribute("delayModel").value
    delay_json_dict = json.loads(delay_json)
    return delay_generated_time, delay_json_dict


def get_multiple_generated_delay_time_values(
    cspsal_node: str, timeout: int, json_value: dict, count: int
) -> Tuple[list, float]:
    """
    Get five consecutive start validity and delay time values
    for five iterations of delay calculation at the fequency of delay
    cadence.

    :param cspsal_node: TANGO DeviceProxy of CSP Subarray Leaf Node device
    :type cspsal_node: TANGO DeviceProxy
    :param timeout: Timeout to start the delay generation
    :type timeout: int
    :param json_value: Example delay JSON
    :type json_value: JSON string
    :param count: Number of delay values to be stored in the list
    :type count: int
    :return: start_validity_time_list
    :rtype: list
    :return: delay_cadence
    :rtype: float
    """
    elapsed_time = 0
    start_time = time.time()
    delay_generated_time = None

    start_validity_time_list = []

    while (
        cspsal_node.delayModel == json.dumps(json_value)
        or elapsed_time <= timeout
    ):
        elapsed_time = time.time() - start_time
        time.sleep(0.1)

    delay_json_dict = json.loads(
        cspsal_node.read_attribute("delayModel").value
    )
    delay_cadence = delay_json_dict["cadence_sec"]
    logger.info("Delay cadence: %s", delay_cadence)

    # Get the 5 delay JSON versus time values
    for iteration in range(count):
        delay_generated_time = datetime.now()
        delay_json = cspsal_node.read_attribute("delayModel").value
        delay_json_dict = json.loads(delay_json)
        logger.info(
            "iteration: %s ---- start validity: %s ---- now: %s",
            iteration,
            delay_json_dict["start_validity_sec"],
            delay_generated_time,
        )
        start_validity_time_list.append(delay_json_dict["start_validity_sec"])
        time.sleep(delay_cadence)
    return start_validity_time_list, delay_cadence


def check_differences_between_delay_times(
    timestamp_list: list, delay_cadence: float
) -> bool:
    """
    Check the differences between the timestamps in the input list. The
    difference between the consecutive timestamps should be equal to the
    delay cadence

    :param timestamp_list: List containing the timestamps in seconds
    :type timestamp_list: list
    :param delay_cadence: Cadence at which delays are generated
    :type timeout: float
    :rtype: bool
    """
    logger.info("timestamp list is: %s", timestamp_list)
    logger.info("delay cadence is: %s", delay_cadence)
    for index in range(len(timestamp_list) - 1):
        if (
            timestamp_list[index + 1] - timestamp_list[index]
        ) != delay_cadence:
            return False
    return True
