import time

import pytest
from ska_tmc_common.device_info import SubArrayDeviceInfo

from ska_tmc_cspsubarrayleafnode.model.input import (
    InputParameterLow,
    InputParameterMid,
)
from tests.settings import create_cm_cspsln, logger


@pytest.mark.cspsln
def test_cspsa_working(tango_context):
    start_time = time.time()
    cm, _ = create_cm_cspsln(input_parameter=InputParameterMid(None))
    dev_info = cm.get_device()
    # assert dev_info.unresponsive is False
    elapsed_time = time.time() - start_time
    logger.info(f"checked {dev_info.dev_name} device in {elapsed_time}")
    assert isinstance(dev_info, SubArrayDeviceInfo)


@pytest.mark.cspsln
def test_cspsa_faulty(tango_context):
    cm, _ = create_cm_cspsln(input_parameter=InputParameterMid(None))
    dev_info = cm.get_device()
    dev_info.update_unresponsive(True)
    assert dev_info.unresponsive


@pytest.mark.skip(reason="Wrong device name syntax (domain/family/member)")
@pytest.mark.cspsln
def test_cspsa_working_low(tango_context):
    start_time = time.time()
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    dev_info = cm.get_device()
    assert dev_info.unresponsive is False
    elapsed_time = time.time() - start_time
    logger.info(f"checked {dev_info.dev_name} device in {elapsed_time}")
    assert isinstance(dev_info, SubArrayDeviceInfo)


@pytest.mark.cspsln
def test_cspsa_faulty_low(tango_context):
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    dev_info = cm.get_device()
    dev_info.update_unresponsive(True)
    assert dev_info.unresponsive
