@XTP-5993
Feature: CspSubarrayLeafNode acceptance

	#Test the ability to generically run a a set of commands and that the execution is completed withing 5 seconds.
	@XTP-5994  @post_deployment @acceptance @SKA_mid @SKA_low
	Scenario: Ability to run commands on CspSubarrayLeafNode
		Given a CspSubarrayLeafNode device
		When I call the command <command_name>
		Then the <command_name> command is executed successfully on Csp Subarray

		Examples:
		| command_name		   |
		| AssignResources      |
		| Configure            |
		| Scan                 |
		| EndScan              |
		| End                  | 
		| ReleaseAllResources  |


	#Check CspSubarrayLeafNode node correctly report failed and working devices defined within its scope of monitoring (internal model)
	@XTP-5995 @post_deployment @acceptance @SKA_mid @SKA_low
	Scenario Outline: Monitor CspSubarrayLeafNode sub-devices
		Given a TANGO ecosystem with a set of devices deployed
