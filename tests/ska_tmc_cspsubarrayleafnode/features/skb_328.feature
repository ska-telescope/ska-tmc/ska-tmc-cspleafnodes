@XTP-28348 @XTP-43414 @Team_SAHYADRI @post_deployment @SKA_low
Scenario: Verify correct station/sub-station IDs in low delay polynomials(SKB-328)
	Given the CspSubarrayLeafNode is in obsState IDLE
	When I execute configure on CspSubarrayLeafNode with lowcbf <stations_substation_ids> 
	Then CspSubarrayLeafNode starts generating low delay polynomials for <stations_substation_ids> received from lowcbf
	When I execute End on CspSubarrayLeafNode
	Then CspSubarrayLeafNode stops generating low delay polynomials
	Examples:
		| stations_substation_ids   |
		| [[1,1],[2,1],[3,1],[4,1]] |