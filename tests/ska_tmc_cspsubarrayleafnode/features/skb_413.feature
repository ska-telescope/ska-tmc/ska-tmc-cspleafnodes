@XTP-28347 @XTP-58507 @Team_SAHYADRI @post_deployment @SKA_mid
Scenario: Verify SKB-413 with TMC CspSubarrayLeafNode as entrypoint
	Given the CspSubarrayLeafNode is in obsState IDLE with receptors <receptors>
	When I execute configure on CspSubarrayLeafNode
	Then CspSubarrayLeafNode starts generating delay polynomials for correct <receptors>
	Examples:
		| receptors            |
		| ['SKA001', 'SKA002'] |