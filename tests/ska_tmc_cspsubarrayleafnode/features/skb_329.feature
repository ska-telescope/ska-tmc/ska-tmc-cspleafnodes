@XTP-28348 @XTP-45165 @Team_SAHYADRI @post_deployment @SKA_mid
Scenario: Verify SKB-329
    Given the CspSubarrayLeafNode is in obsState IDLE
    And I configure the TMC CspSubarrayLeafNode
    And CspSubarrayLeafNode immediately starts generating delay polynomials
    When I end the observation
    Then CspSubarrayLeafNode Node stops generating delay values without waiting
