# This BDD test verifies the admin mode functionality
Scenario Outline: Test admin mode functionality csp subarray
    Given the CSP Subarray is in adminMode ONLINE set by leafnode
    And CSP Subarray is in IDLE obsState after AssignResources command
    When adminMode of CSP Subarray is set to OFFLINE by leafnode
    Then CSP Subarray denies invocation of releaseresources command
    And CSP Subarray remains in IDLE obsState