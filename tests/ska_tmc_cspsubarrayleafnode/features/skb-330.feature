# This BDD test verifies the SKB-330
@XTP-28347 @XTP-43544 @post_deployment @SKA_mid @SKA_low
Scenario Outline: Fix bug SKB-330 in TMC
    Given the CSP Subarray Leaf Node is in ON state
    And CSP Subarray Leaf Node in obsState IDLE
    When Configure command is successfully executed on the CSP subarray Leaf Node
    Then CSP subarray leaf node starts generating delay values
    And in each delay iteration the start validity time should be incremented by delay cadence