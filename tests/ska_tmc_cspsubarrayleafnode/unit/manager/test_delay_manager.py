"""Tests for the DelayManager class."""

import json

import pytest

from tests.settings import get_delay_manager, wait_for_condition


def calculate_delay_model() -> None:
    """Dummy for calculating the delay model."""


@pytest.mark.parametrize(
    "telescope",
    ["low", "mid"],
)
def test_delay_model_start_stop_method(
    tango_context,
    telescope: str,
    telmodel_source: str,
    telmodel_path_low: str,
    telmodel_path_mid: str,
):
    """Test the start method of the DelayManager class."""
    delay_manager = get_delay_manager(
        telescope, telmodel_source, telmodel_path_low, telmodel_path_mid
    )
    delay_manager.calculate_delay_model = calculate_delay_model
    delay_manager.start()
    wait_for_condition(
        delay_manager,
        condition="is_alive",
        attribute="delay_model_calculator_thread",
    )
    delay_manager.stop()
    wait_for_condition(
        delay_manager,
        attribute="delay_model_calculator_thread",
        expected_value=None,
    )


@pytest.mark.parametrize(
    "telescope",
    ["low", "mid"],
)
def test_delay_model_cadence(
    tango_context,
    telescope: str,
    telmodel_source: str,
    telmodel_path_low: str,
    telmodel_path_mid: str,
) -> None:
    """Test the cadence of delay model calculations."""
    delay_manager = get_delay_manager(
        telescope, telmodel_source, telmodel_path_low, telmodel_path_mid
    )
    cm = delay_manager.csp_sln_cm
    delay_manager.calculate_delay_model = calculate_delay_model
    delay_manager.delay_model_json = {"delay": 0}
    delay_manager.start()
    wait_for_condition(
        cm,
        attribute="delay_model",
        expected_value=json.dumps({"delay": 0}),
        interval=1,
    )
    delay_manager.delay_model_json = {"delay": 1}
    try:
        wait_for_condition(
            cm,
            attribute="delay_model",
            expected_value=json.dumps({"delay": 0}),
            timeout=4,
            interval=1,
            equal_to=False,
        )
        pytest.fail("Delay Model updated before the expected time")
    except TimeoutError:
        pass
    wait_for_condition(
        cm,
        attribute="delay_model",
        expected_value=json.dumps({"delay": 1}),
        interval=1,
    )
