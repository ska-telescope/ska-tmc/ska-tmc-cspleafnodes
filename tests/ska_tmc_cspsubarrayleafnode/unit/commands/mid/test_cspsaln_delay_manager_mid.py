from datetime import datetime, timedelta

import katpoint
import numpy as np
import pytest
from ska_tango_base.commands import ResultCode

from ska_tmc_cspsubarrayleafnode.delay_model_mid import DelayManagerMid
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterMid
from tests.settings import (
    DELAY_ADVANCE_TIME,
    MID_DELAY_CADENCE,
    MID_DELAY_VALIDITY_PERIOD,
    WRONG_TELMODEL_SOURCE,
    create_cm_cspsln,
    logger,
)

pointing_json = {
    "pointing": {
        "target": {
            "reference_frame": "ICRS",
            "target_name": "Polaris Australis",
            "ra": "21:08:47.92",
            "dec": "-88:57:22.9",
        }
    }
}

# expected delays
expected_delays = {
    "SKA063": np.array(
        [
            5.63213457e-08,
            2.74650637e-12,
            1.04668401e-16,
            -2.43404402e-21,
            -4.56476852e-26,
            5.26145846e-30,
        ]
    ),
    "SKA100": np.array(
        [
            -1.36162206e-06,
            -2.50181570e-12,
            -3.61228280e-17,
            2.21723929e-21,
            1.48305084e-26,
            -1.32313384e-31,
        ]
    ),
}


@pytest.mark.cspsln_mid
def test_mid_delay_manager_calculate_geometric_delays(
    tango_context,
    telmodel_source,
    telmodel_path_mid,
):
    # constant value for t0
    time_t0 = datetime(2023, 3, 1, 18, 15, 24, 841024)
    cm, _ = create_cm_cspsln(input_parameter=InputParameterMid(None))
    # set receptors IDs
    # Tel model is updated antenna 1 location,
    # it is now same as reference antenna
    cm.receptor_ids_str = ["SKA063", "SKA100"]

    # set target
    pointing_params = pointing_json["pointing"]
    target_ra = pointing_params["target"]["ra"]
    target_dec = pointing_params["target"]["dec"]
    cm.set_target(katpoint.Target(f"radec , {target_ra} , {target_dec}"))

    # call update config and calculate geometric delays
    delay_manager = DelayManagerMid(
        cm,
        telmodel_source,
        telmodel_path_mid,
        DELAY_ADVANCE_TIME,
        MID_DELAY_CADENCE,
        MID_DELAY_VALIDITY_PERIOD,
        logger,
    )
    delay_manager.update_config_params()
    calculated_delays = delay_manager.calculate_geometric_delays(
        time_t0, delay_manager.antenna_names
    )
    logger.info(calculated_delays)
    # verify calculated and expected delays are equal
    assert str(calculated_delays) == str(expected_delays)
    # verify delays are pointed to assigned receptors
    for receptor in cm.receptor_ids_str:
        assert receptor in str(calculated_delays)


@pytest.mark.cspsln_mid
def test_mid_delay_manager_start_time_for_delay_calculation(
    tango_context,
    telmodel_source,
    telmodel_path_mid,
    set_admin_mode_csp_to_online_mid,
):
    # Initialise
    cm, _ = create_cm_cspsln(input_parameter=InputParameterMid(None))
    delay_manager = DelayManagerMid(
        cm,
        telmodel_source,
        telmodel_path_mid,
        DELAY_ADVANCE_TIME,
        MID_DELAY_CADENCE,
        MID_DELAY_VALIDITY_PERIOD,
        logger,
    )
    # assert time_t0 is None
    assert delay_manager.start_time_for_delay_calculation is None
    # call calculate_delay_model
    delay_manager.calculate_delay_model()

    # assert time_t0 is updated
    assert delay_manager.start_time_for_delay_calculation is not None
    start_time = delay_manager.start_time_for_delay_calculation
    delay_manager.calculate_delay_model()

    # assert for next iteration,
    # delay is calculated at time_t0+MID_DELAY_CADENCE
    assert (
        delay_manager.start_time_for_delay_calculation
        == start_time + timedelta(seconds=MID_DELAY_CADENCE)
    )


# This test verifies the number of retries added got exhausted for SKB-495
@pytest.mark.cspsln_mid
def test_mid_delay_wrong_path(
    tango_context, telmodel_path_mid, set_admin_mode_csp_to_online_mid
):
    # Initialise
    cm, _ = create_cm_cspsln(input_parameter=InputParameterMid(None))
    delay_manager = DelayManagerMid(
        cm,
        WRONG_TELMODEL_SOURCE,
        telmodel_path_mid,
        DELAY_ADVANCE_TIME,
        MID_DELAY_CADENCE,
        MID_DELAY_VALIDITY_PERIOD,
        logger,
    )
    # assert time_t0 is None
    assert delay_manager.start_time_for_delay_calculation is None
    # call calculate_delay_model
    result_code, message = delay_manager.update_config_params()
    assert result_code == ResultCode.FAILED
    assert (
        message
        == "Exception occurred while fetching the station locations: 404: 404"
        + " Commit Not FoundNumber of retries exhausted"
    )
