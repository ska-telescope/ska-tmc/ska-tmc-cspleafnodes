import json
from os.path import dirname, join

import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.dev_factory import DevFactory
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.configure_command import Configure
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterMid
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_cm_obstate_attribute_value,
)


def get_configure_input_str(
    configure_input_file="cspsln_configure_command.json",
):
    path = join(
        dirname(__file__), "..", "..", "..", "..", "data", configure_input_file
    )
    with open(path, "r") as f:
        configure_input_str = f.read()
    return configure_input_str


@pytest.mark.cspsln_mid
def test_configure_command_completed(
    tango_context,
    csp_subarray_device_mid,
    task_callback,
    set_admin_mode_csp_to_online_mid,
):
    # set CSP Subarray obsState to be ObsState.IDLE
    dev_factory = DevFactory()
    csp_subarray = dev_factory.get_device(csp_subarray_device_mid)
    csp_subarray.SetDirectObsState(ObsState.IDLE)

    cm, _ = get_cspsln_command_obj(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_mid, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    assert cm.is_command_allowed("Configure")
    cm.configure(configure_input_str, task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    cm.update_device_obs_state(csp_subarray_device_mid, ObsState.CONFIGURING)
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.CONFIGURING)
    cm.update_device_obs_state(csp_subarray_device_mid, ObsState.READY)
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.READY)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspsln_mid
def test_configure_non_sidereal_command_completed(
    tango_context,
    csp_subarray_device_mid,
    task_callback,
    set_admin_mode_csp_to_online_mid,
):
    # set CSP Subarray obsState to be ObsState.IDLE
    dev_factory = DevFactory()
    csp_subarray = dev_factory.get_device(csp_subarray_device_mid)
    csp_subarray.SetDirectObsState(ObsState.IDLE)

    cm, _ = get_cspsln_command_obj(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_mid, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str(
        "cspsln_configure_command_non_sidereal.json"
    )
    assert cm.is_command_allowed("Configure")
    cm.configure(configure_input_str, task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    cm.update_device_obs_state(csp_subarray_device_mid, ObsState.CONFIGURING)
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.CONFIGURING)
    cm.update_device_obs_state(csp_subarray_device_mid, ObsState.READY)
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.READY)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspsln_mid
def test_configure_command_fail_check_allowed_with_invalid_obsState(
    tango_context, csp_subarray_device_mid, set_admin_mode_csp_to_online_mid
):
    # set CSP Subarray obsState to be ObsState.IDLE
    dev_factory = DevFactory()
    csp_subarray = dev_factory.get_device(csp_subarray_device_mid)
    csp_subarray.SetDirectObsState(ObsState.EMPTY)
    cm, _ = get_cspsln_command_obj(Configure)
    assert not cm.is_command_allowed_callable("Configure")()


@pytest.mark.cspsln_mid
def test_telescope_configure_command_fail_check_allowed_with_device_unresponsive(  # noqa: E501
    csp_subarray_device_mid,
):
    cm, _ = get_cspsln_command_obj(Configure)
    cm._device = DeviceInfo(csp_subarray_device_mid, _unresponsive=True)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_mid),
    ):
        cm.is_command_allowed("Configure")


@pytest.mark.cspsln_mid
def test_configure_command_fail_subarray(
    tango_context, csp_subarray_device_mid, set_admin_mode_csp_to_online_mid
):
    cm, _ = create_cm_cspsln()
    adapter_factory = HelperAdapterFactory()
    attrs = {"Configure.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_mid, AdapterType.CSPSUBARRAY, proxy=subarrayMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_mid, cm, ObsState.IDLE
    )
    configure = Configure(cm, logger)
    configure.adapter_factory = adapter_factory
    configure_input_str = get_configure_input_str()
    assert cm.is_command_allowed("Configure")
    (result_code, message) = configure.do(configure_input_str)
    assert result_code == ResultCode.FAILED
    assert csp_subarray_device_mid in message


@pytest.mark.cspsln_mid
def test_configure_resources_command_not_allowed(
    csp_subarray_device_mid,
    task_callback,
    tango_context,
    set_admin_mode_csp_to_online_mid,
):
    cm, _ = create_cm_cspsln(
        input_parameter=InputParameterMid(),
    )
    assert cm.is_command_allowed("Configure")
    configure_input_str = get_configure_input_str()

    cm.update_device_obs_state(csp_subarray_device_mid, ObsState.EMPTY)
    cm.configure(configure_input_str, task_callback)

    task_callback.assert_against_call(status=TaskStatus.QUEUED)

    task_callback.assert_against_call(
        status=TaskStatus.REJECTED,
        result=(ResultCode.NOT_ALLOWED, "Command is not allowed"),
    )


@pytest.mark.cspsln_mid
def test_configure_adr_63(
    tango_context,
    csp_subarray_device_mid,
    task_callback,
    set_admin_mode_csp_to_online_mid,
):
    # set CSP Subarray obsState to be ObsState.IDLE
    dev_factory = DevFactory()
    csp_subarray = dev_factory.get_device(csp_subarray_device_mid)
    csp_subarray.SetDirectObsState(ObsState.IDLE)

    cm, _ = get_cspsln_command_obj(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_mid, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    configure_input_str = json.loads(configure_input_str)
    configure_input_str["pointing"] = {
        "groups": [
            {
                "receptors": ["SKA001", "SKA036", "SKA100", "SKA063"],
                "field": {
                    "target_name": "Polaris Australis",
                    "reference_frame": "ICRS",
                    "attrs": {"c1": 317.199, "c2": -88.95636},
                },
            }
        ]
    }
    configure_input_str = json.dumps(configure_input_str)
    assert cm.is_command_allowed("Configure")
    cm.configure(configure_input_str, task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    cm.update_device_obs_state(csp_subarray_device_mid, ObsState.CONFIGURING)
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.CONFIGURING)
    cm.update_device_obs_state(csp_subarray_device_mid, ObsState.READY)
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.READY)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )
