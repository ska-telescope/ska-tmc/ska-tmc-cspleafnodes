import pytest
from ska_control_model import AdminMode, HealthState
from ska_tango_base.control_model import ControlMode, SimulationMode, TestMode
from tango import DevState

from ska_tmc_cspsubarrayleafnode import release


@pytest.mark.cspsln
def test_attributes(cspsln_device) -> None:
    assert cspsln_device.State() == DevState.ON
    cspsln_device.loggingTargets = ["console::cout"]
    assert "console::cout" in cspsln_device.loggingTargets
    cspsln_device.testMode = TestMode.NONE
    assert cspsln_device.testMode == TestMode.NONE
    cspsln_device.simulationMode = SimulationMode.FALSE
    assert cspsln_device.simulationMode == SimulationMode.FALSE
    cspsln_device.controlMode = ControlMode.REMOTE
    assert cspsln_device.controlMode == ControlMode.REMOTE
    cspsln_device.cspSubarrayDevName = "cspsubarray"
    assert cspsln_device.cspSubarrayDevName == "cspsubarray"
    assert cspsln_device.versionId == release.version
    assert cspsln_device.buildState == (
        "{},{},{}".format(release.name, release.version, release.description)
    )
    assert cspsln_device.healthState == HealthState.OK
    assert cspsln_device.cspSubarrayAdminMode == AdminMode.ONLINE
    assert cspsln_device.isAdminModeEnabled is True
