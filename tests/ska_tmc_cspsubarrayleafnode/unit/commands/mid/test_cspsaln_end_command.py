import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.end_command import End
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_unresponsive,
)


@pytest.mark.cspsln_mid
def test_end_command_completed(
    tango_context,
    task_callback,
    csp_subarray_device_mid,
    set_admin_mode_csp_to_online_mid,
):
    cm, _ = get_cspsln_command_obj(End)
    simulate_obs_state_change_on_device(
        csp_subarray_device_mid, cm, ObsState.READY
    )
    assert cm.is_command_allowed("End")
    cm.end(task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspsln_mid
def test_end_command_fail_subarray(
    tango_context, csp_subarray_device_mid, set_admin_mode_csp_to_online_mid
):
    cm, _ = create_cm_cspsln()
    adapter_factory = HelperAdapterFactory()

    # include exception in End command
    attrs = {"GoToIdle.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_mid, AdapterType.CSPSUBARRAY, proxy=subarrayMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_mid, cm, ObsState.IDLE
    )
    end_command = End(cm, logger)
    end_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("End")
    (result_code, message) = end_command.do()
    assert result_code == ResultCode.FAILED
    assert csp_subarray_device_mid in message


@pytest.mark.cspsln_mid
def test_end_command_fail_check_allowed_with_invalid_obsState(
    tango_context, csp_subarray_device_mid, set_admin_mode_csp_to_online_mid
):
    cm, _ = get_cspsln_command_obj(End)
    assert not cm.is_command_allowed_callable("End")()


@pytest.mark.cspsln_mid
def test_end_command_fail_check_allowed_with_device_unresponsive(
    csp_subarray_device_mid,
):
    cm, _ = get_cspsln_command_obj(End)
    cm._device = DeviceInfo(csp_subarray_device_mid, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_mid),
    ):
        cm.is_command_allowed("End")
