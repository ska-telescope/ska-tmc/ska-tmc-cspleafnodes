import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.abort_command import Abort
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj_low,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_cm_obstate_attribute_value,
    wait_for_unresponsive,
)


def get_abort_command_obj():
    cm, _ = create_cm_cspsln()
    abort_command = Abort(cm, logger=logger)
    return abort_command, cm


@pytest.mark.cspsln_low
@pytest.mark.parametrize(
    "Obsstate",
    [
        ObsState.RESOURCING,
        ObsState.IDLE,
        ObsState.CONFIGURING,
        ObsState.READY,
        ObsState.SCANNING,
    ],
)
def test_low_abort_command(
    tango_context,
    csp_subarray_device_low,
    Obsstate,
    set_admin_mode_csp_to_online_low,
):
    cm, _ = get_cspsln_command_obj_low(Abort)
    simulate_obs_state_change_on_device(csp_subarray_device_low, cm, Obsstate)
    assert wait_for_cm_obstate_attribute_value(cm, Obsstate)
    cm.is_command_allowed("Abort")
    result_code, _ = cm.abort_commands()
    assert result_code == ResultCode.QUEUED


@pytest.mark.cspsln_low
def test_abort_command_with_failed_csp_subarray(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
):
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow)

    helper_adapter_factory = HelperAdapterFactory()

    failing_dev = "low-csp/subarray/01"
    attrs = {"Abort.side_effect": Exception}
    CspsubarraylnMock = mock.Mock(**attrs)

    helper_adapter_factory.get_or_create_adapter(
        failing_dev, AdapterType.CSPSUBARRAY, proxy=CspsubarraylnMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.IDLE)
    abort_command = Abort(cm, logger=logger)
    abort_command.adapter_factory = helper_adapter_factory
    result_code, _ = abort_command.invoke_abort()
    assert result_code == ResultCode.FAILED


@pytest.mark.cspsln_low
def test_abort_command_is_allowed_device_unresponsive(csp_subarray_device_low):
    cm, _ = get_cspsln_command_obj_low(Abort)

    cm._device = DeviceInfo(csp_subarray_device_low, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_low),
    ):
        cm.is_command_allowed("Abort")


@pytest.mark.cspsln_low
def test_abort_command_invalid_obsstate(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
):
    cm, _ = get_cspsln_command_obj_low(Abort)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.EMPTY
    )
    assert not cm.is_command_allowed_callable("Abort")()
