from datetime import datetime, timedelta

import katpoint
import numpy as np
import pytest
from ska_tango_base.commands import ResultCode

from ska_tmc_cspsubarrayleafnode.delay_model_low import DelayManagerLow
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    DELAY_ADVANCE_TIME,
    DELAY_FOR_431_IN_NS,
    LOCAL_DATA_PATH,
    LOCAL_DATA_SOURCE,
    LOW_DELAY_CADENCE,
    LOW_DELAY_VALIDITY_PERIOD,
    WRONG_TELMODEL_SOURCE,
    create_cm_cspsln,
    logger,
)

subarray_beams_json = {
    "sky_coordinates": {"reference_frame": "ICRS", "c1": 180.0, "c2": 45.0}
}

# expected delays
expected_delays = {
    "1": np.array(
        [
            5.46741922e-07,
            1.91405436e-11,
            -5.48681754e-16,
            -1.69633307e-20,
            2.43183945e-25,
            4.42323364e-30,
        ]
    ),
    "2": np.array(
        [
            -4.12337192e-07,
            -1.23279973e-11,
            4.41834135e-16,
            1.09257093e-20,
            -1.95827535e-25,
            -2.83516554e-30,
        ]
    ),
    "3": np.array(
        [
            1.74853085e-07,
            1.14177190e-11,
            -1.90437559e-16,
            -1.01189584e-20,
            8.44046630e-26,
            2.65982596e-30,
        ]
    ),
    "4": np.array(
        [
            1.22218351e-07,
            -2.34304845e-11,
            -1.03581005e-17,
            2.07652498e-20,
            4.59133819e-27,
            -5.52061400e-30,
        ]
    ),
    "5": np.array(
        [
            -1.69131049e-08,
            3.21281965e-13,
            1.39818282e-17,
            -2.84734961e-22,
            -6.19697102e-27,
            7.78505337e-32,
        ]
    ),
    "6": np.array(
        [
            -6.44881094e-07,
            -2.68714198e-12,
            6.12793851e-16,
            2.38151965e-21,
            -2.71599727e-25,
            -5.37716893e-31,
        ]
    ),
}


# expected delays with fixed delays defined
expected_delays_for_custom_data = {
    "431": np.array(
        [
            1.09083475e-05,
            -1.24063692e-09,
            -5.01962031e-15,
            1.09951326e-18,
            2.22479877e-24,
            -2.93009728e-28,
        ]
    ),
    "432": np.array(
        [
            1.43082475e-05,
            -1.24063692e-09,
            -5.01962031e-15,
            1.09951326e-18,
            2.22479922e-24,
            -2.93009964e-28,
        ]
    ),
}


@pytest.mark.cspsln_low
def test_low_delay_manager_calculate_geometric_delays(
    tango_context,
    telmodel_source,
    telmodel_path_low,
    set_admin_mode_csp_to_online_low,
):
    # constant value for t0
    time_t0 = datetime(2023, 3, 1, 18, 15, 24, 841024)
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    # set stations IDs
    cm.station_ids = [1, 2, 3, 4, 5, 6]

    # set target
    sky_coordinates = subarray_beams_json["sky_coordinates"]
    target_ra = sky_coordinates["c1"]
    target_dec = sky_coordinates["c2"]
    cm.set_target(katpoint.Target(f"radec , {target_ra} , {target_dec}"))

    # call update config and calculate geometric delays
    delay_manager_low = DelayManagerLow(
        cm,
        telmodel_source,
        telmodel_path_low,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        logger,
    )
    delay_manager_low.update_config_params()

    calculated_delays = delay_manager_low.calculate_geometric_delays(
        time_t0, delay_manager_low.station_names
    )
    # verify calculated and expected delays are equal
    assert str(calculated_delays) == str(expected_delays)


@pytest.mark.cspsln_low
def test_low_delay_manager_start_time_for_delay_calculation(
    tango_context,
    telmodel_source,
    telmodel_path_low,
    set_admin_mode_csp_to_online_low,
):
    # Initialise
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    delay_manager = DelayManagerLow(
        cm,
        telmodel_source,
        telmodel_path_low,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        logger,
    )
    # assert time_t0 is None
    assert delay_manager.start_time_for_delay_calculation is None
    # call calculate_delay_model
    cm.delay_start_time = datetime.today()
    delay_manager.calculate_delay_model()

    # assert time_t0 is updated
    assert delay_manager.start_time_for_delay_calculation is not None
    start_time = delay_manager.start_time_for_delay_calculation
    delay_manager.calculate_delay_model()
    # assert for next iteration, delay is
    # calculated at time_t0+LOW_DELAY_CADENCE
    assert (
        delay_manager.start_time_for_delay_calculation
        == start_time + timedelta(seconds=LOW_DELAY_CADENCE)
    )


# This test verifies the number of retries added got exhausted for SKB-495
@pytest.mark.cspsln_low
def test_low_delay_wrong_path(
    tango_context, telmodel_path_low, set_admin_mode_csp_to_online_low
):
    # Initialise
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    delay_manager_low = DelayManagerLow(
        cm,
        WRONG_TELMODEL_SOURCE,
        telmodel_path_low,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        logger,
    )
    # assert time_t0 is None
    assert delay_manager_low.start_time_for_delay_calculation is None
    # call calculate_delay_model
    result_code, message = delay_manager_low.update_config_params()
    assert result_code == ResultCode.FAILED
    assert (
        message
        == "Exception occurred while fetching the station locations: 404: 404"
        + " Commit Not FoundNumber of retries exhausted"
    )


@pytest.mark.cspsln_low
def test_delay_calculation_with_fixed_delays_defined(
    tango_context, set_admin_mode_csp_to_online_low
):
    """Test the delay calculation with fixed delays defined in the model"""
    component_manager, _ = create_cm_cspsln(
        input_parameter=InputParameterLow(None)
    )
    delay_manager_low = DelayManagerLow(
        component_manager,
        LOCAL_DATA_SOURCE,
        LOCAL_DATA_PATH,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        logger,
    )

    # constant value for t0
    time_t0 = datetime(2023, 3, 1, 18, 15, 24, 841024)

    # set stations ID to s10-3
    component_manager.station_ids = [431, 432]

    # set target
    sky_coordinates = subarray_beams_json["sky_coordinates"]
    target_ra = sky_coordinates["c1"]
    target_dec = sky_coordinates["c2"]
    component_manager.set_target(
        katpoint.Target(f"radec , {target_ra} , {target_dec}")
    )

    delay_manager_low.update_config_params()

    calculated_delays = delay_manager_low.calculate_geometric_delays(
        time_t0, delay_manager_low.station_names
    )
    logger.info("Calculated Delays: %s", calculated_delays)

    # verify calculated and expected delays are equal
    np.testing.assert_allclose(
        calculated_delays["431"], expected_delays_for_custom_data["431"]
    )
    np.testing.assert_allclose(
        calculated_delays["432"], expected_delays_for_custom_data["432"]
    )

    first_term_with_fixed_delays = expected_delays_for_custom_data["432"][0]
    first_term_without_fixed_delays = expected_delays_for_custom_data["431"][0]

    difference_in_first_terms_in_ns = (
        first_term_with_fixed_delays - first_term_without_fixed_delays
    ) * 1e9

    logger.info(
        "Difference in first term with and without fixed delays: %s",
        difference_in_first_terms_in_ns,
    )
    assert round(difference_in_first_terms_in_ns, 1) == DELAY_FOR_431_IN_NS
