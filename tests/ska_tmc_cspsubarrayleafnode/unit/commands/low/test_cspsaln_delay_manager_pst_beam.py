from datetime import datetime, timedelta

import katpoint
import numpy as np
import pytest

from ska_tmc_cspsubarrayleafnode.delay_model_low import DelayManagerLow
from ska_tmc_cspsubarrayleafnode.delay_model_pst_beam import (
    DelayManagerPSTBeam,
)
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    DELAY_ADVANCE_TIME,
    LOW_DELAY_CADENCE,
    LOW_DELAY_VALIDITY_PERIOD,
    create_cm_cspsln,
    logger,
)

beams = {
    "pst_beam_id": 1,
    "field": {
        "target_id": "PSR J0024-7204R",
        "reference_frame": "icrs",
        "attrs": {
            "c1": 6.023625,
            "c2": -72.08128333,
            "pm_c1": 4.8,
            "pm_c2": -3.3,
        },
    },
    "stn_beam_id": 1,
    "stn_weights": [0.9, 1, 1, 1, 0.9, 1],
}

# expected delays
expected_delays = {
    "1": np.array(
        [
            -5.36521366e-07,
            -9.00304195e-12,
            2.06804047e-16,
            7.97894868e-21,
            -9.16594922e-26,
            -2.08849849e-30,
        ]
    ),
    "2": np.array(
        [
            3.96313654e-07,
            5.90880044e-12,
            -1.71639917e-16,
            -5.23667705e-21,
            7.60739670e-26,
            1.36511301e-30,
        ]
    ),
    "3": np.array(
        [
            -1.63147516e-07,
            -5.20005524e-12,
            6.38734864e-17,
            4.60854875e-21,
            -2.83101566e-26,
            -1.21492797e-30,
        ]
    ),
    "4": np.array(
        [
            -1.75751810e-07,
            1.01715409e-11,
            4.32938654e-17,
            -9.01452190e-21,
            -1.91876805e-26,
            2.40263157e-30,
        ]
    ),
    "5": np.array(
        [
            1.81929544e-08,
            -1.22238248e-13,
            -6.60933700e-18,
            1.08333375e-22,
            2.92935095e-27,
            -2.98211864e-32,
        ]
    ),
    "6": np.array(
        [
            6.57015567e-07,
            1.93114982e-12,
            -2.61911670e-16,
            -1.71148899e-21,
            1.16083505e-25,
            4.14128854e-31,
        ]
    ),
}


@pytest.mark.cspsln_low
def test_pst_beam_delay_manager_calculate_geometric_delays(
    tango_context,
    telmodel_source,
    telmodel_path_low,
    set_admin_mode_csp_to_online_low,
):
    # constant value for t0
    time_t0 = datetime(2023, 3, 1, 18, 15, 24, 841024)
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    # set stations IDs
    cm.station_ids = [1, 2, 3, 4, 5, 6]

    # get values for beam_id, right_ascension, ddeclination
    pst_beam_id = beams.get("pst_beam_id")
    target_ra = beams["field"]["attrs"].get("c1")
    target_dec = beams["field"]["attrs"].get("c2")

    # call update config and calculate geometric delays
    delay_manager_pst_beam = DelayManagerPSTBeam(
        cm,
        telmodel_source,
        telmodel_path_low,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        pst_beam_id,
        target_ra,
        target_dec,
        logger,
    )
    delay_manager_pst_beam.update_config_params()

    calculated_delays = delay_manager_pst_beam.calculate_geometric_delays(
        time_t0, delay_manager_pst_beam.station_names
    )
    print(calculated_delays)
    # verify calculated and expected delays are equal
    assert str(calculated_delays) == str(expected_delays)


@pytest.mark.cspsln_low
def test_pst_beam_delay_manager_start_time_for_delay_calculation(
    tango_context,
    telmodel_source,
    telmodel_path_low,
    set_admin_mode_csp_to_online_low,
):
    # get values for beam_id, right_ascension, ddeclination
    pst_beam_id = beams.get("pst_beam_id")
    target_ra = beams["field"]["attrs"].get("c1")
    target_dec = beams["field"]["attrs"].get("c2")
    # Initialise
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    delay_manager_pst_beam = DelayManagerPSTBeam(
        cm,
        telmodel_source,
        telmodel_path_low,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        pst_beam_id,
        target_ra,
        target_dec,
        logger,
    )
    # assert time_t0 is None
    assert delay_manager_pst_beam.start_time_for_delay_calculation is None
    # call calculate_delay_model
    cm.delay_start_time = datetime.today()
    delay_manager_pst_beam.calculate_delay_model()

    # assert time_t0 is updated
    assert delay_manager_pst_beam.start_time_for_delay_calculation is not None
    start_time = delay_manager_pst_beam.start_time_for_delay_calculation
    delay_manager_pst_beam.calculate_delay_model()
    # assert for next iteration, delay is
    # calculated at time_t0+LOW_DELAY_CADENCE
    assert (
        delay_manager_pst_beam.start_time_for_delay_calculation
        == start_time + timedelta(seconds=LOW_DELAY_CADENCE)
    )


@pytest.mark.cspsln_low
def test_pst_beam_and_station_beam_delay_with_same_target_values_and_time(
    tango_context,
    telmodel_source,
    telmodel_path_low,
    set_admin_mode_csp_to_online_low,
):
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    # set stations IDs
    cm.station_ids = [1, 2, 3, 4, 5, 6]
    cm.delay_start_time = datetime.today()
    # get values for beam_id, right_ascension, ddeclination
    pst_beam_id = beams.get("pst_beam_id")
    target_ra = beams["field"]["attrs"].get("c1")
    target_dec = beams["field"]["attrs"].get("c2")

    # call update config and calculate geometric delays
    delay_manager_pst_beam = DelayManagerPSTBeam(
        cm,
        telmodel_source,
        telmodel_path_low,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        pst_beam_id,
        target_ra,
        target_dec,
        logger,
    )
    delay_manager_pst_beam.update_config_params()

    pst_beam_delays = delay_manager_pst_beam.calculate_geometric_delays(
        cm.delay_start_time, delay_manager_pst_beam.station_names
    )

    cm.set_target(katpoint.Target(f"radec , {target_ra} , {target_dec}"))

    # call update config and calculate geometric delays
    delay_manager_low = DelayManagerLow(
        cm,
        telmodel_source,
        telmodel_path_low,
        DELAY_ADVANCE_TIME,
        LOW_DELAY_CADENCE,
        LOW_DELAY_VALIDITY_PERIOD,
        logger,
    )
    delay_manager_low.update_config_params()

    station_beam_delays = delay_manager_low.calculate_geometric_delays(
        cm.delay_start_time, delay_manager_low.station_names
    )
    # verify calculated and expected delays are equal
    assert str(station_beam_delays) == str(pst_beam_delays)
