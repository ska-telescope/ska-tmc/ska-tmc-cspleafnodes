import json
import threading
from os.path import dirname, join

import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.assign_resources_command import (
    AssignResources,
)
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj_low,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_cm_obstate_attribute_value,
    wait_for_unresponsive,
)


def get_assign_input_str(
    assign_input_file="cspsln_assignresources_command_low.json",
) -> str:
    path = join(
        dirname(__file__), "..", "..", "..", "..", "data", assign_input_file
    )
    with open(path, "r") as f:
        assign_input_str = f.read()
    return assign_input_str


@pytest.mark.xfail(
    reason="Failed due to TRANSIENT CORBA system exception: \
        TRANSIENT_NoUsableProfile"
)
@pytest.mark.cspsln_low
def test_assign_resources_command_completed(
    csp_subarray_device_low, task_callback, set_admin_mode_csp_to_online_low
):
    adapter_factory = HelperAdapterFactory()

    cm, _ = create_cm_cspsln(
        input_parameter=InputParameterLow(),
    )
    assert cm.is_command_allowed("AssignResources")
    assign_input_str = get_assign_input_str()
    json_argument = json.loads(assign_input_str)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_low,
        AdapterType.CSPSUBARRAY,
    )
    assign_command = AssignResources(cm, logger)
    assign_command.adapter_factory = adapter_factory
    assign_command.assign_resources(
        json_argument, task_callback, threading.Event()
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.RESOURCING
    )
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.RESOURCING)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.IDLE)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspsln_low
def test_telescope_assign_resources_command_missing_subarray_id_key(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, assign_res_command = get_cspsln_command_obj_low(AssignResources)
    assign_input_str = get_assign_input_str()
    json_argument = json.loads(assign_input_str)
    json_argument["common"].pop("subarray_id", None)
    assert cm.is_command_allowed("AssignResources")
    # assert cm.is_command_allowed("AssignResources")
    (result_code, _) = assign_res_command.do_low(json.dumps(json_argument))
    assert result_code == ResultCode.FAILED


@pytest.mark.cspsln_low
def test_assign_resources_command_fail_subarray(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, _ = create_cm_cspsln(
        input_parameter=InputParameterLow,
    )
    adapter_factory = HelperAdapterFactory()
    attrs = {"AssignResources.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_low, AdapterType.CSPSUBARRAY, proxy=subarrayMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.EMPTY
    )
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.EMPTY)
    assign_res_command = AssignResources(cm, logger)
    assign_res_command.adapter_factory = adapter_factory
    assign_input_str = get_assign_input_str()
    assert cm.is_command_allowed("AssignResources")
    (result_code, message) = assign_res_command.do_low(assign_input_str)
    assert result_code == ResultCode.FAILED
    assert csp_subarray_device_low in message


@pytest.mark.cspsln_low
def test_assign_resources_command_empty_input_json(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, assign_res_command = get_cspsln_command_obj_low(AssignResources)
    assert cm.is_command_allowed("AssignResources")
    (result_code, _) = assign_res_command.do_low("")
    assert result_code == ResultCode.FAILED


@pytest.mark.cspsln_low
def test_assign_resources_command_fail_check_allowed_with_invalid_obsState(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, _ = get_cspsln_command_obj_low(AssignResources)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.READY
    )
    assert not cm.is_command_allowed_callable("AssignResources")()


@pytest.mark.cspsln_low
def test_telescope_assign_resources_command_fail_check_allowed_with_device_unresponsive(  # noqa: E501
    csp_subarray_device_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(AssignResources)
    cm._device = DeviceInfo(csp_subarray_device_low, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_low),
    ):
        cm.is_command_allowed("AssignResources")
