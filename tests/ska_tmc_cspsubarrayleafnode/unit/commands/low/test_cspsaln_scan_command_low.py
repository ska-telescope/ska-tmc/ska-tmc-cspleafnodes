import json
from os.path import dirname, join

import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.scan_command import Scan
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj_low,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_unresponsive,
)


def get_scan_input_str(
    scan_input_file="cspsln_scan_command_low.json",
) -> str:
    path = join(
        dirname(__file__), "..", "..", "..", "..", "data", scan_input_file
    )
    with open(path, "r") as f:
        scan_input_str = f.read()
    return scan_input_str


@pytest.mark.cspsln_low
def test_scan_command_completed_low(
    tango_context,
    csp_subarray_device_low,
    task_callback,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Scan)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.READY
    )
    assert cm.is_command_allowed("Scan")
    scan_input_str = get_scan_input_str()
    cm.scan(scan_input_str, task_callback=task_callback)

    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspsln_low
def test_scan_command_missing_scan_id_key(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, scan_command = get_cspsln_command_obj_low(Scan)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.READY
    )
    scan_input_str = get_scan_input_str()
    json_argument = json.loads(scan_input_str)
    json_argument["lowcbf"].pop("scan_id", None)
    assert cm.is_command_allowed("Scan")
    (result_code, _) = scan_command.do_low(json.dumps(json_argument))
    assert result_code == ResultCode.FAILED


@pytest.mark.cspsln_low
def test_scan_command_fail_cspsubarray(
    tango_context, csp_subarray_device_low
) -> None:
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    adapter_factory = HelperAdapterFactory()

    # include exception in Scan command
    attrs = {"Scan.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_low, AdapterType.CSPSUBARRAY, proxy=subarrayMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.READY
    )
    scan_command = Scan(cm, logger)
    scan_command.adapter_factory = adapter_factory
    scan_input_str = get_scan_input_str()
    assert cm.is_command_allowed("Scan")
    (result_code, message) = scan_command.do_low(scan_input_str)
    assert result_code == ResultCode.FAILED
    assert csp_subarray_device_low in message


@pytest.mark.cspsln_low
def test_scan_command_empty_input_json(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, scan_res_command = get_cspsln_command_obj_low(Scan)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.READY
    )
    assert cm.is_command_allowed("Scan")
    (result_code, _) = scan_res_command.do("")
    assert result_code == ResultCode.FAILED


@pytest.mark.cspsln_low
def test_scan_command_fail_check_allowed_with_invalid_obsState(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, _ = get_cspsln_command_obj_low(Scan)
    assert not cm.is_command_allowed_callable("Scan")()


@pytest.mark.cspsln_low
def test_scan_command_fail_check_allowed_with_device_unresponsive(  # noqa: E501
    csp_subarray_device_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Scan)
    cm._device = DeviceInfo(csp_subarray_device_low, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_low),
    ):
        cm.is_command_allowed("Scan")
