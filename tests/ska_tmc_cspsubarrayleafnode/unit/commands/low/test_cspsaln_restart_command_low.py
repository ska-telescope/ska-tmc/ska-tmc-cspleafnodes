import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.restart_command import Restart
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj_low,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_unresponsive,
)


def get_restart_command_obj():
    cm, _ = create_cm_cspsln()
    restart_command = Restart(cm, logger=logger)
    return restart_command, cm


@pytest.mark.cspsln_low
def test_restart_command_completed(
    tango_context,
    csp_subarray_device_low,
    task_callback,
    set_admin_mode_csp_to_online_low,
):
    cm, _ = get_cspsln_command_obj_low(Restart)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.ABORTED
    )
    cm.restart(task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspsln_low
def test_restart_command_with_failed_csp_subarray(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, _ = create_cm_cspsln(
        input_parameter=InputParameterLow,
    )

    adapter_factory = HelperAdapterFactory()
    attrs = {"Restart.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_low, AdapterType.CSPSUBARRAY, proxy=subarrayMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.ABORTED
    )
    restart_command = Restart(cm, logger)
    restart_command.adapter_factory = adapter_factory
    (result_code, message) = restart_command.do()
    assert result_code == ResultCode.FAILED
    assert csp_subarray_device_low in message


@pytest.mark.cspsln_low
def test_restart_command_fail_is_allowed_device_unresponsive(
    csp_subarray_device_low,
):
    cm, _ = get_cspsln_command_obj_low(Restart)
    cm._device = DeviceInfo(csp_subarray_device_low, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_low),
    ):
        cm.is_command_allowed("Restart")


@pytest.mark.cspsln_low
def test_restart_command_invalid_obsstate(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
):
    cm, _ = get_cspsln_command_obj_low(Restart)
    assert not cm.is_command_allowed_callable("Restart")()
