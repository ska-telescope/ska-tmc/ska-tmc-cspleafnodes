import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.release_all_resources_command import (  # noqa: E501
    ReleaseAllResources,
)
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj_low,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_unresponsive,
)


@pytest.mark.cspsln_low
def test_release_resources_command_queued(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(ReleaseAllResources)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    assert cm.is_command_allowed("ReleaseAllResources")
    cm.release_all_resources(task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )


@pytest.mark.cspsln_low
def test_release_resources_command_fail_subarray(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    adapter_factory = HelperAdapterFactory()

    # include exception in ReleaseAllResources command
    attrs = {"ReleaseAllResources.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_low, AdapterType.CSPSUBARRAY, proxy=subarrayMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    release_res_command = ReleaseAllResources(cm, logger)
    release_res_command.adapter_factory = adapter_factory
    assert cm.is_command_allowed("ReleaseAllResources")
    (result_code, message) = release_res_command.do_low()
    assert result_code == ResultCode.FAILED
    assert csp_subarray_device_low in message


@pytest.mark.cspsln_low
def test_release_resources_command_fail_check_allowed_with_invalid_obsState(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
) -> None:
    cm, _ = get_cspsln_command_obj_low(ReleaseAllResources)
    assert not cm.is_command_allowed_callable("ReleaseAllResources")()


@pytest.mark.cspsln_low
def test_telescope_release_resources_command_fail_check_allowed_with_device_unresponsive(  # noqa: E501
    csp_subarray_device_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(ReleaseAllResources)
    cm._device = DeviceInfo(csp_subarray_device_low, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_low),
    ):
        cm.is_command_allowed("ReleaseAllResources")
