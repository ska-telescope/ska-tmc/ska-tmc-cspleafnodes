import json
from os.path import dirname, join

import mock
import pytest
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from ska_tango_base.executor import TaskStatus
from ska_tmc_common.adapters import AdapterType
from ska_tmc_common.device_info import DeviceInfo
from ska_tmc_common.exceptions import DeviceUnresponsive
from ska_tmc_common.test_helpers.helper_adapter_factory import (
    HelperAdapterFactory,
)

from ska_tmc_cspsubarrayleafnode.commands.configure_command import Configure
from ska_tmc_cspsubarrayleafnode.model.input import InputParameterLow
from tests.settings import (
    create_cm_cspsln,
    get_cspsln_command_obj_low,
    logger,
    simulate_obs_state_change_on_device,
    wait_for_cm_obstate_attribute_value,
    wait_for_unresponsive,
)


def get_configure_input_str(
    configure_input_file="cspsln_configure_command_low.json",
):
    path = join(
        dirname(__file__), "..", "..", "..", "..", "data", configure_input_file
    )
    with open(path, "r") as f:
        configure_input_str = f.read()
    return configure_input_str


@pytest.mark.cspsln_low
def test_configure_command_completed(
    tango_context,
    csp_subarray_device_low,
    task_callback,
    set_admin_mode_csp_to_online_low,
):
    cm, _ = get_cspsln_command_obj_low(
        Configure,
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    assert cm.is_command_allowed("Configure")
    configure_input_str = get_configure_input_str()
    cm.configure(configure_input_str, task_callback=task_callback)
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.QUEUED}
    )
    task_callback.assert_against_call(
        call_kwargs={"status": TaskStatus.IN_PROGRESS}
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.CONFIGURING
    )
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.CONFIGURING)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.READY
    )
    assert wait_for_cm_obstate_attribute_value(cm, ObsState.READY)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(ResultCode.OK, "Command Completed"),
    )


@pytest.mark.cspsln_low
def test_configure_command_fail_subarray(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
):
    cm, _ = create_cm_cspsln(
        input_parameter=InputParameterLow,
    )
    adapter_factory = HelperAdapterFactory()
    attrs = {"Configure.side_effect": Exception}
    subarrayMock = mock.Mock(**attrs)
    adapter_factory.get_or_create_adapter(
        csp_subarray_device_low, AdapterType.CSPSUBARRAY, proxy=subarrayMock
    )
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )

    configure = Configure(cm, logger)
    configure.adapter_factory = adapter_factory
    configure_input_str = get_configure_input_str()
    assert cm.is_command_allowed("Configure")
    (result_code, message) = configure.do(configure_input_str)
    assert result_code == ResultCode.FAILED
    assert csp_subarray_device_low in message


@pytest.mark.cspsln_low
def test_configure_command_fail_check_allowed_with_invalid_obsState(
    tango_context, csp_subarray_device_low, set_admin_mode_csp_to_online_low
):
    cm, _ = get_cspsln_command_obj_low(Configure)
    assert not cm.is_command_allowed_callable("Configure")()


@pytest.mark.cspsln_low
def test_telescope_configure_command_fail_check_allowed_with_device_unresponsive(  # noqa: E501
    csp_subarray_device_low,
):
    cm, _ = get_cspsln_command_obj_low(Configure)
    cm._device = DeviceInfo(csp_subarray_device_low, _unresponsive=True)
    assert wait_for_unresponsive(cm)
    with pytest.raises(
        DeviceUnresponsive,
        match="{} not available".format(csp_subarray_device_low),
    ):
        cm.is_command_allowed("Configure")


@pytest.mark.cspsln_low
def test_configure_command_with_missing_subarray_beam_id_key(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    json_argument = json.loads(configure_input_str)
    # delete subarray_beam_id key
    json_argument["mccs"]["subarray_beams"][0].pop("subarray_beam_id", None)
    assert cm.is_command_allowed("Configure")
    cm.configure(json.dumps(json_argument), task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(status=TaskStatus.FAILED)


@pytest.mark.cspsln_low
def test_configure_command_with_missing_config_id_key(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    json_argument = json.loads(configure_input_str)
    # delete config_id key
    json_argument["csp"]["common"].pop("config_id", None)
    assert cm.is_command_allowed("Configure")
    cm.configure(json.dumps(json_argument), task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(status=TaskStatus.FAILED)


@pytest.mark.cspsln
def test_configure_command_with_missing_subarray_beams_key(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    json_argument = json.loads(configure_input_str)
    # delete subarray_beams key
    json_argument["mccs"].pop("subarray_beams", None)
    assert cm.is_command_allowed("Configure")
    cm.configure(json.dumps(json_argument), task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            (
                "Missing subarray_beams key, Configure"
                + " command failed on CspSubarrayLeafNode: 'subarray_beams'"
            ),
        ),
        exception="Missing subarray_beams key,"
        + " Configure command failed on CspSubarrayLeafNode: 'subarray_beams'",
    )


@pytest.mark.cspsln
def test_configure_command_with_missing_sky_coordinates_key(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    json_argument = json.loads(configure_input_str)
    subarray_beams = json_argument["mccs"]["subarray_beams"][0]
    # delete sky_coordinates key
    subarray_beams.pop("sky_coordinates", None)
    json_argument["mccs"]["subarray_beams"][0] = subarray_beams
    assert cm.is_command_allowed("Configure")
    cm.configure(json.dumps(json_argument), task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            (
                "Missing Ra key, Configure command failed on"
                + " CspSubarrayLeafNode: 'sky_coordinates'"
            ),
        ),
        exception="Missing Ra key, "
        + "Configure command failed on CspSubarrayLeafNode: 'sky_coordinates'",
    )


@pytest.mark.cspsln
def test_configure_command_with_missing_sky_coordinates_ra_key(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    json_argument = json.loads(configure_input_str)
    sky_coordinates = json_argument["mccs"]["subarray_beams"][0][
        "sky_coordinates"
    ]
    # delete sky_coordinates Ra(c1) key
    sky_coordinates.pop("c1", None)
    json_argument["mccs"]["subarray_beams"][0][
        "sky_coordinates"
    ] = sky_coordinates
    assert cm.is_command_allowed("Configure")
    cm.configure(json.dumps(json_argument), task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            (
                "Missing Ra key, Configure command failed on"
                + " CspSubarrayLeafNode: 'c1'"
            ),
        ),
        exception="Missing Ra key, "
        + "Configure command failed on CspSubarrayLeafNode: 'c1'",
    )


@pytest.mark.cspsln
def test_configure_command_with_missing_sky_coordinates_dec_key(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    json_argument = json.loads(configure_input_str)
    sky_coordinates = json_argument["mccs"]["subarray_beams"][0][
        "sky_coordinates"
    ]
    # delete sky_coordinates Dec(c2) key
    sky_coordinates.pop("c2", None)
    json_argument["mccs"]["subarray_beams"][0][
        "sky_coordinates"
    ] = sky_coordinates
    assert cm.is_command_allowed("Configure")
    cm.configure(json.dumps(json_argument), task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            (
                "Missing Dec key, Configure command failed on"
                + " CspSubarrayLeafNode: 'c2'"
            ),
        ),
        exception="Missing Dec key, "
        + "Configure command failed on CspSubarrayLeafNode: 'c2'",
    )


@pytest.mark.cspsln
def test_configure_command_with_missing_stns_key(
    tango_context,
    task_callback,
    csp_subarray_device_low,
    set_admin_mode_csp_to_online_low,
) -> None:
    cm, _ = get_cspsln_command_obj_low(Configure)
    simulate_obs_state_change_on_device(
        csp_subarray_device_low, cm, ObsState.IDLE
    )
    configure_input_str = get_configure_input_str()
    json_argument = json.loads(configure_input_str)
    low_cbf_stations = json_argument["csp"]["lowcbf"]["stations"]
    # delete stns key
    low_cbf_stations.pop("stns", None)
    json_argument["csp"]["lowcbf"]["stations"] = low_cbf_stations
    assert cm.is_command_allowed("Configure")
    cm.configure(json.dumps(json_argument), task_callback=task_callback)
    task_callback.assert_against_call(status=TaskStatus.QUEUED)
    task_callback.assert_against_call(status=TaskStatus.IN_PROGRESS)
    task_callback.assert_against_call(
        status=TaskStatus.COMPLETED,
        result=(
            ResultCode.FAILED,
            (
                "Missing stns key, Configure command failed on"
                + " CspSubarrayLeafNode:'stns'"
            ),
        ),
        exception="Missing stns key, "
        + "Configure command failed on CspSubarrayLeafNode:'stns'",
    )
