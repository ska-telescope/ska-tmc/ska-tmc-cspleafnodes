import pytest
import tango
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import HealthState
from ska_tango_base.commands import ResultCode
from ska_tango_base.control_model import ObsState
from tango import Database, DeviceProxy

from tests.conftest import LOW_CSP_SUBARRAY, MID_CSP_SUBARRAY
from tests.settings import COMMAND_COMPLETED, logger
from tests.ska_tmc_cspsubarrayleafnode.integration.common import (
    wait_for_final_csp_subarray_obsstate,
)


@given(
    "a TANGO ecosystem with a set of devices deployed",
    target_fixture="device_list",
)
def device_list():
    db = Database()
    return db.get_device_exported("*")


@given(
    parsers.parse("a CspSubarrayLeafNode device"),
    target_fixture="cspsubarrayleaf_node",
)
def cspsubarrayleaf_node():
    database = Database()
    instance_list = database.get_device_exported_for_class(
        "LowTmcLeafNodeCspSubarray"
    )
    for instance in instance_list.value_string:
        return DeviceProxy(instance)

    instance_list = database.get_device_exported_for_class(
        "MidTmcLeafNodeCspSubarray"
    )
    for instance in instance_list.value_string:
        return DeviceProxy(instance)


@when(parsers.parse("I call the command {command_name}"))
def call_command(
    cspsubarrayleaf_node,
    command_name: str,
    json_factory,
) -> None:
    try:
        logger.info(
            "CSP Subarray Leaf Node device name: %s",
            cspsubarrayleaf_node.dev_name(),
        )
        if "mid" in cspsubarrayleaf_node.dev_name():
            csp_subarray_device = tango.DeviceProxy(MID_CSP_SUBARRAY)
            if csp_subarray_device.adminMode != 0:
                csp_subarray_device.adminMode = 0
        else:
            csp_subarray_device = tango.DeviceProxy(LOW_CSP_SUBARRAY)
            if csp_subarray_device.adminMode != 0:
                csp_subarray_device.adminMode = 0

        if command_name == "AssignResources":
            if "mid" in cspsubarrayleaf_node.dev_name():
                assign_res_string = json_factory(
                    "cspsln_AssignResources_command"
                )
            else:
                assign_res_string = json_factory(
                    "cspsln_assignresources_command_low"
                )
            pytest.command_result = cspsubarrayleaf_node.command_inout(
                command_name, assign_res_string
            )
        elif command_name == "Configure":
            if "mid" in cspsubarrayleaf_node.dev_name():
                configure_string = json_factory("cspsln_configure_command")
            else:
                configure_string = json_factory("cspsln_configure_command_low")
            pytest.command_result = cspsubarrayleaf_node.command_inout(
                command_name, configure_string
            )
        elif command_name == "Scan":
            if "mid" in cspsubarrayleaf_node.dev_name():
                scan_string = json_factory("cspsln_scan_command")
            else:
                scan_string = json_factory("cspsln_scan_command_low")
            pytest.command_result = cspsubarrayleaf_node.command_inout(
                command_name, scan_string
            )
        else:
            pytest.command_result = cspsubarrayleaf_node.command_inout(
                command_name
            )
    except Exception as ex:
        assert "CommandNotAllowed" in str(ex)
        pytest.command_result = "CommandNotAllowed"


@then(
    parsers.parse(
        "the {command_name} command is executed successfully on CSP Subarray"
    )
)
def check_command(
    cspsubarrayleaf_node, command_name: str, change_event_callbacks
) -> None:
    cspsln_health_state = cspsubarrayleaf_node.read_attribute(
        "healthState"
    ).value
    logger.info(
        "Current CSP Subarray leaf node healthstate is %s", cspsln_health_state
    )
    assert cspsln_health_state == HealthState.OK

    if pytest.command_result == "CommandNotAllowed":
        return

    assert pytest.command_result[0][0] == ResultCode.QUEUED
    unique_id = pytest.command_result[1][0]

    assert unique_id.endswith(str(command_name))
    cspsubarrayleaf_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )

    if command_name == "AssignResources":
        wait_for_final_csp_subarray_obsstate(
            cspsubarrayleaf_node, ObsState.IDLE
        )

    elif command_name == "Configure":
        wait_for_final_csp_subarray_obsstate(
            cspsubarrayleaf_node, ObsState.READY
        )

    elif command_name == "Scan":
        wait_for_final_csp_subarray_obsstate(
            cspsubarrayleaf_node, ObsState.SCANNING
        )
    elif command_name == "EndScan":
        wait_for_final_csp_subarray_obsstate(
            cspsubarrayleaf_node, ObsState.READY
        )

    elif command_name == "End":
        wait_for_final_csp_subarray_obsstate(
            cspsubarrayleaf_node, ObsState.IDLE
        )

    elif command_name == "ReleaseAllResources":
        wait_for_final_csp_subarray_obsstate(
            cspsubarrayleaf_node, ObsState.EMPTY
        )

    cspsubarrayleaf_node.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        change_event_callbacks["longRunningCommandResult"],
    )

    change_event_callbacks["longRunningCommandResult"].assert_change_event(
        (unique_id, COMMAND_COMPLETED),
        lookahead=4,
    )


scenarios(
    "../ska_tmc_cspsubarrayleafnode/features/cspsubarrayleafnode.feature"
)
