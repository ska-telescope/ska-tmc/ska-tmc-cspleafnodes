"""Common settings for testing of CSP Leaf Nodes
"""
import json
import logging
import time
from operator import methodcaller
from typing import Any, List, Union

import pytest
from ska_control_model import ObsState
from ska_ser_logging.configuration import configure_logging
from ska_tango_base.commands import ResultCode
from ska_tmc_common import DevFactory, FaultType, LivelinessProbeType
from tango import DeviceProxy
from tango.db import Database, DbDevInfo

from ska_tmc_cspmasterleafnode.manager import (
    CspMLNComponentManager,
    CspMLNComponentManagerMid,
)
from ska_tmc_cspsubarrayleafnode.delay_model import DelayManager
from ska_tmc_cspsubarrayleafnode.manager import (
    CspSLNComponentManagerLow,
    CspSLNComponentManagerMid,
)
from ska_tmc_cspsubarrayleafnode.model.input import (
    InputParameterLow,
    InputParameterMid,
)
from tests.conftest import (
    CSPSUBARRAYLEAFNODE_LOW,
    CSPSUBARRAYLEAFNODE_MID,
    LOW_CSP_SUBARRAY,
    MID_CSP_SUBARRAY,
)

dev_factory = DevFactory()

configure_logging()
logger = logging.getLogger(__name__)

SLEEP_TIME = 0.5
TIMEOUT = 100

LOW_DELAY_CADENCE = 300
LOW_DELAY_VALIDITY_PERIOD = 600
MID_DELAY_CADENCE = 10
MID_DELAY_VALIDITY_PERIOD = 20
DELAY_ADVANCE_TIME = 30

CSP_MASTER_DEVICE = "mid-csp/control/0"
CSP_MASTER_DEVICE_MID = "mid-csp/control/0"
CSP_MASTER_DEVICE_LOW = "low-csp/control/0"
CSP_SUBARRAY_DEVICE_MID = "mid-csp/subarray/01"
CSP_SUBARRAY_DEVICE_LOW = "low-csp/subarray/01"
CSP_MASTER_LEAF_DEVICE_MID = "mid-tmc/leaf-node-csp/0"
CSP_MASTER_LEAF_DEVICE_LOW = "low-tmc/leaf-node-csp/0"

COMMAND_COMPLETED = json.dumps([ResultCode.OK, "Command Completed"])
TIMEOUT_OCCURRED = json.dumps(
    [ResultCode.FAILED, "Timeout has occurred, command failed"]
)
EXCEPTION_OCCURRED = json.dumps(
    [ResultCode.FAILED, "Exception occured, command failed."]
)

TIMEOUT_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.STUCK_IN_INTERMEDIATE_STATE,
        "error_message": "Command stuck in processing",
        "result": ResultCode.FAILED,
        "intermediate_state": ObsState.RESOURCING,
    }
)

ERROR_PROPAGATION_DEFECT = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.LONG_RUNNING_EXCEPTION,
        "error_message": "Exception occured, command failed.",
        "result": ResultCode.FAILED,
    }
)

COMMAND_NOT_ALLOWED_AFTER_QUEUING = json.dumps(
    {
        "enabled": True,
        "fault_type": FaultType.COMMAND_NOT_ALLOWED_AFTER_QUEUING,
        "error_message": "Command is not allowed",
        "result": ResultCode.QUEUED,
    }
)

RESET_DEFECT = json.dumps(
    {
        "enabled": False,
        "fault_type": FaultType.FAILED_RESULT,
        "error_message": "Default exception.",
        "result": ResultCode.FAILED,
    }
)

WRONG_TELMODEL_SOURCE = (
    "gitlab://gitlab.com/ska-telescope/ska-telmodel-data?t1#tmdata"
)

LOCAL_DATA_SOURCE = f"file://{__file__}/../data/"
LOCAL_DATA_PATH = "low-layout.json"

DELAY_FOR_431_IN_NS = 3399.9


def count_faulty_devices(component_manager):
    """Returns the count of faulty devices

    :return: Number of faulty/unresponsive devices

    :rtype: int
    """
    result = 0
    for dev_info in component_manager.checked_devices:
        if dev_info.unresponsive:
            result += 1
    return result


def wait_for_unresponsive(component_manager) -> bool:
    """Waits for device unresponsive update to True.

    :return: True if the device becomes unresponsive within the timeout,
        False otherwise.
    :return: boolean"""
    count = 0
    timeout = 20
    while count < timeout:
        if component_manager.get_device().unresponsive:
            return True
        time.sleep(1)
        count += 1
    return False


def create_cm_cspmln(device, device_type="low"):
    """Returns component manager of device"""
    if device_type == "mid":
        component_manager = CspMLNComponentManagerMid(
            device,
            logger=logger,
            _liveliness_probe=LivelinessProbeType.NONE,
            _event_receiver=True,
        )
    else:
        component_manager = CspMLNComponentManager(
            device,
            logger=logger,
            _liveliness_probe=LivelinessProbeType.NONE,
            _event_receiver=True,
        )
    start_time = time.time()
    time.sleep(SLEEP_TIME)
    elapsed_time = time.time() - start_time
    if elapsed_time > TIMEOUT:
        pytest.fail("Timeout occurred while executing the test")
    return component_manager, start_time


def get_cspmln_command_obj(component_manager, command_class):
    """Returns component manager and command class object for
    CSP Controller leaf node"""
    logger.info("checked %s device", component_manager.csp_master_device_name)
    component_manager.csp_master_device_name = CSP_MASTER_DEVICE_MID
    command_obj = command_class(component_manager, logger=logger)
    return command_obj


def get_cspsln_command_obj(command_class):
    """Returns component manager and command class object for
    CSP Subarray Leaf Node"""
    cm, _ = create_cm_cspsln(input_parameter=InputParameterMid(None))
    command_obj = command_class(cm, logger=logger)
    return cm, command_obj


def get_cspsln_command_obj_low(command_class):
    """Returns component manager and command class object for
    CSP Subarray Leaf Node"""
    cm, _ = create_cm_cspsln(input_parameter=InputParameterLow(None))
    command_obj = command_class(cm, logger=logger)
    return cm, command_obj


def simulate_obs_state_change_on_device(
    device_name: str,
    component_manager: Union[
        CspSLNComponentManagerLow, CspSLNComponentManagerMid
    ],
    obs_state: ObsState,
) -> None:
    """Simulate the change to given obsState for the given device."""
    device_proxy = dev_factory.get_device(device_name)
    device_proxy.SetDirectObsState(obs_state)
    if not wait_for_cm_obstate_attribute_value(
        component_manager, obs_state, 5
    ):
        component_manager.update_device_obs_state(device_name, obs_state)


def create_cm_cspsln(
    liveliness_probe=LivelinessProbeType.NONE,
    event_receiver=True,
    input_parameter: Union[
        InputParameterMid, InputParameterLow
    ] = InputParameterMid(None),
):
    """Creating component manager"""

    if isinstance(input_parameter, InputParameterMid):
        component_manager = CspSLNComponentManagerMid(
            "gitlab://gitlab.com/ska-telescope/ska-telmodel-data?main#tmdata",
            "instrument/ska1_mid/layout/mid-layout.json",
            _input_parameter=InputParameterMid(
                device_name=CSP_SUBARRAY_DEVICE_MID
            ),
            _liveliness_probe=liveliness_probe,
            delay_cadence=10,
            delay_validity_period=20,
            logger=logger,
            _event_receiver=event_receiver,
        )
    else:
        component_manager = CspSLNComponentManagerLow(
            "gitlab://gitlab.com/ska-telescope/ska-telmodel-data?main#tmdata",
            "instrument/ska1_low/layout/low-layout.json",
            _input_parameter=InputParameterLow(
                device_name=CSP_SUBARRAY_DEVICE_LOW
            ),
            delay_cadence=300,
            pst_delay_cadence=300,
            _liveliness_probe=liveliness_probe,
            delay_validity_period=600,
            pst_delay_validity_period=600,
            logger=logger,
            _event_receiver=event_receiver,
        )
    logger.info(
        "csp_subarray_dev_name is in settings: %s ",
        component_manager.input_parameter.csp_subarray_dev_name,
    )
    start_time = time.time()
    return component_manager, start_time


def create_cm_no_faulty_devices(
    liveliness_probe,
    event_receiver,
    input_parameter=InputParameterMid(None),
):
    """Create a method for faulty devices"""
    component_manager, start_time = create_cm_cspsln(
        liveliness_probe, event_receiver, input_parameter
    )
    num_faulty = count_faulty_devices(component_manager)

    assert num_faulty == 0
    elapsed_time = time.time() - start_time
    logger.info("checked %s devices in %s", num_faulty, elapsed_time)
    return component_manager


def event_remover(group_callback, attributes: List[str]) -> None:
    """Removes residual events from the queue."""
    for attribute in attributes:
        try:
            iterable = group_callback._mock_consumer_group._views[
                attribute
            ]._iterable
            for node in iterable:
                logger.info("Payload is: %s", repr(node.payload))
                node.drop()
        except KeyError:
            pass


def get_csp_devices(deployment):
    """Return CSP devices proxy objects based on the CSP type."""
    if deployment == "mid":
        cspsal_node = dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)
        csp_subarray = dev_factory.get_device(MID_CSP_SUBARRAY)
    elif deployment == "low":
        cspsal_node = dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)
        csp_subarray = dev_factory.get_device(LOW_CSP_SUBARRAY)
    else:
        raise ValueError(
            "Invalid CSP type specified. Expected 'mid' or 'low'."
        )
    return cspsal_node, csp_subarray


def extract_json_data(delay_json: dict) -> tuple:
    """
    Extract all the keys and datatype of their respective values.
    :param delay_json : json of delay
    :type delay_json : dict
    :return : tuple of list of keys and values data type
    """
    keys = []
    value_data_type = []

    def extract_keys(delay_data):
        if isinstance(delay_data, dict):
            for key, value in delay_data.items():
                keys.append(key)
                value_type = str(type(value))
                value_data_type.append(value_type[8:-2])
                extract_keys(value)
        elif isinstance(delay_data, list):
            for item in delay_data:
                extract_keys(item)

    extract_keys(delay_json)
    return keys, value_data_type


def retry_for_attribute_value_after_restart(
    device: DeviceProxy,
    attribute_name: str,
    expected_value: str,
    retry: int = 0,
):
    """This method retries multiple times to get the attribute
    value available.
    """
    flag = False
    while retry < 3 and not flag:
        logger.info("FLAG= %s Retry = %s", flag, retry)
        flag = wait_and_validate_device_attribute_value(
            device, attribute_name, expected_value
        )
        retry = retry + 1
    return flag


def wait_and_validate_device_attribute_value(
    device: DeviceProxy,
    attribute_name: str,
    expected_value: str,
    timeout: int = 70,
    negation: bool = False,
    is_json: bool = False,
):
    """This method wait and validate if attribute value is equal to provided
    expected value
    """
    count = 0
    error = ""
    while count <= timeout:
        try:
            attribute_value = device.read_attribute(attribute_name).value
            if is_json:
                attribute_value = json.loads(attribute_value)
            if attribute_value:
                logging.info(
                    "%s current %s value: %s",
                    device.name(),
                    attribute_name,
                    attribute_value,
                )
            if negation:
                if attribute_value != expected_value:
                    return True
            else:
                if attribute_value == expected_value:
                    return True
        except Exception as exception:
            # Device gets unavailable due to restart and the above command
            # tries to access the attribute resulting into exception
            # It keeps it printing till the attribute is accessible
            # the exception log is suppressed by storing into variable
            # the error is printed later into the log in case of failure
            error = exception
        count += 10
        # When device restart it will at least take 10 sec to up again
        # so added 10 sec sleep and to avoid frequent attribute read.
        time.sleep(10)

    logging.exception(
        "Exception occurred while reading attribute %s and cnt is %s",
        error,
        count,
    )
    return False


def add_device_to_db(device_name, class_name, server_name):
    """Add Device to DB"""
    db = Database()
    dev_info = DbDevInfo()
    dev_info.name = device_name
    dev_info._class = class_name
    dev_info.server = server_name
    db.add_device(dev_info)


def wait_for_cm_obstate_attribute_value(
    cm, obs_state: ObsState, timeout=TIMEOUT
) -> bool:
    """Waits for attribute value to change on the given device."""
    start_time = time.time()
    while cm.get_obs_state() != obs_state:
        time.sleep(0.5)
        if time.time() - start_time >= timeout:
            logger.info(
                "The attribute value after time out is: %s",
                cm.get_obs_state(),
            )
            return False
    return True


def get_delay_manager(
    telescope: str,
    telmodel_source: str,
    telmodel_path_low: str,
    telmodel_path_mid: str,
    delay_model_time_in_advance: int = 0,
    delay_cadence: int = 5,
) -> DelayManager:
    """Creates and returns an instance of the DelayManager class."""
    if telescope == "low":
        input_parameter = InputParameterLow()
        cm, _ = create_cm_cspsln(input_parameter=input_parameter)
        delay_manager = DelayManager(
            cm,
            telmodel_source,
            telmodel_path_low,
            delay_model_time_in_advance,
            delay_cadence,
            logger,
        )
    else:
        input_parameter = InputParameterMid()
        cm, _ = create_cm_cspsln(input_parameter=input_parameter)
        delay_manager = DelayManager(
            cm,
            telmodel_source,
            telmodel_path_mid,
            delay_model_time_in_advance,
            delay_cadence,
            logger,
        )
    return delay_manager


def wait_for_condition(
    class_instance: Any,
    condition: str = "",
    attribute: str = "",
    expected_value: Any = True,
    equal_to: bool = True,
    timeout: int = 10,
    interval: float = 0.1,
):
    """Wait for the condition to be either True or equal to/not equal to the
    expected value.

    If condition is not provided, the method checks the attribute value
    against the expected value.
        `class_instance.attribute == expected_value` if equal_to is True
        `class_instance.attribute != expected_value` if equal_to is False

    If the condition is provided along with the attribute, the method checks
    the condition on the attribute value.
        `class_instance.attribute.condition() == expected_value` if equal_to
            is True
        `class_instance.attribute.condition() != expected_value` if equal_to
            is False

    If the condition is provided without the attribute, the method checks the
    condition on the class instance.
        `class_instance.condition() == expected_value` if equal_to is True
        `class_instance.condition() != expected_value` if equal_to is False

    Any exception raised during the condition check is logged and the method
    continues to check the condition until the timeout is reached.

    :param class_instance: Instance of the class to check the condition on.
    :type class_instance: Any (Instance of a class)
    :param condition: Condition to check on the class instance/class attribute.
    :type condition: str (the method name to call on the class instance/class
        attribute)
    :param attribute: Attribute to check or attribute to check the condition
        on.
    :type attribute: str (the attribute name)
    :param expected_value: Expected value of the attribute/condition.
    :type expected_value: Any
    :param equal_to: Flag to check if the condition should be equal to/not
        equal to the expected value.
    :type equal_to: bool
    :param timeout: Timeout in seconds.
    :type timeout: int
    :param interval: Interval in which to check the condition.
    :type interval: float
    """
    start_time = time.time()
    while time.time() - start_time < timeout:
        try:
            if attribute and not condition:
                if (
                    (getattr(class_instance, attribute) == expected_value)
                    if equal_to
                    else (getattr(class_instance, attribute) != expected_value)
                ):
                    return
            else:
                function = methodcaller(condition)
                if attribute:
                    if (
                        (
                            function(getattr(class_instance, attribute))
                            == expected_value
                        )
                        if equal_to
                        else (
                            function(getattr(class_instance, attribute))
                            != expected_value
                        )
                    ):
                        return
                else:
                    if (
                        (function(class_instance) == expected_value)
                        if equal_to
                        else (function(class_instance) != expected_value)
                    ):
                        return
        except Exception as exception:
            logging.exception(
                "Exception occurred while waiting for condition: %s. Input "
                + "data was %s, %s, %s, %s, %s, %s",
                exception,
                class_instance,
                condition,
                attribute,
                expected_value,
                timeout,
                interval,
            )
        time.sleep(interval)
    raise TimeoutError("Condition not met within timeout.")


MID_DELAY_JSON: dict[str, Any] = {
    "interface": "https://schema.skao.int/ska-mid-csp-delaymodel/3.0",
    "start_validity_sec": 0.1,
    "cadence_sec": 0.1,
    "validity_period_sec": 0.1,
    "config_id": "",
    "subarray": 1,
    "receptor_delays": [
        {"receptor": "", "xypol_coeffs_ns": [], "ypol_offset_ns": 0.0},
        {"receptor": "", "xypol_coeffs_ns": [], "ypol_offset_ns": 0.0},
    ],
}

LOW_DELAY_JSON: dict[str, Any] = {
    "interface": "https://schema.skao.int/ska-low-csp-delaymodel/1.0",
    "start_validity_sec": 0.1,
    "cadence_sec": 0.1,
    "validity_period_sec": 0.1,
    "config_id": "",
    "station_beam": 1,
    "subarray": 1,
    "station_beam_delays": [
        {
            "station_id": 1,
            "substation_id": 1,
            "xypol_coeffs_ns": [],
            "ypol_offset_ns": 0.0,
        },
        {
            "station_id": 1,
            "substation_id": 1,
            "xypol_coeffs_ns": [],
            "ypol_offset_ns": 0.0,
        },
    ],
}
