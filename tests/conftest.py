"""Conftest file for CspLeafNodes """
import logging
from os.path import dirname, join
from typing import Dict, List

import pytest
import tango
from ska_tango_testing.mock import MockCallable
from ska_tango_testing.mock.tango import MockTangoEventCallbackGroup
from ska_tmc_common import (
    DevFactory,
    HelperCspMasterDevice,
    HelperSubArrayDevice,
)
from tango import Database
from tango.test_context import MultiDeviceTestContext
from tango.test_utils import DeviceTestContext

from ska_tmc_cspmasterleafnode.csp_master_leaf_node import (
    TmcLeafNodeCspControl,
)
from ska_tmc_cspsubarrayleafnode.csp_subarray_leaf_node_low import (
    LowTmcLeafNodeCspSubarray,
)

CSPSUBARRAYLEAFNODE_MID = "mid-tmc/subarray-leaf-node-csp/01"
CSPSUBARRAYLEAFNODE_LOW = "low-tmc/subarray-leaf-node-csp/01"
CSP_MASTER_LEAF_NODE_MID = "mid-tmc/leaf-node-csp/0"
MID_CSP_SUBARRAY = "mid-csp/subarray/01"
LOW_CSP_SUBARRAY = "low-csp/subarray/01"
CSP_MASTER = "mid-csp/control/0"
CSP_MASTER_LOW = "low-csp/control/0"


def pytest_sessionstart() -> None:
    """
    Pytest hook; prints info about tango version.
    """
    print(tango.utils.info())


def pytest_addoption(parser) -> None:
    """
    Pytest hook; implemented to add the `--true-context` option, used to
    indicate that a true Tango subsystem is available, so there is no
    need for a :py:class:`tango.test_context.MultiDeviceTestContext`.
    """
    parser.addoption(
        "--true-context",
        action="store_true",
        default=False,
        help=(
            "Tell pytest that you have a true Tango context and don't "
            "need to spin up a Tango test context"
        ),
    )


@pytest.fixture
def csp_master_device_mid() -> str:
    """Returns device name for Mid CSP Master Device"""
    return "mid-csp/control/0"


@pytest.fixture
def csp_master_device_low() -> str:
    """Returns device name for Low CSP Master Device Low"""
    return "low-csp/control/0"


@pytest.fixture
def csp_subarray_device_mid() -> str:
    """Returns device name of CSP Subarray Device"""
    return "mid-csp/subarray/01"


@pytest.fixture
def csp_subarray_device_low() -> str:
    """Returns device name of CSP Subarray Device"""
    return "low-csp/subarray/01"


@pytest.fixture()
def telmodel_source():
    """Return the telmodel source for telescope layouts."""
    return "gitlab://gitlab.com/ska-telescope/ska-telmodel-data?main#tmdata"


@pytest.fixture()
def telmodel_path_low():
    """Return the telmodel layout path for Low."""
    return "instrument/ska1_low/layout/low-layout.json"


@pytest.fixture()
def telmodel_path_mid():
    """Return the telmodel layout path for Mid."""
    return "instrument/ska1_mid/layout/mid-layout.json"


@pytest.fixture
def devices_to_load() -> List[Dict[str, List[Dict[str, str]]]]:
    """Returns Helper Devices"""

    # Redefining the fixture in a local scope to avoid
    # redefined-outer-name warning
    def _devices_to_load():
        return [
            {
                "class": HelperSubArrayDevice,
                "devices": [
                    {"name": "mid-csp/subarray/01"},
                    {"name": "low-csp/subarray/01"},
                ],
            },
            {
                "class": HelperCspMasterDevice,
                "devices": [
                    {"name": "mid-csp/control/0"},
                    {"name": "low-csp/control/0"},
                ],
            },
        ]

    return _devices_to_load()


# pylint: disable=redefined-outer-name
@pytest.fixture
def tango_context(devices_to_load, request):
    """Provide context to run devices without database"""
    true_context = request.config.getoption("--true-context")
    logging.info("true context: %s", true_context)
    if not true_context:
        with MultiDeviceTestContext(
            devices_to_load, process=True, timeout=100
        ) as context:
            DevFactory._test_context = context
            logging.info("test context set")
            yield context
    else:
        yield None


@pytest.fixture
def cspsln_device(request):
    """Create DeviceProxy for tests"""
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(LowTmcLeafNodeCspSubarray) as proxy:
            yield proxy
    else:
        database = tango.Database()
        instance_list = database.get_device_exported_for_class(
            "CspSubarrayLeafNode"
        )
        for instance in instance_list.value_string:
            yield tango.DeviceProxy(instance)
            break


@pytest.fixture
def cspmln_device(request):
    """Create DeviceProxy for tests"""
    true_context = request.config.getoption("--true-context")
    if not true_context:
        with DeviceTestContext(TmcLeafNodeCspControl) as proxy:
            yield proxy
    else:
        database = tango.Database()
        instance_list = database.get_device_exported_for_class(
            "TmcLeafNodeCspControl"
        )
        for instance in instance_list.value_string:
            yield tango.DeviceProxy(instance)
            break


@pytest.fixture
def cspsal_node() -> str:
    """Check for the Mid or Low the CSP Subarray Leaf Node instance and
    return the CSP Subarray Leaf Node device proxy"""
    database = Database()
    dev_factory = DevFactory()
    instance_list_mid = database.get_device_exported_for_class(
        "MidTmcLeafNodeCspSubarray"
    )
    if CSPSUBARRAYLEAFNODE_MID in instance_list_mid.value_string:
        return dev_factory.get_device(CSPSUBARRAYLEAFNODE_MID)

    return dev_factory.get_device(CSPSUBARRAYLEAFNODE_LOW)


@pytest.fixture
def task_callback() -> MockCallable:
    """Creates a mock callable for asynchronous testing"""

    # Redefining the fixture in a local scope to avoid
    # redefined-outer-name warning
    def _task_callback():
        return MockCallable(30)

    return _task_callback()


@pytest.fixture
def change_event_callbacks() -> MockTangoEventCallbackGroup:
    """
    Return a dictionary of Tango device change event callbacks with asynchrony
    support.
    """
    return MockTangoEventCallbackGroup(
        "longRunningCommandResult",
        "cspSubarrayObsState",
        "DishVccMapValidationResult",
        timeout=60,
    )


@pytest.fixture
def group_callback() -> MockTangoEventCallbackGroup:
    """Creates a mock callback group for asynchronous testing"""

    # Redefining the fixture in a local scope to avoid
    # redefined-outer-name warning
    def _group_callback():
        return MockTangoEventCallbackGroup(
            "longRunningCommandsInQueue",
            "longRunningCommandResult",
            "longRunningCommandIDsInQueue",
            "DishVccMapValidationResult",
            "sourceDishVccConfig",
            "isSubsystemAvailable",
            timeout=300,
        )

    return _group_callback()


def get_input_str(path) -> str:
    """
    Returns input json string
    """
    with open(path, "r", encoding="utf-8") as file:
        input_str = file.read()
    return input_str


@pytest.fixture()
def json_factory():
    """
    Json factory for getting json files
    """

    def _get_json(slug):
        return get_input_str(join(dirname(__file__), "data", f"{slug}.json"))

    return _get_json


@pytest.fixture()
def json_schema_factory():
    """
    Json schema factory for getting json schema files
    """

    def _get_json(slug):
        return get_input_str(join(dirname(__file__), "schema", f"{slug}.json"))

    return _get_json


@pytest.fixture()
def set_admin_mode_to_online():
    """
    Validate dish vcc config set to true
    """
    csp_master_device = tango.DeviceProxy(CSP_MASTER)
    if csp_master_device.adminMode != 0:
        csp_master_device.adminMode = 0
        csp_master_device.setdirectstate(tango.DevState.OFF)


@pytest.fixture()
def set_admin_mode_csp_to_online_mid():
    """Set the admin mode of mid csp devices"""
    dev_factory = DevFactory()
    csp_master_device = dev_factory.get_device(CSP_MASTER)
    csp_subarray_device = dev_factory.get_device(MID_CSP_SUBARRAY)
    if csp_master_device.adminMode != 0:
        csp_master_device.adminMode = 0
    if csp_subarray_device.adminMode != 0:
        csp_subarray_device.adminMode = 0


@pytest.fixture()
def set_admin_mode_csp_to_online_low():
    """Set the admin mode of low csp devices"""
    dev_factory = DevFactory()
    csp_master_device = dev_factory.get_device(CSP_MASTER_LOW)
    csp_subarray_device = dev_factory.get_device(LOW_CSP_SUBARRAY)
    if csp_master_device.adminMode != 0:
        csp_master_device.adminMode = 0
    if csp_subarray_device.adminMode != 0:
        csp_subarray_device.adminMode = 0


@pytest.fixture()
def set_csp_low_device_admin_mode():
    """Set the admin mode of low csp devices"""
    csp_master_device = tango.DeviceProxy(CSP_MASTER_LOW)
    csp_subarray_device = tango.DeviceProxy(LOW_CSP_SUBARRAY)
    if csp_master_device.adminMode != 0:
        csp_master_device.adminMode = 0
    if csp_subarray_device.adminMode != 0:
        csp_subarray_device.adminMode = 0


@pytest.fixture()
def set_csp_mid_device_admin_mode():
    """Set the admin mode of mid csp devices"""
    csp_master_device = tango.DeviceProxy(CSP_MASTER)
    csp_subarray_device = tango.DeviceProxy(MID_CSP_SUBARRAY)
    if csp_master_device.adminMode != 0:
        csp_master_device.adminMode = 0
    if csp_subarray_device.adminMode != 0:
        csp_subarray_device.adminMode = 0
